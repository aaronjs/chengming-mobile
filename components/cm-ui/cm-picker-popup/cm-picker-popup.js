/*
  @slot 插槽
  default 标题头部位置
  
  @event 时间
  confirm 点击确定
  cancel 点击取消
*/

export default {
  name: 'cm-picker-popup',
  props: {
    /* 
      常规列表数据，但是当列表数据过大时，不推荐传此值，通过相应的方法初始化数据
      每一个对象详情
      text 显示的文字
      value 实际取值
    */
    pickerData: {
      default () {
        return []
      },
      type: Array
    },
    // 初始选中值的索引集
    values: {
      type: Array,
      default () {
        return []
      }
    },
    // 选中框样式，只能传样式CSS字符串
    indicatorStyle: {
      type: String,
      default: 'background-color: rgba(0, 0, 0, 0.04);'
    },
    // 列名，注意跟列表对象对应起来，否则会出现样式错误
    colNames: {
      type: Array,
      default () {
        return []
      }
    },
    // 初始是否生成，若为true则早已生成弹框元素，为false则在第一次弹起时才生成弹框元素
    exist: {
      default: false,
      type: Boolean
    }
  },
  data () {
    return {
    }
  },
  computed: {
  },
  methods: {
    // picker值发生变化时触发的事件
    changeHandler (values, text, indexes) {
      console.log(values, text, indexes)
      this.indexes = indexes
      this.text = text
      this.vals = values
      this.$emit('change', this.vals, this.text, this.indexes)
    },
    
    // 点击确定按键触发的事件
    confirmClickhHandler () {
      this.$emit('confirm', this.vals, this.text, this.indexes)
      this.hide()
    },
    
    // 点击取消按键触发的事件
    cancelClickHandler () {
      this.$emit('calcel')
      this.hide()
    },
    
    // 显示
    show (callback = () => {}, special = () => {}) {
      this.$refs.cmPopup.show(callback, special)
    },
    
    // 隐藏
    hide (callback = () => {}) {
      this.$refs.cmPopup.hide(callback)
    },
    
    // 设定picker的值
    setValues (values = []) {
      this.$nextTick(() => {
        this.$refs.popPicker.setValues(values)
      })
    },
    
    // 设置数据集
    setData (data = []) {
      this.$nextTick(() => {
        this.$refs.popPicker.setData(data)
      })
    },
    
    updateData (data = []) {
      this.$nextTick(() => {
        this.$refs.popPicker.updateData(data)
      })
    },
    
    
    // 设定数据集和选中值，可以用于手动初始化
    set (data = [], values = []) {
      // 初始化数据集
      this.setData(data)
      // 初始化选中值
      this.setValues(values)
    },
  }
}