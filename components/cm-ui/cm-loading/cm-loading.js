import commonMixin from '@/mixins/common.js'
import cmStyleMixin from '@/mixins/cmStyle.js'
/*
  @slot 插槽
  default 加载图标下面的内容
  
  @events 事件
  loading 触发加载，加载事件，为函数，默认带有两个回调函数参数
    @prop onSuccess {Function} - 加载成功回调
    @prop onError {Function} - 加载失败回调
*/

export default {
  name: 'cm-loading',
  mixins: [
    cmStyleMixin,
    commonMixin
  ],
  props: {
    // 默认加载状态,有三种取值 loading/success/error 分别代表 加载中/加载成功/加载失败(显示重新加载按钮)
    state: {
      default: 'loading',
      type: String
    },
    
    // 是否整个页面的加载
    isPageLoading: {
      default: true,
      type: Boolean
    },
    // 加载失败时,是否允许重新加载
    reloadEnabled: {
      default: true,
      type: Boolean
    },
    
    // 加载中显示的文字
    loadingText: {
      default: '加载中...',
      type: String
    },
    errorText: {
      default: '加载失败。',
      type: String
    },
    reloadText: {
      default: '加载失败，点击重新加载',
      type: String
    },
    
    // 加载中显示ICON
    loadingIcon: {
      // 需要在APP.vue中引入colorui，否则必须手动引入才有效
      default: 'cm-icon-load load-cuIcon',
      type: String
    },
    // 加载失败时显示ICON
    errorIcon: {
      // 需要引入对应iconfont文件
      default: 'iconfont icon-reeor',
      type: String
    },
    // 允许重新加载时显示ICON
    reloadIcon: {
      default: 'iconfont icon-exchangerate',
      type: String
    }
  },
  data () {
    return {
      // 加载状态
      loadingState: this.state,
      // 样式 整个页面的加载用fixed定位，局部的加载遮挡用absolute
      loadingClass: this.isPageLoading ? 'cm-loading-fixed' : 'cm-loading-absolute'
    }
  },
  computed: {
    // 加载显示的ICON
    iconType () {
      if (this.loadingState === 'loading') {
        return this.loadingIcon
      }
      else if (this.loadingState === 'error') {
        if (this.reloadEnabled) {
          return this.reloadIcon
        }
        return this.errorIcon
      }
    },
    
    // 加载显示的文字
    txtType () {
      if (this.loadingState === 'loading') {
        return this.loadingText
      }
      else if (this.loadingState === 'error') {
        if (this.reloadEnabled) {
          return this.reloadText
        }
        return this.errorText
      }
    }
  },
  methods: {
    // 初始化方法
    init () {
      // 执行加载事件
      if (this.state == 'loading') {
        this.loading()
      }
    },
    
    // 加载成功
    success () {
      this.loadingState = 'success'
    },
    
    // 加载失败
    error () {
      this.loadingState = 'error'
    },
    
    // 加载
    loading () {
      this.loadingState = 'loading'
      this.$emit('loading', this.success, this.error)
    },
    
    // 点击回调，用于处理点击重新加载事件
    clickHandler () {
      if (this.reloadEnabled && this.loadingState === 'error') {
        this.loading()
      }
    },
    
    // 阻止loading蒙版出现时的页面滚动
    moveHandler () {}
  },
  mounted () {
    // 页面动画结束后，再进行请求
    this.wait(100, 'loading').then(() => {
      this.init()
    })
    .catch((e) => {
      console.log(e)
    })
  }
}