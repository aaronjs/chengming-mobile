import commonMixin from '@/mixins/common.js'

export default {
  name: 'cm-scroll-nav',
  mixins: [
    commonMixin
  ],
  props: {
    // 导航条的方向，hor 顶部横向导航 / ver 左侧纵向导航
    direction: {
      default: 'hor',
      type: String
    },
  },
  data () {
    return {

    }
  },
  computed: {

  },
  methods: {

  },
  mounted () {

  }
}