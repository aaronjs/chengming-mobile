import cmStyleMixin from '@/mixins/cmStyle.js'

export default {
  name: 'cm-radio',
  mixins: [
    cmStyleMixin
  ],
  props: {
    /*
      单选框数据
      value 唯一标记
      text 显示文字
      disabled 是否禁用
    */
    radioData: {
      type: Array,
      default () {
        return []
      }
    },
    // 选中的值
    value: {
      type: String,
      default: ''
    },
    // radioData为空数组，即单个radio时有效
    checked: {
      type: Boolean,
      default: false
    },
    // radioData为空数组，即单个radio时有效
    disabled: {
      type: Boolean,
      default: false
    },
    // radio的类别 primary warning error success
    type: {
      type: String,
      default: 'primary'
    },
    // radio组样式 inline横向排列 block纵向排列
    groupType: {
      type: String,
      default: 'inline'
    },
    // 缩放比例
    scale: {
      type: [Number, String],
      default: 1
    }
  },
  computed: {
    // 是否为选项组
    isRadioGroup () {
      if (this.$u.obj.isArray(this.radioData) && this.radioData.length > 0) {
        return true
      }
      return false
    },
    
    // 选项组的排布样式
    radioGroupClass () {
      if (this.groupType === 'inline') {
        return 'cm-flex cm-flex-wrap cm-flex-j-around'
      }
      return ''
    },
    
    // 选项单元的排布样式
    radioItemClass () {
      if (this.groupType === 'inline') {
        return 'cm-inline-flex cm-flex-a-center'
      }
      return 'cm-flex cm-flex-a-center'
    },
    
    radioStyle () {
      let result = {}
      result.transform = `scale(${this.scale})`
      return result
    },
    
    // 单选框的颜色
    // 暂时无法通过CSS设置，因此只能写死四种颜色在常量配置中
    radioColor () {
      return this.$u.consts.colors[this.type]
    }
  },
  methods: {
    changeHandler (e) {
      const val = e.detail.value
      this.$emit('change', val)
    },
    
    clickHandler (item, index) {
      this.$emit('click', item, index)
    }
  }
}