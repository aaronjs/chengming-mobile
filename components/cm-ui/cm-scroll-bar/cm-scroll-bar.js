import commonMixin from '../mixins/common.js'

export default {
  name: 'cm-scroll-bar',
  mixins: [
    commonMixin
  ],
  props: {
    height: {
      default: 40,
      type: [String, Number]
    },
    /* 
      数据，对象数组，每一个对象结构
      text 导航文字
      id 导航的唯一标识
    */
    list: {
      default () {
        return []
      },
      type: Array
    },
    // 初始选择
    selected: {
      default: 0,
      type: Number
    }
  },
  data () {
    return {
      // 容器的样式
      style: {
        height: this.height + 'px'
      },
      
      /* 布局信息 */
      // 滑动对象的位置信息
      posList: [],
      // scroller的滑动位置
      scrollLeft: 0,
      // 选中的索引
      selectedIndex: this.selected
    }
  },
  computed: {
  },
  methods: {
    // 查找所有组件的位置，并记录
    init () {
      let itemWidths = 0
      const len = this.list.length
      for (let i = 0; i < len; i++) {
        let item = uni.createSelectorQuery().in(this).select('#item-' + this.list[i].id)
        item.boundingClientRect((data) => {
          const obj = {
            start: itemWidths,
            end: itemWidths + data.width
          }
          this.posList.push(obj)
          itemWidths = itemWidths + data.width
        }).exec()
      }
    },
    
    // 点击触发
    clickHandler (index) {
      this.$emit('click', this.list[index], index)
      if (index === this.selectedIndex) { 
        return
      }
      this.scrollToIndex(index)
    },
    
    // 滑动到第i个对象
    scrollToIndex (index) {
      if (index === 0) {
        this.scrollLeft = 0
      }
      if (this.posList[index - 1]) {
        this.scrollLeft = this.posList[index - 1].start
      }
      this.selectedIndex = index
      this.$emit('change', this.list[index], index)
    }
  },
  mounted () {
    this.init()
  }
}