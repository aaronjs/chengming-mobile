import cmStyleMixin from '@/mixins/cmStyle.js'
/*
  @event 时间
  click 点击事件
  change 切换事件
*/

export default {
  name: 'cm-tabbar',
  mixins: [
    cmStyleMixin
  ],
  props: {
    height: {
      default: 50,
      type: [Number, String]
    },
    /*
      每一个单元对象
      icon 指定ICON
      img 指定图片url，图片优先级高于ICON，若不为空串则图片先有效
      text 标签下文字
      badgeTxt 角标文字，传给cm-badge组件
      badgeMin 角标最小数字，传给cm-badge组件
      badgeMax 角标最大数字，传给cm-badge组件
      disabled 不会因为点击而被激活
    */
    tabList: {
      default () {
        return []
      },
      type: Array
    },
    // 初始被激活的tab
    defaultIndex: {
      default: 0,
      type: [Number, String]
    },
    // 被激活的tab拥有的额外class
    activeClass: {
      default: 'app-theme-color',
      type: String
    },
    // 是否为页面底部的选项卡
    isBottom: {
      default: true,
      type: Boolean
    }
  },
  data () {
    return {
      style: {
        // 指定高度，如果是底部的选项卡，需要适配IOS安全距离
        height: this.height + 'px'
      },
      // 当前被激活的tab
      activeIndex: parseInt(this.defaultIndex)
    }
  },
  computed: {
  },
  methods: {
    clickHandler (index) {
      // 点击事件
      this.$emit('click', this.tabList[index], index)
      if (!this.tabList[index].disabled && this.activeIndex !== index) {
        // 切换事件
        this.activeIndex = index
        this.$emit('change', this.tabList[index], index)
      }
    },
    
    // 防止滑动事件穿透
    moveHandler () {}
  }
}