import commonMixin from '../mixins/common.js'

/*
  @slot 插槽
  default 按钮内显示内容，默认为flex布局
  
  @event 时间
  click 点击触发事件
*/

export default {
  name: 'cm-button',
  mixins: [
    commonMixin
  ],
  props: {
    // 按钮类型 default / primary / success / warn / error
    type: {
      default: 'default',
      type: String
    },
    // 按下去的样式，传给原生button组件
    hoverClass: {
      default: 'cm-button-hovered',
      type: String
    },
    // 按钮的内容尺寸
    size: {
      default: 14,
      type: [String, Number]
    },
    // 按钮的两侧边距
    paddingSide: {
      default: 12,
      type: [String, Number]
    },
    // 按钮的上下边距
    paddingFloor: {
      default: 12,
      type: [String, Number]
    },
    // 是否为镂空按钮
    outlined: {
      default: false,
      type: Boolean
    },
    // 是否有边框，必须在outlined为true的情况下有效
    border: {
      default: true,
      type: Boolean
    },
    // 是否有阴影
    shadow: {
      default: false,
      type: Boolean
    },
    // 圆角设置 none没有 / standard标准5px / round两侧圆形 / circle完全圆形
    radius: {
      default: 'standard',
      type: String,
      validator (value) {
        // 这个值必须匹配下列字符串中的一个
        return ['none', 'standard', 'round', 'circle'].indexOf(value) !== -1
      }
    },
    // 是否禁用
    disabled: {
      default: false,
      type: Boolean
    },
    
    // 开放能力
    openType: {
      default: '',
      type: String
    }
  },
  data () {
    return {
    }
  },
  computed: {
    // 按钮的类
    btnClass () {
      return [
        this.type !== 'default' ? `cm-button-${this.type}` : '',
        this.outlined ? 'cm-button-outlined' : '',
        this.shadow ? 'cm-button-shadow' : '',
        !this.border ? 'cm-button-no-border' : ''
      ].filter(str => str !== '').join(' ')
    },
    
    // 按钮的样式
    btnStyle () {
      let style = {
        fontSize: this.size + 'px',
        padding: `${this.paddingFloor}px ${this.paddingSide}px`
      }
      if (this.radius === 'standard') {
        style.borderRadius = '5px'
      }
      else if (this.radius === 'round') {
        style.borderRadius = this.size + this.paddingFloor + 'px'
      }
      else if (this.radius === 'circle') {
        style.borderRadius = '50%'
      }
      return style
    }
  },
  methods: {
    clickHandler () {
      this.$emit('click')
    },
    
    getUserInfoHandler () {
      this.$emit('getuserinfo')
    }
  }
}