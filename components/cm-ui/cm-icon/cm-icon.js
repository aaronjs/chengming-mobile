import commonMixin from '../mixins/common.js'

// 标签默认CSS类名前缀,内容要与该组件的CSS同步
const DEFAULT_CLASS_HEAD = 'cm-icon-size-'

// 显示ICON小标签的组件
/*
  @slot 插槽
  default 默认插槽为附加内容,一般会给插槽中的事物加上position:absolute,用于处理右上角圆点或者遮罩等
  
  @events 事件
  click 点击事件
*/
export default {
  name: 'cm-icon',
  mixins: [
    commonMixin
  ],
  props: {
    // size 标签大小,单位为px,默认为24,范围为12到36,不合法则标签无效果
    size: {
      default: 24,
      type: [Number, String],
      validator (value) {
        // 这个值必须匹配下列字符串中的一个
        return parseInt(value) >= 12 && parseInt(value) <= 60
      }
    },
    // 图标类型，决定显示
    type: {
      default: '',
      type: String
    }
  },
  data () {
    return {
    }
  },
  computed: {
    // 控制icon大小的类名
    sizeClass () {
      return DEFAULT_CLASS_HEAD + this.size
    }
  },
  methods: {
    // 点击回调事件
    clickHandler () {
      this.$emit('click')
    }
  }
}