import cmStyleMixin from '@/mixins/cmStyle.js'

/* 自定义顶部导航 */
export default {
  name: 'cm-nav',
  mixins: [
    cmStyleMixin
  ],
  props: {
    // 指定返回按键，为false或null不显示返回按键，为true单显示返回按键，为文字则显示返回按键和对应文字
    back: {
      default: false,
      type: [Boolean, String]
    },
    // 标题栏排布 between around start
    navAlign: {
      default: 'between',
      type: String
    }
  },
  data() {
    return {
      StatusBar: this.StatusBar,
      CustomBar: this.CustomBar,
    }
  },
  computed: {
    // 标题栏样式
    navClasses () {
      return `cm-flex-j-${this.navAlign}`
    },
    
    // 标题栏包裹层样式，必须在computed声明，因为小程序的StatusBar和CustomBar必须先经过data赋值才有效
    wrapperStyle () {
      return {
        height: this.CustomBar + 'px'
      }
    },
    
    // 标题栏主容器样式
    style () {
      return {
        height: this.CustomBar + 'px',
        paddingTop: this.StatusBar + 'px'
      }
    },
    
    // 居中标题的行高样式
    titleCenterStyle () {
      return {
        paddingTop: this.StatusBar + 'px',
        lineHeight: (this.CustomBar - this.StatusBar) + 'px'
      }
    },
    
    isBackTextShow () {
      return typeof this.back === 'string'
    }
  },
  methods: {
    backHandler () {
      uni.navigateBack({
        delta: 1
      })
    },
    
    // 阻止点击与滑动事件穿透
    moveHandler () {},
    clickHandler () {},
  }
}