import commonMixin from '@/mixins/common.js'
import cmStyleMixin from '@/mixins/cmStyle.js'

/*
  默认撑满屏幕宽度的容器，如果屏幕宽度不足，根据设定的最小宽度，按照屏幕宽度进行比例缩小的容器
  @event finish 加载完成
*/

export default {
  name: 'cm-scale-view',
  mixins: [
    commonMixin,
    cmStyleMixin
  ],
  props: {
    // 容器设计宽度，必填，单位为px
    contentWidth: {
      type: [String, Number],
      default: null
    }
  },
  data () {
    return {
      // 屏幕宽度
      wrapperWidth: -1,
      // 屏幕高度
      wrapperHeight: -1,
      // 容器样式
      wrapperStyle: {},
      // 内容样式
      contentStyle: {
        width: this.contentWidth + 'px'
      }
    }
  },
  computed: {
    compId () {
      return 'scale-' + this.id
    },
  },
  methods: {
    init () {
      // 获取外部容器宽度
      let item = uni.createSelectorQuery().in(this).select('#' + this.compId)
      item.boundingClientRect((data) => {
        this.wrapperWidth = data.width
        this.wrapperHeight = data.height
        // 最终确定样式
        this.setStyle()
      }).exec()
    },
    
    // 样式确定
    setStyle () {
      if (this.contentWidth === null) {
        // 未指定最小宽度，或者未获取到容器宽度，无从判断，直接返回
        this.onFinished()
        return
      }
      // 获取到容器宽度后
      const contentWidth = parseInt(this.contentWidth)
      if (contentWidth < this.wrapperWidth) {
        // 容器宽度大于最小宽度，不用缩放，内容会自动居中
        this.onFinished()
        return
      }
      
      // 容器宽度不足，获取到内容高度以后,根据缩放比例确认新高度
      const ratio = this.wrapperWidth / contentWidth
      this.$set(this.contentStyle, 'transform', `scale(${ratio})`)
      this.$set(this.wrapperStyle, 'height', `${this.wrapperHeight * ratio}px`)
      // 样式加载完成
      this.onFinished()
    },
    
    // 样式加载完成
    onFinished() {
      this.$emit('finish')
    }
  },
  mounted () {
    this.init()
  }
}