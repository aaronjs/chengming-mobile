import commonMixin from '@/mixins/common.js'
import cmStyleMixin from '@/mixins/cmStyle.js'
// 默认的占位图
const IMG_FILL = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEXu7u6DSdFtAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg=='

export default {
  name: 'cm-img',
  mixins: [
    cmStyleMixin,
    commonMixin
  ],
  props: {
    src: {
      default: '',
      type: String
    },
    // 图片的宽度,CSS属性,如果不指定，默认为320px
    width: {
      default: null,
      type: String
    },
    // 图片的高度,CSS属性,如果不指定，默认为240px
    height: {
      default: null,
      type: String
    },
    
    // uni-image图片显示的形式
    mode: {
      default: 'aspectFit',
      type: String
    },
    // 预加载遮盖图片的显示形式
    coverMode: {
      default: 'scaleToFill',
      type: String
    },
    
    // 是否预加载
    preload: {
      default: true,
      type: Boolean
    },
    // 是否懒加载，preload必须为true，该参数才生效
    lazy: {
      default: false,
      type: Boolean
    },
    // 图片加载完成之前的占位图，Base64或静态资源地址
    loadingSrc: {
      default: IMG_FILL,
      type: String
    },
    // 图片加载完成失败之后的占位图，Base64或静态资源地址
    errorSrc: {
      default: IMG_FILL,
      type: String
    },
    
    // 图片预览分组,为null时不开启图片预览功能
    preview: {
      default: null,
      type: String
    },
    
    // 图片的额外样式，可以给图片加个边框什么的，注意预加载的图片也会被填充该样式
    imgStyle: {
      default () {
        return {}
      },
      type: Object
    },
    // 图片的额外的类名，可以给图片加个边框什么的，注意预加载的图片也会被填充该样式
    imgClass: {
      default: '',
      type: String
    },
  },
  data () {
    return {
      // 加载遮挡图的图片地址
      coverSrc: this.loadingSrc,
      // 正式显示图的图片地址，懒加载模式下初始为空串
      imgSrc: !this.lazy ? this.src : '',
      // 加载状态
      isLoading: this.preload ? true : false,
    }
  },
  computed: {
    // 图片组件的常规样式
    imgCommonStyle () {
      let style = {}
      if (typeof this.width === 'string' && this.width !== '') {
        style.width = this.width
      }
      if (typeof this.height === 'string' && this.height !== '') {
        style.height = this.height
      }
      return style
    },
    
    // 控制正式图片的类名
    imgShowClass () {
      if ((!this.isLoading && this.preload) || !this.preload) {
        // 加载完成或无需加载，显示完整样式
        return [this.imgClass]
      }
      return []
    },
    
    // 控制正式图片的样式
    imgShowStyle () {
      if ((!this.isLoading && this.preload) || !this.preload) {
        // 加载完成或无需加载，显示完整样式
        return {
          ...this.imgStyle,
          ...this.imgCommonStyle
        }
      }
      // 加载完成前，高度为0
      return {
        width: 0,
        height: 0
      }
    },
    
    // 覆盖图片的样式
    imgCoverStyle () {
      return {
        ...this.imgStyle,
        ...this.imgCommonStyle
      }
    }
  },
  methods: {    
    // 图片的点击回调事件
    clickHandler () {
      this.$emit('click')
    },
    
    // 图片加载成功的回调事件
    loadHandler () {
      /*setTimeout(() => {
        this.isLoading = false
      }, 1000)*/
      this.isLoading = false
    },
    
    // 图片加载失败的回调事件
    errorHandler () {
      this.coverSrc = this.errorSrc
    }
  },
  mounted () {
  },
  watch: {
    lazyScroll (newVal, oldVal) {
      if (typeof newVal === 'number' && this.preload) {
        // 执行懒加载判断，懒加载必须要求preload为true
      }
    }
  }
}