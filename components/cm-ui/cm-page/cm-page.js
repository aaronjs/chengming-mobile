import commonMixin from '../mixins/common.js'
// 比较方便定义贴顶模块的页面布局组件
export default {
  name: 'cm-page',
  mixins: [
    commonMixin
  ],
  props: {
    // 页面主内容距离顶部的距离
    top: {
      default: 0,
      type: [Number, String]
    },
    // 如果没有导航栏
    // 页面主内容距离底部的距离，考虑到tabbar传参麻烦，就不自带底部组件了
    bottom: {
      default: 0,
      type: [Number, String]
    },
    /* 
      页面模式
      standard 常规自适应，使用页面自带的滚动，此时top区域如果大于0，将自动构造贴顶模块，bottom会向下产生padding-bottom，给底部tab留出空间
      fill 铺满页面，通过绝对定位使页面铺满屏幕，top区域不再贴顶，页面的主体区域一般使用scroll-view滚动
      fill-wrap 铺满容器，已知一个高度确定的容器，该组件将通过绝对定位铺满容器，不再关注CustomBar、StatusBar等页面级设定，也不再关注是否具有标题栏
    */
    mode: {
      default: 'standard',
      type: String,
      validator (value) {
        // 这个值必须匹配下列字符串中的一个
        return ['standard', 'fill', 'fill-wrap'].indexOf(value) !== -1
      }
    },
    // 所在页面是否有顶部标题栏，如果为true，将减去一个CustomBar的高度
    // mode为standard或者fill时有效
    hasTitleBar: {
      default: true,
      type: Boolean
    }
  },
  data () {
    return {
      // 无论是statusBar还是customBar都要再重复赋值，避免小程序出问题
      CustomBar: this.CustomBar,
      StatusBar: this.StatusBar,
    }
  },
  computed: {
    // 容器的样式
    style () {
      if (this.mode === 'standard') {
        // 常规页面，高度自适应，使用页面自带滚动，只用定义内容的上边距
        return {
          paddingTop: this.top + 'px',
          paddingBottom: this.bottom + 'px'
        }
      }
      else if (this.mode === 'fill') {
        // 铺满页面，要设定高度，并给Bottom区域留足位置
        return {
          height: this.hasTitleBar ? `calc(100vh - ${this.CustomBar}px - ${this.bottom}px)` : `calc(100vh - ${this.bottom}px)`
        }        
      }
      else if (this.mode === 'fill-wrap') {
        // 铺满容器，只需要留足底部空间
        return {
          height: `calc(100% - ${this.bottom}px)`
        }
      }
    },
    
    // top区域的样式
    topStyle () {
      let style = { height: this.top + 'px' }
      if (this.mode === 'standard') {
        // 设置fixed定位，贴顶
        style.position = 'fixed'
        style.top = this.hasTitleBar ? this.CustomBar + 'px' : this.StatusBar + 'px'
      }
      else if (this.mode === 'fill') {
        style.position = 'absolute'
        // 注意没标题栏时，顶部区域不能和状态栏抢位置
        style.top = this.hasTitleBar ? 0 : this.StatusBar + 'px'
      }
      return style
    },
    
    // 内容区域的样式
    contentStyle () {
      if (this.mode === 'fill') {
        // 在铺满页面，内容区域要铺满页面
        return {
          position: 'absolute',
          top: this.hasTitleBar ? this.top + 'px' : (this.StatusBar + parseInt(this.top)) + 'px',
          bottom: 0,
          left: 0,
          right: 0
        }
      }
      else if (this.mode === 'fill-wrap') {
        // 铺满容器
        return {
          position: 'absolute',
          top: this.top + 'px',
          bottom: 0,
          left: 0,
          right: 0
        }
      }
    }
  },
  methods: {
    // 阻止点击与滑动事件穿透
    moveHandler () {},
    clickHandler () {},
  }
}