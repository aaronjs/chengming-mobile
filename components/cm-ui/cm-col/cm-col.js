import commonMixin from '../mixins/common.js'

export default {
  name: 'cm-col',
  mixins: [
    commonMixin
  ],
  props: {
    height: {
      default: null,
      type: String
    },
    width: {
      default: null,
      type: String
    },
    borderLeft: {
      default: false,
      type: Boolean
    },
    borderRight: {
      default: false,
      type: Boolean
    },
    borderLeftClass: {
      default: 'app-border-color_ex-left',
      type: String
    },
    borderRightClass: {
      default: 'app-border-color_ex-right',
      type: String
    }
  },
  computed: {
    // 边框类
    borderClass () {
      return (this.borderLeft ? this.borderLeftClass : '') + ' '
        + (this.borderRight ? this.borderRightClass : '')
    },
    
    colStyle () {
      const style = {}
      if (this.height && this.height !== '') {
        style.height = this.height
      }
      if (this.width && this.width !== '') {
        style.width = this.width
        // 一旦指定了宽度,就要去掉flex-grow和flex-shrink
        style.flexGrow = 0
        style.flexShrink = 0
      }
      return style
    }
  }
}