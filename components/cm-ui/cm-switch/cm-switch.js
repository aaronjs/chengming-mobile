import cmStyleMixin from '@/mixins/cmStyle.js'

export default {
  name: 'cm-switch',
  model: {
    event: 'change',
    prop: 'checked'
  },
  mixins: [
    cmStyleMixin
  ],
  props: {
    // 是否选中
    checked: {
      type: Boolean,
      default: false
    },
    // 样式
    type: {
      type: String,
      default: 'primary'
    },
    // label，如果不为空，则展示为表单样式
    label: {
      default: '',
      type: String
    },
    // labelWidth，单位为px
    labelWidth: {
      default: 80,
      type: [Number, String]
    },
    // 是否禁用
    disabled: {
      type: Boolean,
      default: false
    }
  },
  computed: {
    labelStyle () {
      return {
        width: this.labelWidth + 'px'
      }
    },
    
    // 颜色
    // 暂时无法通过CSS设置，因此只能写死四种颜色在常量配置中
    switchColor () {
      return this.$u.consts.colors[this.type]
    }
  },
  methods: {
    changeHandler (e) {
      this.$emit('change', e.detail.value)
    },
    
    clickHandler () {
      this.$emit('click')
    }
  }
}