import CM from '@/class/index.js'

const ganList = CM.Gan.ganList.map((item) => {
  return { text: item, value: item }
})
const zhiList = CM.Zhi.zhiList.map((item) => {
  return { text: item, value: item }
})
// 阳干配阳支，抽取阳支集合
const solarZhiList = zhiList.filter((item, index) => {
  return index % 2 === 0
})
// 阴支集合
const lunarZhiList = zhiList.filter((item, index) => {
  return index % 2 !== 0
})

// 功能不够健壮，相应不完善，凑活一下
export default {
  name: 'cm-picker-ganzhi-popup',
  props: {
    // 类型，gan/zhi/ganzhi分别会出现干单选框，支单选框，干支级联选框
    type: {
      type: String,
      default: 'gan'
    },
    // 初始选中的值，索引数组
    values: {
      default () {
        return []
      },
      type: Array
    },
  },
  data () {
    return {
      // 实际数据
      dataList: [],
      // 实际选中
      indexes: []
    }
  },
  computed: {
    colNames () {
      if (this.type === 'gan') {
        return ['天干']
      }
      else if (this.type === 'zhi') {
        return ['地支']
      }
      return ['天干', '地支']
    }
  },
  methods: {
    changeHandler (values, text, indexes) {
      if (this.type !== 'gan' && this.type !== 'zhi') {
        this.updateData(indexes)
        this.setData()        
      }
      const valueText = indexes.map((des, index) => {
        if (!Array.isArray(this.dataList[index]) || this.dataList[index].length === 0) {
          return null
        }
        if (des < this.dataList[index].length) {
          return this.dataList[index][des].text
        }
        // 处理长度超标的情况，修正val值
        const fixed = this.dataList[index].length - 1
        val[index] = fixed
        return this.dataList[index][fixed].text
      })
      // 获取value集
      const valueList = indexes.map((des, index) => {
        if (!Array.isArray(this.dataList[index]) || this.dataList[index].length === 0) {
          return null
        }
        // 由于之前获取text集时已经修正过val值，所以这里不会再出现长度超标的情况
        return this.dataList[index][des].value
      })
      this.indexes = indexes
      console.log(valueList, valueText, indexes)
      this.$emit('change', valueList, valueText, indexes)    
    },
    
    confirmClickhHandler (values, text, indexes) {
      // 修正value集
      const valueList = indexes.map((des, index) => {
        if (!Array.isArray(this.dataList[index]) || this.dataList[index].length === 0) {
          return null
        }
        // 由于之前获取text集时已经修正过val值，所以这里不会再出现长度超标的情况
        return this.dataList[index][des].value
      })
      const res = (this.type === 'gan' || this.type === 'zhi') ? valueList[0] : (valueList[0] + valueList[1])
      this.$emit('confirm', res)
    },
    
    setData () {
      this.$nextTick(() => {
        this.$refs.popPicker.updateData(this.dataList)
      })
    },
    
    setValues (values) {
      this.$nextTick(() => {
        this.$refs.popPicker.setValues(values)
      })
    },
    
    // 干支级联选择器的场景下，根据天干确定第二列地支
    updateData (values = []) {
      values = this.fillValues(values)
      
      const isSolar = values[0] % 2 === 0
      this.$set(this.dataList, 1, isSolar ? solarZhiList : lunarZhiList)
    },
    
    init () {
      if (this.type === 'gan') {
        this.$set(this.dataList, 0, ganList)
      }
      else if (this.type === 'zhi') {
        this.$set(this.dataList, 0, zhiList)
      }
      else {
        this.$set(this.dataList, 0, ganList)
        this.updateData(this.values)
      }
      this.indexes = this.fillValues(this.values)
    },
    
    // 对长度不足的values对象进行补齐，对长度超过的values的对象进行截断
    fillValues (values = []) {
      let len = this.colNames.length
      const valuesLen = values.length
      if (valuesLen > len) {
        // 截断
        return values.slice(0, len)
      }
      // 补齐
      for (let i = 0; i < len - valuesLen; i++) {
        values.push(0)
      }
      return values
    },
    
    // 显示
    show (callback = () => {}, special = () => {}) {
      this.$refs.popPicker.show(callback, special)
    },
    
    // 隐藏
    hide (callback = () => {}) {
      this.$refs.popPicker.hide(callback)
    },
  },
  created () {
    this.init()
  }
}