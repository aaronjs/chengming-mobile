import commonMixin from '../mixins/common.js'
// 快速进行flex布局的组件，支持边框显示

export default {
  name: 'cm-row',
  mixins: [
    commonMixin
  ],
  props: {
    height: {
      default: null,
      type: String
    },
    width: {
      default: null,
      type: String
    },
    borderTop: {
      default: false,
      type: Boolean
    },
    borderBottom: {
      default: false,
      type: Boolean
    },
    borderTopClass: {
      default: 'app-border-color_ex-top',
      type: String
    },
    borderBottomClass: {
      default: 'app-border-color_ex-bottom',
      type: String
    }
  },
  computed: {
    // 边框类
    borderClass () {
      return (this.borderTop ? this.borderTopClass : '') + ' '
        + (this.borderBottom ? this.borderBottomClass : '')
    },
    
    rowStyle () {
      const style = {}
      if (this.width && this.width !== '') {
        style.width = this.width
      }
      if (this.height && this.height !== '') {
        style.height = this.height
        // 一旦指定了高度,就要去掉flex-grow和flex-shrink
        style.flexGrow = 0
        style.flexShrink = 0
      }
      return style
    }
  }
}