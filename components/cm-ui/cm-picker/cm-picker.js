import cmStyleMixin from '@/mixins/cmStyle.js'

export default {
  name: 'cm-picker',
  mixins: [
    cmStyleMixin
  ],
  props: {
    /* 
      常规列表数据，但是当列表数据过大时，不推荐传此值，通过相应的方法初始化数据
      每一个对象详情
      text 显示的文字
      value 实际取值
    */
    pickerData: {
      default () {
        return []
      },
      type: Array
    },
    // 初始选中值的索引集
    values: {
      type: Array,
      default () {
        return []
      }
    },
    // 选中框样式，只能传样式CSS字符串
    indicatorStyle: {
      type: String,
      default: 'background-color: rgba(0, 0, 0, 0.04);'
    }
  },
  computed: {
  },
  data () {
    return {
      // 真正决定视图内容的列表数据
      dataList: [],
      // 真正决定视图内容的选中索引集
      indexes: [],
      /*
        没在data中定义，但是是重要的组件参数的
        colNum 列数
      */
    }
  },
  methods: {    
    // 选中数据改变是触发
    changeHandler (e) {
      // console.log(e)
      let val = e.detail.value
      // 获取text集
      const valueText = val.map((des, index) => {
        if (!Array.isArray(this.dataList[index]) || this.dataList[index].length === 0) {
          return null
        }
        if (des < this.dataList[index].length) {
          return this.dataList[index][des].text
        }
        // 处理长度超标的情况，修正val值
        const fixed = this.dataList[index].length - 1
        val[index] = fixed
        return this.dataList[index][fixed].text
      })
      // 获取value集
      const valueList = val.map((des, index) => {
        if (!Array.isArray(this.dataList[index]) || this.dataList[index].length === 0) {
          return null
        }
        // 由于之前获取text集时已经修正过val值，所以这里不会再出现长度超标的情况
        return this.dataList[index][des].value
      })
      // 更新index数据
      this.indexes = val
      // 触发事件
      this.$emit('change', valueList, valueText, val)
    },
    
    // 设定数据集和选中值，可以用于手动初始化
    set (data = [], values = []) {
      // 初始化数据集
      this.setData(data)
      // 初始化选中值
      this.setValues(values)
    },
    
    // 设置数据集，并重置所有选中值为0
    setData (data = []) {
      this.dataList = []
      this.indexes = []
      // 更新行数
      this.colNum = data.length
      for (let i = 0; i < this.colNum; i++) {
        // 一段一段赋值，避免单次setData内容量过大
        const list = data[i]
        if (!Array.isArray(list)) {
          this.dataList.push([])
        }
        else {
          this.dataList.push(list)
        }
        this.indexes.push(0)
      }
    },
    
    updateData(data = []) {
      this.dataList = data
    },
    
    // 设定picker的值
    setValues (values = []) {
      values = this.fillValues(values)
      // 模拟onChange事件，并手动触发
      const event = {
        // isSet标明这是模拟的事件对象
        isSet: true,
        detail: {
          value: values
        }
      }
      this.changeHandler(event)
    },
    
    // 对长度不足的values对象进行补齐，对长度超过的values的对象进行截断
    fillValues (values = []) {
      let len = this.colNum
      const valuesLen = values.length
      if (valuesLen > len) {
        // 截断
        return values.slice(0, len)
      }
      // 补齐
      for (let i = 0; i < len - valuesLen; i++) {
        values.push(0)
      }
      return values
    },
  },
  created () {
    // 初始化
    this.set(this.pickerData, this.values)
  }
}