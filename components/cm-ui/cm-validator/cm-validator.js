import cmStyleMixin from '@/mixins/cmStyle.js'

export default {
  name: 'cm-validator',
  mixins: [
    cmStyleMixin
  ],
  props: {
    // 需要校验的数据
    validateData: {
      type: String,
      default: ''
    },
    // 校验器的key
    validateKey: {
      type: String,
      default: ''
    },
    // 校验器的名称
    name: {
      type: String,
      default: '数据'
    },
    // 是否开启非空校验
    notNull: {
      type: Boolean,
      default: true
    },
    // 校验规则，因为小程序prop不兼容函数，只能使用String
    validate: {
      type: String,
      default: ''
    },
    // 校验通过文字
    successTxt: {
      type: String,
      default: '合法'
    },
    // 校验失败文字
    errorTxt: {
      type: String,
      default: '非法'
    },
    // 空值失败文字
    nullTxt: {
      type: String,
      default: '不得为空'
    }
  },
  data () {
    return {
      validateInstance: this.validate,
      
      result: true,
      resultTxt: this.name + this.successTxt
    }
  },
  watch: {
    validateData (val) {
      // 每一次data发生变化，都需要进行一次校验
      this.doValidate()
    }
  },
  created () {
    this.doValidate()
  },
  computed: {
    validateFunc () {
      if (typeof this.validateInstance === 'function') {
        return this.validateInstance
      }
            
      switch (this.validateInstance) {
        case 'mobile':
          // 手机号校验函数
          return (mobile) => {
            const res = this.$u.str.isPhoneNum(mobile)
            return res ? { sign: true, txt: '合法的手机号' } : { sign: false, txt: '非法的手机号' }
          }
          break
        default:
          // 若没有匹配，返回空校验函数
          return () => {
            return true
          }
      }
    },
    
    // 校验结果标签
    resultIcon () {
      if (this.result) {
        return 'iconfont icon-success-fill app-color-success'
      }
      return 'iconfont icon-reeor-fill app-color-error'
    }
  },
  methods: {
    // 校验
    doValidate () {
      let res = 1
      // 校验器校验
      let validateRes = this.validateFunc(this.validateData)
      validateRes = this.solveValidateResult(validateRes)
      res = res & validateRes.sign
      this.resultTxt = validateRes.txt
      
      if (this.notNull && this.validateData === '') {
        // 非空校验
        res = res & 0
        this.resultTxt = this.name + this.nullTxt
      }
      
      this.result = res ? true : false
      this.$emit('validate', this.validateKey, this.result, this.resultTxt)
    },
    
    // 解析校验函数返回的结果
    solveValidateResult (res) {
      if (typeof res === 'object' && res.hasOwnProperty('sign') && res.hasOwnProperty('txt')) {
        return res
      }
      
      if (res) {
        return {
          sign: true,
          txt: this.name + this.successTxt
        }
      }
      
      return {
        sign: false,
        txt: this.name + this.errorTxt
      }
    },
    
    // 设置校验器，兼容小程序，小程序的prop不能传递函数
    setValidate (validate) {
      this.validateInstance = validate
    }
  }
}