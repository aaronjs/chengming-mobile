import commonMixin from '../mixins/common.js'

export default {
  name: 'cm-tag',
  mixins: [
    commonMixin
  ],
  props: {
    // 标签颜色主题，填各种颜色即可，white / black / background 是两个特殊的参数，代表 白色标签 / 黑色标签 / 背景色标签
    color: {
      default: 'background',
      type: String
    },
    // 是否为浅色主题，和镂空outlined不同时生效，outlined优先级更高，覆盖浅色
    light: {
      default: false,
      type: Boolean
    },
    // 是否镂空，优先级高于light
    outlined: {
      default: false,
      type: Boolean
    },
    // 是否可以关闭
    closable: {
      default: false,
      type: Boolean
    },
    // 文字尺寸
    size: {
      default: 12,
      type: [String, Number]
    },
    // 两侧边距
    paddingSide: {
      default: 8,
      type: [String, Number]
    },
    // 上下边距
    paddingFloor: {
      default: 6,
      type: [String, Number]
    },
    // 圆角设置 none没有 / standard标准5px / round两侧圆形 / circle完全圆形
    radius: {
      default: 'standard',
      type: String,
      validator (value) {
        // 这个值必须匹配下列字符串中的一个
        return ['none', 'standard', 'round', 'circle'].indexOf(value) !== -1
      }
    }
  },
  computed: {
    // 标签class控制
    tagClass () {
      let className = ''
      if (this.color === 'white') {
        // 白色标签只有白底黑字，没有浅色系
        className = className + 'app-text-color-imp app-color-white-imp_bg'
        if (this.outlined) {
          // 白色可以有镂空
          className = 'app-color-white-imp app-color-white-imp_bd outlined'
        }
      }
      else if (this.color === 'black') {
        // 黑色标签只有黑底白字，没有浅色系
        className = className + 'app-text-color-reverse-imp app-color-black-imp_bg'
        if (this.outlined) {
          // 黑色可以有镂空
          className = 'app-color-black-imp app-color-black_bd outlined'
        }
      }
      else if (this.color === 'background') {
        // 背景色标签只有背景色+黑字，没有浅色系
        className = className + 'app-text-color-imp app-bg-color-imp_bg'
        if (this.outlined) {
          // 可以有镂空
          className = 'app-bg-color-imp app-bg-color_bd outlined'
        }
      }
      else {
        // 其他颜色
        className = className + `app-text-color-reverse-imp app-color-${this.color}-imp_bg`
        if (this.light) {
          // 浅色系
          className = `app-color-${this.color}-imp app-color-${this.color}-lt-imp_bg`
        }
        if (this.outlined) {
          // 镂空
          className = `app-color-${this.color}-imp app-color-${this.color}_bd outlined`
        }
      }
      return className
    },
    
    // 标签样式
    tagStyle () {
      let style = {
        fontSize: this.size + 'px',
        padding: `${this.paddingFloor}px ${this.paddingSide}px`
      }
      if (this.radius === 'standard') {
        style.borderRadius = '5px'
      }
      else if (this.radius === 'round') {
        style.borderRadius = this.size + this.paddingFloor + 'px'
      }
      else if (this.radius === 'circle') {
        style.borderRadius = '50%'
      }
      return style
    }
  },
  methods: {
    clickHandler () {
      this.$emit('tag-close')
    }
  }
}