// 由于涉及dom检索，需要id确保组件的唯一性
// ID格式：当前时间戳 + 随机串
const getId = () => {
  return new Date().getTime() + Math.random().toString(36).substr(2)
}

// 各组件都需要的公共混入
export default {
  // 混入参数
  props: {
    // 组件外层view的自定义class，为了兼容小程序，但是注意，只有App.vue中定义的全局样式会生效，组件或者页面的私有样式是不生效的
    cmClass: {
      default: '',
      type: String
    },
    // 组件外层view的自定义style，为了兼容小程序，但是注意，只有App.vue中定义的全局样式会生效，组件或者页面的私有样式是不生效的
    cmStyle: {
      default () {
        return {}
      },
      type: Object
    }
  },
  data () {
    return {
      // 组件的唯一ID
      id: this.name + getId()
    }
  },
  methods: {
  }
}