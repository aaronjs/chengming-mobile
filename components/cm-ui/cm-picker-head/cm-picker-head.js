/*
  @slot 插槽
  default 标题头部位置
  
  @event 事件
  left-click 点击左边按钮
  right-click 点击右边按钮
*/
export default {
  name: 'cm-picker-head',
  props: {
    // 列名，注意跟列表对象对应起来，否则会出现样式错误
    colNames: {
      type: Array,
      default () {
        return []
      }
    },
    // 左边按钮文字
    leftText: {
      type: String,
      default: '取消'
    },
    // 右边按钮文字
    rightText: {
      type: String,
      default: '确定'
    }
  },
  computed: {
    // 表头列数
    colNamesNum () {
      if (!Array.isArray(this.colNames)) {
        return 0
      }
      return this.colNames.length
    }
  },
  methods: {
    // 点击确定按键触发的事件
    leftClickhHandler () {
      this.$emit('left-click')
    },
    
    // 点击取消按键触发的事件
    rightClickHandler () {
      this.$emit('right-click')
    },
  }
}