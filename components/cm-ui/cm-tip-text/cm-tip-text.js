import commonMixin from '../mixins/common.js'

// Chrome浏览器最小字体12px
const MIN_FONT = 12

export default {
  name: 'cm-tip-text',
  mixins: [
    commonMixin
  ],
  props: {
    // 主体文字大小
    size: {
      default: 16,
      type: [String, Number]
    },
    // 角标文字大小
    tipSize: {
      default: null,
      type: [String, Number]
    },
  },
  data () {
    return {
      // 边框层样式
      borderClasses: '',
      // 背景层样式
      backgroundClasses: ''
    }
  },
  computed: {
    // 主体文字区域的样式
    mainStyle () {
      const size = (parseInt(this.size) < MIN_FONT) ? MIN_FONT : parseInt(this.size)
      return {
        lineHeight: size + 'px',
        fontSize: size + 'px'
      }
    },
    
    // 角标文字区域的样式
    tipStyle () {
      // 未指定角标大小的情况下，角标大小为主体文字大小的2/3
      let size = isNaN(parseInt(this.tipSize)) ? (parseInt(this.size) * (2 / 3)) : parseInt(this.tipSize)
      if (size < MIN_FONT) {
        size = MIN_FONT
      }
      return {
        height: size + 'px',
        lineHeight: size + 'px',
        fontSize: size + 'px'
      }
    }
  },
  methods: {
    clickHandler () {
      this.$emit('click')
    },
    
    /* 
      设置样式
      type 类别 border/background 边框/背景
      color 颜色，决定颜色CSS，使用名词
    */
    setStyle (type = 'border', color) {
      if (type === 'border') {
        this.borderClasses = `app-color-${color}_bd`
      }
      else {
        this.backgroundClasses = `app-color-${color}-opa_bg`
      }
    },
    
    removeStyle (type) {
      if (type === 'border') {
        this.borderClasses = ''
      }
      else {
        this.backgroundClasses = ''
      }
    }
  }
}