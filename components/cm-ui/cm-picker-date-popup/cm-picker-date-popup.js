// 每种类型的日期权重
const DATE_TYPES = {
  year: 1,
  month: 2,
  day: 3,
  hour: 4,
  min: 5
}

// 一堆坑的组件，使用时准备好面对BUG
export default {
  name: 'cm-picker-date-popup',
  props: {
    // 类型，solar公历时间选择器 / lunar农历时间选择器 / both公历农历时间选择器(切换)
    type: {
      default: 'solar',
      type: String,
      validator (value) {
        // 这个值必须匹配下列字符串中的一个
        return ['solar', 'lunar', 'both'].indexOf(value) !== -1
      }
    },
    // 列名，注意跟列表对象对应起来，否则会出现样式错误
    colNames: {
      type: Array,
      default () {
        return []
      }
    },
    // 初始是否为公历
    solarPickerInit: {
      type: Boolean,
      default: true
    },
    
    // 公历日期选择器的种类，year年 / month月 / day日 / hour小时 / min分钟
    solarType: {
      default: 'min',
      type: String,
      validator (value) {
        // 这个值必须匹配下列字符串中的一个
        return ['year', 'month', 'day', 'hour', 'min'].indexOf(value) !== -1
      }
    },
    // 起始日期,注意Date对象的月一定要比正常情况下减1
    solarStart: {
      default: '1900/01/01 00:00',
      type: String
    },
    // 结束日期,注意Date对象的月一定要比正常情况下减1
    solarEnd: {
      default: '2100/12/31 23:59',
      type: String
    },
    // 初始化公历日期
    solarValues: {
      default: 'now',
      type: String
    },
    
    // 农历日期选择器的种类，year年 / month月 / day日 / hour小时 / min分钟
    lunarType: {
      default: 'min',
      type: String,
      validator (value) {
        // 这个值必须匹配下列字符串中的一个
        return ['year', 'month', 'day', 'hour', 'min'].indexOf(value) !== -1
      }
    },
    // 起始农历年, 默认1900年开始
    lunarStart: {
      default: 1900,
      type: [String, Number]
    },
    // 结束农历年, 默认2100年结束
    lunarEnd: {
      default: 2100,
      type: [String, Number]
    },
    // 农历日期初始，索引数组
    lunarValues: {
      default () {
        return []
      },
      type: Array
    },
    // 选中框样式，只能传样式CSS字符串
    indicatorStyle: {
      type: String,
      default: 'background-color: rgba(0, 0, 0, 0.04);'
    },
    // 初始是否生成，若为true则早已生成弹框元素，为false则在第一次弹起时才生成弹框元素
    exist: {
      default: false,
      type: Boolean
    }
  },
  data () {
    return {
      // 当前选定的picker是否为公历
      isSolarPicker: this.solarPickerInit,
    }
  },
  methods: {
    // 公历picker值发生变化触发
    solarChangeHandler (values, text, indexes) {
      console.log(values, text, indexes, 'solar')
      this.solarIndexes = indexes
      this.solarText = text
      this.solarVals = values
      this.$emit('change', 'solar', this.solarVals, this.solarText, this.solarIndexes)
    },
    
    // picker值发生变化时触发的事件
    lunarChangeHandler (values, text, indexes) {
      console.log(values, text, indexes, 'lunar')
      this.lunarIndexes = indexes
      this.lunarText = text
      this.lunarVals = values
      this.$emit('change', 'lunar', this.solarVals, this.solarText, this.solarIndexes)
    },
    
    // 点击确定按键触发的事件
    confirmClickhHandler () {
      if (this.isSolarPicker) {
        this.$emit('confirm', 'solar', this.solarVals, this.solarText, this.solarIndexes)
      }
      else {
        this.$emit('confirm', 'lunar', this.lunarVals, this.lunarText, this.lunarIndexes)
      }
      this.hide()
    },
    
    // 点击取消按键触发的事件
    cancelClickHandler () {
      this.$emit('calcel')
      this.hide()
    },
    
    // 显示
    show (callback = () => {}, special = () => {}) {
      this.$refs.cmPopup.show(callback, special)
    },
    
    // 隐藏
    hide (callback = () => {}) {
      this.$nextTick(() => {
        this.$refs.cmPopup.hide(callback)
      })
    },
    
    // 切换公历农历picker
    changePicker (sign = true) {
      this.isSolarPicker = sign
      // 触发切换事件
      this.$emit('switch', sign ? 'solar' : 'lunar')
    },
    
    // 设定数据集和选中值，可以用于手动初始化
    setSolar (startDate, endDate, values) {
      // 初始化数据集
      this.setSolarData(startDate, endDate)
      // 初始化选中值
      this.setSolarValues(values)
    },
    
    setSolarData (startDate, endDate) {
      this.$refs.solarPicker.setData(startDate, endDate)
    },
    
    setSolarValues (values) {
      this.$refs.solarPicker.setValues(values)
    },
    
    setLunar (startYear, endYear, values = []) {
      // 初始化数据集
      this.setLunarData(startYear, endYear)
      // 初始化选中值
      this.setLunarValues(values)
    },
    
    setLunarData (startYear, endYear) {
      this.$refs.lunarPicker.setData(startYear, endYear)
    },
    
    setLunarValues (values = []) {
      this.$refs.lunarPicker.setValues(values)
    }
  }
}