import commonMixin from '../mixins/common.js'

// 卡片组件
/*
  @slot 默认插槽为卡片主体内容
  @slot header 标题栏标题
  @slot footer 卡片底部
  
  @event extra-click 右侧灰色文字点击事件
*/

export default {
  name: 'cm-card',
  mixins: [
    commonMixin
  ],
  props: {
    /* @section 是否存在头部和底部 */
    // 是否存在卡片底部
    footerEnabled: {
      default: false,
      type: Boolean
    },
    
    /* @section 卡片标题样式 */
    // 卡片标题
    title: {
      default: '',
      type: String
    },
    // 卡片额外内容文字
    extra: {
      default: '',
      type: String
    },
    // 标题前是否有标签，standard模式下有效
    titleIcon: {
      default: '',
      type: String
    },
    
    /* @section 卡片整体样式 */
    // 是否占满宽度
    isFull: {
      default: true,
      type: Boolean
    },
    // 是否有阴影
    isShadow: {
      default: true,
      type: Boolean
    }
  },
  computed: {
    // 卡片的class组合，决定整体样式
    cardClasses () {
      return [
        !this.isFull ? 'not-full' : '',
        this.isShadow ? 'shadow' : ''
      ].filter(str => str !== '').join(' ')
    },
    
    // 根据标题、额外内容，图片等信息决定头部的板式
    headMode () {
      if ((this.title && this.title !== '') || (this.extra && this.extra !== '')) {
        // 标准头部，左标题，右额外内容
        return 'standard'
      }
      // 自定义头部
      return ''
    }
  },
  methods: {
    extraClickHandler () {
      this.$emit('extra-click')
    }
  }
}