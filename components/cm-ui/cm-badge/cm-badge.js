import commonMixin from '../mixins/common.js'

// 默认边距数据
const PADDING_FLOOR = 2
const PADDING_SIDE = 5
const PADDING_RADIUS = 4

// 数字角标
export default {
  name: 'cm-badge',
  mixins: [
    commonMixin
  ],
  props: {
    // 字体大小
    size: {
      default: 12,
      type: [String, Number]
    },
    // 内容，为true是单纯显示红点，为false、null时不显示，为数字或字符时，显示对应内容
    text: {
      default: false,
      type: [String, Number, Boolean]
    },
    // 最大数字，若不想设置截断，可设为null或者false
    maxNum: {
      default: 99,
      type: [String, Number]
    },
    // 最小数字，小于此数则不显示，设置为null或者false无效
    minNum: {
      default: 1,
      type: [String, Number]
    },
    // 类别 primary/success/warn/error 分别对应 蓝/绿/黄/红
    type: {
      default: 'error',
      type: String
    },
    // 是否不显示背景色
    inverted: {
      default: false,
      type: Boolean
    }
  },
  data () {
    return {
    }
  },
  computed: {
    badgeClass () {
      return this.inverted ? `app-color-${this.type}` : `app-color-${this.type}_bg app-text-color-reverse`
    },
    
    badgeStyle () {
      let style = {
        fontSize: this.size + 'px',
        padding: PADDING_FLOOR + 'px ' + PADDING_SIDE + 'px',
      }
      // console.log(this.text, this.maxNum, this.minNum)
      if (
        // 文字小于最小截断数
        typeof this.minNum === 'string' || typeof this.minNum === 'number' && parseInt(this.minNum) > parseInt(this.text) ||
        // 或者文字为false
        (typeof this.text === 'boolean' && !this.text) ||
        // 或者为空串
        this.text === '' ||
        // 或者为null
        this.text === null
      ) {
        // 不显示，隐藏起来的情况
        style.display = 'none'
      }
      else if (typeof this.text === 'boolean' && this.text) {
        // 显示圆点
        style.borderRadius = '50%'
        style.padding = PADDING_RADIUS + 'px'          
      }
      else {
        // 常规显示
        style.borderRadius = (parseInt(this.size) / 2 + PADDING_FLOOR) + 'px'
      }
      return style
    },
    
    textShow () {
      if (typeof this.text === 'string' || typeof this.text === 'number') {
        if (typeof this.maxNum === 'string' || typeof this.maxNum === 'number') {
          // 设置了最大截断数
          const maxNum = parseInt(this.maxNum)
          const num = parseInt(this.text)
          if (!isNaN(maxNum) && !isNaN(num) && num > maxNum) {
            return this.maxNum + '+'
          }
        }
        // 不到截断数 / 未设置截断数 / text为非数字字符串
        return this.text
      }
      return ''
    }
  }
}