export default {
  name: 'cm-modal',
  props: {
    // 顶部标题的文字
    title: {
      default: '',
      type: String
    },
    /*
      模态框类型
      slot 自定义
      standard 只有文字内容
      input 带有输入框
      其他更多
    */
    type: {
      default: 'standard',
      type: String
    },
    // 输入框的默认值
    inputValue: {
      default: '',
      type: String
    },
    // 输入框的placeholder
    inputHolder: {
      default: '',
      type: String
    },
    // 中部内容文字
    content: {
      default: '',
      type: String
    },
    // 高度，传给cmpopup
    height: {
      default: '170px',
      type: String
    },
    // 底部取消按钮的文字
    cancelTxt: {
      default: '取消',
      type: String
    },
    // 底部确认按钮的文字
    confirmTxt: {
      default: '确定',
      type: String
    },
    // 是否禁用有底部按钮
    bottomDisabled: {
      default: false,
      type: Boolean
    },
    // 点击遮罩层关闭
    maskClosed: {
      default: false,
      type: Boolean
    },
  },
  data () {
    return {
      modalInput: this.inputValue
    }
  },
  watch: {
    inputValue (val) {
      this.modalInput = val
    }
  },
  computed: {
    
  },
  methods: {
    show () {
      this.$refs.popup.show()
    },
    
    hide () {
      this.$refs.popup.hide()
    },
    
    inputClear () {
      this.modalInput = ''
    },
    
    // 点击取消触发事件
    cancelHandler () {
      this.$emit('cancel')
      this.hide()
    },
    
    // 点击确认触发事件
    confirmHandler () {
      let result = {}
      if (this.type === 'input') {
        result.inputTxt = this.modalInput
      }
      this.$emit('confirm', result)
    }
  }
}