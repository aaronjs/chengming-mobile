import cmStyleMixin from '@/mixins/cmStyle.js'

export default {
  name: 'cm-label-text',
  mixins: [
    cmStyleMixin
  ],
  props: {
    // 表头内容
    label: {
      default: '',
      type: String
    },
    // 表头部分宽度
    labelWidth: {
      default: 80,
      type: [Number, String]
    },
    labelAlign: {
      default: 'center',
      type: String
    },
    labelNoPadding: {
      default: false,
      type: Boolean
    },
    // 表头部分样式类
    labelClass: {
      default: '',
      type: String
    },
    // 表头部分样式
    labelStyle: {
      default () {
        return {}
      },
      type: Object
    },
    // 主体部分样式类
    contentClass: {
      default: '',
      type: String
    },
    // 主体部分样式
    contentStyle: {
      default () {
        return {}
      },
      type: Object
    },
    // 主体部分不设置边距
    contentNoPadding: {
      default: false,
      type: Boolean
    },
    // 表头是否在右边
    reverse: {
      default: false,
      type: Boolean
    },
    // 表头和内容是否有边框分隔
    borderSplit: {
      default: false,
      type: Boolean
    },
    // 顶部边框
    borderTop: {
      default: false,
      type: Boolean
    },
    // 底部边框
    borderBottom: {
      default: false,
      type: Boolean
    }
  },
  data () {
    return {
      
    }
  },
  computed: {
    // 容器整体样式类
    wrapperClass () {
      return [
        this.borderTop ? 'app-border-color_ex-top' : '',
        this.borderBottom ? 'app-border-color_ex-bottom' : '',
        this.reverse ? 'cm-flex-r-row' : ''
      ].join(' ')
    },
    
    // 表头部分样式
    labelSize () {
      return {
        width: parseInt(this.labelWidth) + 'px'
      }
    },
    
    // 表头部分动态类
    labelExtraClass () {
      let classes = [
        `cm-text-${this.labelAlign}`,
        this.labelNoPadding ? '' : 'cm-padding-10'
      ]
      if (this.reverse) {
        classes.push(this.borderSplit ? 'app-border-color_ex-left' : '')
      }
      else {
        classes.push(this.borderSplit ? 'app-border-color_ex-right' : '')
      }
      return classes.join(' ')
    },
    
    // 内容部分是否分割
    contentExtraClass () {
      return this.contentNoPadding ? '' : 'cm-padding-10'
    }
  }
}