import cmStyleMixin from '@/mixins/cmStyle.js'

export default {
  name: 'cm-input',
  model: {
    event: 'input',
    prop: 'value'
  },
  mixins: [
    cmStyleMixin
  ],
  props: {
    value: {
      type: String,
      default: ''
    },
    // 文字大小
    size: {
      type: [Number, String],
      default: 16
    },
    // 类型,主要控制弹出哪种类型的键盘，并不限制输入内容
    type: {
      type: String,
      default: 'text'
    },
    // 是否可清除
    clearable: {
      type: Boolean,
      default: false
    },
    // 是否为密码
    password: {
      type: Boolean,
      default: false
    },
    // 是否显示密码，只在password为true时生效
    showPassword: {
      type: Boolean,
      default: true
    },
    // 是否禁用
    disabled: {
      type: Boolean,
      default: false
    },
    // 占位符
    placeholder: {
      type: String,
      default: ''
    },
    // 占位符默认样式类
    placeholderClass: {
      type: String,
      default: 'input-placeholder'
    },
    // 占位符默认样式
    placeholderStyle: {
      type: String,
      default: ''
    },
    // 是否获取焦点
    focus: {
      type: Boolean,
      default: false
    },
    // 最大输入长度，设置为 -1 的时候不限制最大长度
    maxlength: {
      type: Number,
      default: -1
    },
    // 是否提示输入长度
    showMaxlength: {
      type: Boolean,
      default: false
    },
    // 设置弹出软键盘键盘右下角按钮的文字，仅在 type="text" 时生效。
    confirmType: {
      type: String,
      default: 'done'
    },
    // 输入框是否有边框
    border: {
      type: [Boolean, String],
      default: false
    },
    // 是否初始化完成后立即触发一次input事件
    inputInit: {
      type: Boolean,
      default: false
    }
  },
  data () {
    return {
      inputValue: this.value,
      
      // iuput 占位文字的最终样式
      placeholderFinalStyle: `font-size: ${this.size}px; ${this.placeholderStyle}`,
      // input 文字大小样式
      sizeStyle: {
        fontSize: this.size + 'px' 
      },
      
      // input是否被聚焦
      isFocus: this.focus,
      // 状态
      state: {
        password: this.password
      }
    }
  },
  watch: {
    value (val) {
      this.inputValue = val
    }
  },
  computed: {
    // 决定input边框的类
    borderClass () {
      if (!this.border) { return '' }
      const borderClass = this.border === 'black' ? 'border-black' : 'border'
      return this.isFocus ? 'border-focus' : borderClass
    }
  },
  methods: {
    // 输入发生变化时触发事件
    inputHandler (e) {
      const val = e.detail.value
      this.inputValue = val
      this.$emit('input', val)
    },
    
    // 聚焦时触发事件
    focusHandler (e) {
      this.isFocus = true
      this.$emit('focus', e)
    },
    
    // 失去焦点时触发事件
    blurHandler (e) {
      this.isFocus = false
      this.$emit('blur', e)
    },
    
    // 点击完成按钮时触发
    confirmHandler (e) {
      this.$emit('confirm', e)
    },
    
    // 键盘高度发生变化的时候触发
    keyboardChangeHandler (e) {
      this.$emit('keyboard-change', e)
    },
    
    // 密码可视触发
    showPasswordHandler () {
      this.state.password = !this.state.password
    },
    
    // 清楚输入框
    clearHandler () {
      this.inputValue = ''
      this.$emit('clear')
    }
  },
  created () {
    if (this.inputInit) {
      this.$emit('input', this.value)
    }
  }
}