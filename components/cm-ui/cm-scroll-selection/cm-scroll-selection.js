/*
  @event select 选中了某个选项
*/

// 滚动选项表
export default {
  name: 'cm-scroll-selection',
  props: {
    // hor横向列表，ver纵向列表
    type: {
      default: 'hor',
      type: String
    },
    // 容器宽度，CSS属性
    width: {
      default: '100%',
      type: String
    },
    // 容器高度，CSS属性
    height: {
      default: '100%',
      type: String
    },
    // 选项数据
    selections: {
      default () {
        return []
      },
      type: Array
    },
    // 选则的数据索引
    selected: {
      default: 0,
      type: Number
    },
    // 选项容器的宽度或者高度，取决于横向或纵向，单位px
    selectionSize: {
      default: 50,
      type: [Number, String]
    },
    // 选项容器的边距，单位px
    selectionMargin: {
      default: 0,
      type: [Number, String]
    },
    // 滑动是否伴随动画
    scrollWithAnimation: {
      default: true,
      type: Boolean
    },
    // 选项滑动距离向偏移个数，1代表选项左边会至少暴露一个选项
    scrollOffset: {
      default: 0,
      type: [Number, String]
    }
  },
  data () {
    return {
      // 左滑的距离
      scrollPos: 0,
      // 左滑距离历史记录
      scrollOld: 0,
      // 选中的选项
      selectedIndex: this.selected
    }
  },
  watch: {
    // 监听选项变化
    selected (val) {
      this.gotoIndex(val)
    },
    // 监听选项数据变化
    selections: {
      handler () {
        this.gotoIndex(this.selectedIndex)
      },
      deep: true
    }
  },
  computed: {
    // 容器样式
    wrapperStyle () {
      return {
        width: this.width,
        height: this.height
      }
    },
    
    // 选项容器样式
    selectionWrapperStyle () {
      if (this.type === 'hor') {
        return {
          height: `calc(100% - ${this.selectionMargin * 2}px)`,
          margin: this.selectionMargin + 'px',
          width: this.selectionSize + 'px'
        }
      }
      return {
        width: `calc(100% - ${this.selectionMargin * 2}px)`,
        margin: this.selectionMargin + 'px',
        height: this.selectionSize + 'px'
      }
    }
  },
  methods: {    
    // 选项位置全部加载完成时触发
    init () {
      // 滑动到初始位置
      this.gotoIndex(this.selectedIndex)
    },
    
    // 滑动触发事件
    scrollHandler (e) {
      if (this.type === 'hor') {
        this.scrollOld = e.detail.scrollLeft
      }
      else {
        this.scrollOld = e.detail.scrollTop
      }
    },
    
    // 点击选项触发事件
    clickHandler (item, index) {
      if (this.selectedIndex === index) {
        return
      }
      this.$emit('select', item, index)
    },
    
    // 前往某一个选项
    gotoIndex (index) {
      const len = this.selections.length
      this.selectedIndex = index
      if (this.selectedIndex >= len) {
        this.selectedIndex = len - 1
      }
      if (this.selectedIndex < 0) {
        this.selectedIndex = 0
      }
      
      this.scrollPos = this.scrollOld
      this.$nextTick(() => {
        // 获取偏移
        const itemPos = parseInt(this.selectionSize) + 2 * parseInt(this.selectionMargin)
        let offset = this.selectedIndex - parseInt(this.scrollOffset)
        if (offset < 0) {
          offset = 0
        }
        if (offset >= len) {
          offset = len - 1
        }
        this.scrollPos = itemPos * offset
      })
    }
  },
  mounted () {
    this.init()
  }
}