import Vue from 'vue'

import store from './store/index.js'

import App from './App'

import utils from '@/utils/index.js'
import global from '@/global.js'

Vue.config.productionTip = false

// 挂载vuex
Vue.prototype.$store = store

// 挂载通用方法
Vue.prototype.$u = utils
// 读取配置信息
Vue.prototype.$u.config.load()

global.init()

App.mpType = 'app'

const app = new Vue({
	...App,
  store
})
app.$mount()
