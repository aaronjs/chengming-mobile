import Location from '@/class/location/location.js'

import utils from '@/utils/index.js'

const location = {
  namespaced: true,
  state: {
    // 是否激活位置信息
    isInit: false,
    // 全部地址列表
    locationsList: [],
    // 当前有效的地址
    selectedIndex: -1,
  },
  mutations: {
    // 激活位置信息
    init (state) {
      state.isInit = true
    },
    
    // 往列表添加一个地址
    add (state, location) {
      if (!state.locationsList.find(item => item.address === location.address && item.type === location.type)) {
        // 只添加不重复的地址
        state.locationsList.push(location)
      }
    },
    
    // 往列表中添加一堆地址
    addList (state, locationList) {
      const len = locationList.length
      for (let i = 0; i < len; i++) {
        const location = locationList[i]
        this.commit('location/add', location)
      }
    },
    
    // 向列表中添加一个默认地址
    addDefault (state) {
      let defaultLoc = new Location()
      defaultLoc.defaultLocate()
      // 此处的this是全局store对象
      this.commit('location/add', defaultLoc)
    },
    
    // 选中一个地址
    select (state, index) {
      state.selectedIndex = index
    }
  },
  actions: {
    // 向列表中添加一个指定地址
    async addSet (context, options) {
      let location = new Location()
      location.setLocate(options)
      if (location.isEnabled()) {
        // 必须是有效的地址，才加入地址列表
        context.commit('add', location)
        // 注意要实时更新storage
        await context.dispatch('saveLocations')        
      }
    },
    
    // 向列表中添加一个当前定位地址
    async addLocate (context) {
      let location = new Location()
      await location.locate()
      if (location.isEnabled()) {
        // 成功后，添加
        context.commit('add', location)        
      }
    },
    
    // 存取所有手动指定的地址
    async saveLocations (context) {
      const setLocations = context.getters.setLocation
      await utils.storage.set('locations', setLocations)
    },
    
    // 读取所有手动指定的地址，并加入地址列表
    async loadLocations (context) {
      const locations = await utils.storage.get('locations')
      if (locations) {
        context.commit('addList', locations)
      }
    },
    
    // 初始化地址信息
    async init (context) {
      if (context.state.isInit) {
        // 若已成功激活，则不用重复初始化
        return
      }
      context.commit('init')
      
      // 先添加默认地址，并选中默认地址
      context.commit('addDefault')
      const defaultLoc = context.getters.defaultLocation
      context.commit('select', defaultLoc.index)
      // 读取手动指定的地址，以及手动指定地址的选择
      await context.dispatch('loadLocations')
      // 再添加GPS定位地址
      await context.dispatch('addLocate')
      // 检查GPS定位地址
      const locateLoc = context.getters.locateLocation
      if (locateLoc) {
        // GPS定位成功，选中定位地址
        context.commit('select', locateLoc.index)
      }
    }
  },
  getters: {
    // 当前选中的地址
    selectedLocation (state) {
      if (state.selectedIndex < 0 || !state.locationsList[state.selectedIndex]) {
        return null
      }
      return state.locationsList[state.selectedIndex]
    },
    
    // 获取默认的地址，返回对象+索引
    defaultLocation (state) {
      const index = state.locationsList.findIndex(item => item.type === 'default')
      if (index < 0) {
        return null
      }
      return {
        index: index,
        location: state.locationsList[index]
      }
    },
    
    // 获取定位的地址，返回对象+索引
    locateLocation (state) {
      const index = state.locationsList.findIndex(item => item.type === 'gps')
      if (index < 0) {
        return null
      }
      return {
        index: index,
        location: state.locationsList[index]
      }
    },
    
    // 获取手动添加的指定地址，返回对象列表
    setLocation (state) {
      return state.locationsList.filter(item => item.type === 'set')
    }
  }
}

export default location