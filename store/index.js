import Vue from 'vue'
import Vuex from 'vuex'

import location from './location.js'
import user from './user.js'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    location,
    user
  }
})

export default store