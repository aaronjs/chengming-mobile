import User from '@/class/user/user.js'

const user = {
  namespaced: true,
  state: {
    user: new User()
  },
  mutations: {
    login (state, data) {
      state.uid = data._id
      state.account = data.hasOwnProperty('account') ? data.account : ''
      state.nickname = data.hasOwnProperty('nickname') ? data.nickname : ''
      state.motto = data.hasOwnProperty('motto') ? data.motto : ''
      state.avatar = data.hasOwnProperty('avatar') ? data.avatar : ''
      state.phone = data.hasOwnProperty('phone') ? data.phone : ''
    },
    
    // 登出
    logout (state) {
      state.uid = null,
      state.account = ''
      state.nickname = ''
      state.motto = ''
      state.avatar = ''
      state.phone = ''
      state.belongings = []
    }
  }
}

export default user