module.exports = {
  css: {
    loaderOptions: {
      stylus: {
        import: [
          '~@/styles/variables.styl',
          '~@/styles/mixins.styl'
        ],
      }
    }
  }
}