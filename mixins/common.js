import utils from '@/utils/index.js'

/* 
  自定义组件常规的混入，能够解决以下问题
  1. 生成唯一的ID，确保DOM操作获取唯一对象
  2. 处理定时器的问题,
*/
export default {
  data () {
    return {
      // 组件的唯一ID
      id: ''
    }
  },
  methods: {
    // 获取组件唯一的ID
    initId () {
      // comp- + 时间戳 + 8位随即串
      this.id = 'comp-' + new Date().getTime() + utils.str.randomStr()
    },
    
    // 定时器处理
    initTimer () {
      this.$once('hook:beforeDestroy', () => {
        // 清楚和组件有关的所有定时器
        utils.common.clearTimers(this.id)
      })
    },
    
    // 延时处理
    wait (time = 1000, name = '') {
      return new Promise((resolve, reject) => {
        const compName = this.id + name
        utils.common.wait(time, compName).then(() => {
          resolve()
        })
        .catch((e) => {
          reject(e)
        })
      })
    },
    
    // 定时器
    interval (time = 1000, name = '', func = () => {}) {
      const compName = this.id + name
      utils.common.interval(time, compName, func)
    },
    
    clearTimer (name) {
      const compName = this.id + name
      utils.common.clearTimer(compName)
    }
  },
  created () {
    this.initId()
    this.initTimer()
  }
}