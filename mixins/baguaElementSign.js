// 遁甲标记混入
export default {
  data () {
    return {
      // 对象比较状态，当前选中的是base/opposite
      compareSign: '',
      
      // 对象比较标记
      compareSelected: {
        base: '',
        opposite: ''
      },
      
      // 时令标记
      seasonIndex: -1
    }
  },
  methods: {
    // 点击遁甲符号后触发
    elementClickHandler (item) {
      if (this.compareSign === '' || this.compareSign === 'none' || !this.compareSign) {
        // 不处在对比状态时，直接跳出
        return
      }
      
      // 设置新的样式
      if (this.compareSign === 'base') {
        this.compareSelected.base = item.id
      }
      else if (this.compareSign === 'opposite') {
        this.compareSelected.opposite = item.id
      }
      // 与工具进行通信
      this.onSpecialChange({
        action: 'baguaCompare',
        type: this.compareSign,
        selected: this.compareSelected,
        item
      })
    },
    
    // 清空比较标记
    clearCompareSign (clearDes = '') {
      this.compareSelected.base = ''
      this.compareSelected.opposite = ''
    },
    
    // 检查时令
    checkSeason (data, env) {
      // 季节索引就是月支五行
      if (!data.isSeasonCheck) {
        this.seasonIndex = -1
      }
      else {
        this.seasonIndex = this[env].date.ganzhi[1].zhi.wuxing
      }
    },
    
    // 处理八卦标记事件
    baguaElementEvent (setting, env) {
      // 处理对象比较事件
      if (setting.type === 'baguaCompare' && setting.baguaCompare) {
        this.compareSign = setting.baguaCompare.active
        return
      }
      else {
        // 不需要比较时，清空所有比较标记
        this.compareSign = ''
        this.clearCompareSign()
        this.onSpecialChange({
          action: 'baguaCompare',
          type: 'none',
          selected: this.compareSelected
        })
      }
      
      // 处理时令事件
      if (setting.type === 'seasonCheck' && setting.seasonCheck) {
        this.checkSeason(setting.seasonCheck, env)
        return
      }
    },
  }
}