import utils from '@/utils/index.js'

import CM from '@/class/index.js'

// 使用Popup日期选择器时，需要的混入，封装了一系列方法，磨平了datePicker的多个奇异BUG
export default {
  data () {
    return {
      // 初始Picker是公历还是农历
      curPicker: 'solar',
      
      // 日历数据
      date: new CM.Date(),
      
      // 选择器起止年
      startYear: utils.config.app.timePickerStart,
      endYear: utils.config.app.timePickerEnd,
      // 公历选择器起始时间后缀
      startSolarTail: '/01/01 00:00',
      endSolarTail: '/12/31 23:59',
      // 选择器初始选中的时间
      solarInit: 'now',
      lunarInit: []
    }
  },
  methods: {
    // 初始化
    initDatePicker () {      
      // 初始化选择器默认时间
      /* @bug 如果不初始化农历时间，会在H5平台出现严重BUG */
      this.solarInit = this.getSolarValues()
      this.lunarInit = this.getLunarValues()
    },
    
    // 根据指定date获取对应的Picker值
    getSolarValues () {
      const start = this.startYear
      const startKey = this.startSolarTail.split(' ')[0]
      const end = this.endYear
      const endKey = this.endSolarTail.split(' ')[0]
      if (this.date.year > end) {
        // 当前时间超出了公历的后边界，此时公历选择器选中最后一年的12-31
        return end + endKey + ' ' + this.date.getTime()
      }
      if (this.date.year < start) {
        // 当前时间超出了公历的前边界
        return start + startKey + ' ' + this.date.getTime()
      }
      else {
        return this.date.getDateStr()
      }
    },
    
    // 根据当前的date获取农历Picker索引集
    getLunarValues () {
      const start = this.startYear
      const end = this.endYear
      if (this.date.lunarYear > end) {
        // 当前时间超出了农历的后边界，此时农历选择器选中最后一年的正月初一
        return [end - start, 0, 0, this.date.hour, this.date.min]
      }
      if (this.date.lunarYear < start) {
        // 当前时间超出了农历的前边界，此时农历选择器选中第一年正月初一
        return [0, 0, 0, this.date.hour, this.date.min]
      }
      else {
        // 获取农历月的索引
        const lunarMonthIndex = this.date.getLunarMonthIndex()
        // 拼接农历月选项集
        return [this.date.lunarYear - start, lunarMonthIndex, this.date.lunarDayIndex, this.date.hour, this.date.min]
      }
    },
    
    // 设置日期Picker
    setPicker () {
      if (this.curPicker === 'solar') {
        let dateStr = this.getSolarValues()
        // 设置公历选项
        this.$refs.timePicker.setSolarValues(dateStr)
      }
      else {
        let values = this.getLunarValues()
        // 设置农历选项
        this.$refs.timePicker.setLunarValues(values)
      }
    },
    
    // 呼出日期选择器的回调
    pickTimeHandler () {
      // 根据当前选定的时间，设置选择器选中的项
      // 首次显示时，要延时设置Picker，避免错位BUG
      /*if (!this.hasPickerFirstShow) {
        this.hasPickerFirstShow = true
        // 延时时间至少要超过popup的动画时间
        clearTimeout(this.firstShowTimer)
        this.firstShowTimer = setTimeout(() => {
          this.setPicker()
        }, 500) 
      }
      else {
        // 第一次显示需要以上抹平BUG的恶心代码，之后正常设定时间即可
        this.setPicker()
      }*/
      
      this.$refs.timePicker.show(() => {
        this.setPicker()
      })
      

      // 呼出Picker
      // this.$refs.timePicker.show()
    },
    
    // 切换日期选择器的公、农历时的回调
    switchHandler (type) {
      this.curPicker = type
      // 切换时设置即将显示的Picker的时间，因为在未显示出来时就设置时间，会导致组件bug
      this.setPicker()
    },
  },
  beforeDestroy () {
    // 清除可能存在的定时器
    clearTimeout(this.firstShowTimer)
  }
}