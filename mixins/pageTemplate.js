/*
  模板页面通用部分混入
*/
const PADDING_STANDARD = 10

export default {
  props: {
    // 底部导航高度
    tabHeight: {
      type: [String, Number],
      default: 0
    },
    // 顶部占位高度
    topHeight: {
      type: [String, Number],
      default: 0
    },
    // 悬浮按钮大小
    floatSize: {
      type: [String, Number],
      default: 60
    },
    // 是否有标题栏
    hasNav: {
      type: Boolean,
      default: true
    }
  },
  data () {
    return {
      StatusBar: this.StatusBar,
      CustomBar: this.CustomBar,
      // 保存下页面常规padding间隔
      paddingStandard: PADDING_STANDARD
    }
  },
  computed: {    
    // 悬浮按钮的定位数据
    floatPos () {
      let pos = {}
      pos.bottom = parseInt(this.tabHeight) + PADDING_STANDARD
      pos.top = parseInt(this.topHeight) + PADDING_STANDARD
      if (this.hasNav) {
        pos.top = pos.top + this.CustomBar
      }
      return pos
    },
  },
}