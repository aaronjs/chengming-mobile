import utils from '@/utils/index.js'

let global = {
  // 全局参数对象
  productEnv: {},
  
  init () {
    // 获取后端全局参数
    utils.req.cloud('product-env', {}).then((res) => {
      console.log('获取参数成功', res)
      // 隐藏支付
      global.productEnv.hidePay = res.hidePay
    })
    .catch((e) => {
      console.log('获取参数失败', e)
      utils.msg.errToast(e)
    })
  }
}

export default global