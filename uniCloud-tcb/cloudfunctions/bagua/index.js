'use strict';
const actions = require('./actions.js')
const {
  log,
  error,
  utils
} = require('cm')

exports.main = async (event, context) => {
  try {
    const query = utils.common.solveParams(event)
    const action = query.action || ''
    const token = query.uniIdToken
    const params = query.params || {}
    
    let res = error('OK')
    
    // 校验token
    const tokenRes = await utils.common.checkToken(action, token, params)
    
    switch (action) {
      case 'saveBagua':
        // 保存八卦
        const saveRes = await actions.saveBagua(params)
        res.bagua = saveRes
        break
      case 'getBagua':
        // 读取八卦
        const data = await actions.getBagua(params)
        res.data = data
        break
      case 'deleteBagua':
        // 删除八卦
        await actions.deleteBagua(params)
        break
      default:
        return error('INVALID_REQUEST', '', false)
    }
    log.log(res, '输出结果')
    return res
  }
  catch (e) {
    log.error(e)
    return e
  }
};
