'use strict';

const actions = require('./actions.js')
const {
  log,
  error,
  utils
} = require('cm')

const freeMobiles = require('./freeMobiles.js')

exports.main = async (event, context) => {
  try {
    const query = utils.common.solveParams(event)
    const action = query.action || ''
    const token = query.uniIdToken
    const params = query.params || {}
    
    let res = error('OK')
    
    // 校验token
    const tokenRes = await utils.common.checkToken(action, token, params)
    
    // 满足条件的账户可以0元购买
    const mobile = tokenRes.userInfo.mobile
    if (freeMobiles.indexOf(mobile) >= 0) {
      params.price = 0
      log.log('手机号' + mobile + '满足条件，可0元购买', '特殊处理')
    }
    
    switch (action) {
      case 'getBaguaApp':
        const actionRes = await actions.getBaguaApp(params)
        res.orderInfo = actionRes.orderInfo
        res.baguaAppData = actionRes.baguaAppData
        break
      default:
        return error('INVALID_REQUEST', '', false)
    }
    log.log(res, '输出结果')
    return res
  }
  catch (e) {
    log.error(e)
    return e
  }
};
