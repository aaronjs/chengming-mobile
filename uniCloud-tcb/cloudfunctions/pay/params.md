参数字段说明
@param uniIdToken [String] 校验token，并获取uid的关键参数，除了某些(登录、注册)行为以外，一般需要用户在登录状态才能进行的操作，都必须校验token
@param action [String] 指定行为，具体取值如下
  getBaguaApp 购买八卦APP支付
@param params [Object] 携带的参数，根据action的不同有所不一样。说明如下：

getBaguaApp 特有参数params
@key uid [String] 不需要从前端传，可以通过token校验换取
@key payOptions [Object] 支付关键参数，具体说明如下
  @key provider [String] 初始化必须，支付平台，取值说明：wx微信/alipay支付宝
  @key appType [String] 初始化必须，前端传来的APP标识，后端根据这个字段获取APPID
  @key openid [String] 支付必须，用于确定平台用户
  @key tradeType [String] 支付必须，用于确定支付接口调用方式
@key description [String] 商品说明
@key duration [String/Number] 取值为数字时代表八卦APP的开通时长(单位为秒)，取值为字符串时有且仅有一种取值：forever，代表永久开通
@key price [Number] 需要支付的金额，单位为分
@key onTrial [Boolean] 是否为试用版，试用版每个用户只能领取一次