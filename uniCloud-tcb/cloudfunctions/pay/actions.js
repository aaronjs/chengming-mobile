const {
  log,
  config,
  utils,
  error,
  User,
  Order,
  Pay
} = require('cm')

const actions = {
  /*
    @section 供云函数直接调用的可执行action
  */
  async getBaguaApp (params) {
    // 先获取用户
    const user = await User.getUser({
      _id: params.uid
    })
    
    // 再生成订单
    let orderData = {}
    orderData.uid = params.uid
    orderData.description = params.description
    orderData.price = params.price
    orderData.startTime = utils.date.nowStamp()
    if (params.price === 0) {
      // 0元订单直接成功状态
      orderData.state = 'success'
    }
    else {
      // 需要走支付流程的订单要设置超时流程
      orderData.endTime = utils.date.nowStamp(config.pay.orderTimeout),
      orderData.state = 'pending'
    }
    // 其他参数
    orderData.options = {
      // 是否为试用版
      onTrial: params.onTrial,
      // 持续时间
      duration: params.duration
    }
    // 支付方式
    orderData.payOptions = params.payOptions
    const order = new Order(orderData)
    const insertRes = await order.insert()
    
    let res = {}
    if (params.price !== 0) {
      // 走支付流程，返回支付参数
      const pay = new Pay(params.payOptions)
      res.orderInfo = await pay.getOrder(order.data)
    }
    
    // 获取更新后的数据
    const updateData = await actions.getBaguaAppUpdate(user, order)
    res.baguaAppData = updateData
    
    return res
  },
  
  /*
    @section 各种公共方法
  */
  // 获取用户成功购买后的新APP数据
  async getBaguaAppUpdate (user, order) {
    let data = {}
    if (user.data.baguaAppData) {
      data = { ...user.data.baguaAppData }
    }
    
    const { onTrial, duration } = order.data.options
    if (onTrial) {
      // 试用版使用记录
      data.usedTrial = true
    }
    if (duration !== 'forever') {
      // 非永久有效，注意数据库时间单位是毫秒，参数持续时间的时间单位是秒
      const nowStamp = utils.date.nowStamp()
      if (data.hasOwnProperty('expiredTime') && nowStamp <= data.expiredTime) {
        // 还没过期，延长过期时间
        data.expiredTime = utils.date.stampOffset(data.expiredTime, duration)
      }
      else {
        // 已过期，从当前时间开始计算过期时间
        data.expiredTime = utils.date.stampOffset(nowStamp, duration)
      }
    }
    else {
      // forever为永久有效
      data.expiredTime = 'forever'
    }
    
    // 返回更新后的数据
    return data
  }
}

module.exports = actions