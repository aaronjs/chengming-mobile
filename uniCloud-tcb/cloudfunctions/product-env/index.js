'use strict'
exports.main = async (event, context) => {
  // 客户端从服务器端获取的参数
	return {
    code: 0,
    // 小程序过审期间，屏蔽一切支付功能，过审后，打开支付功能
    hidePay: false
  }
}
