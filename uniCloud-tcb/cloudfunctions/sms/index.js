'use strict';
// 测试短信发送的云函数
exports.main = async (event, context) => {
	//event为客户端上传的参数
  try {
    // 请注意使用自行申请的模板时必须传name字段，值为报备时填写的应用名称
    const res = await uniCloud.sendSms({
      smsKey: '375973ba53aed71842c115d7c46b906f',
      smsSecret: '1005450e22a7e11f9c1983347ff0a635',
      phone: '13269616293',
      templateId: 'uniID_code',
      data: {
        name: 'chengming-mobile',
        code: '123456',
        action: '注册',
        expMinute: '3',
      }
    })
    // 调用成功，请注意这时不代表发送成功
    return res
  } catch(err) {
    // 调用失败
    console.log(err.errCode)
    console.log(err.errMsg)
    return {
      code: err.errCode,
      msg: err.errMsg
    }
  }
};
