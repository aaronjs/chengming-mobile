const Common = require('../common/common.js')

const db = uniCloud.database()

class Order extends Common {
  constructor (data = {}) {
    super(data, Order.key)
  }

  // 根据查询条件，获取首个满足条件的订单
  static async getOrder (query) {
    const data = await Order.get(Order.key, query)
    return new Order(data[0], true)
  }
  
  // 根据查询条件，获取所有满足条件的订单
  static async getOrders (query) {
    const data = await Order.get(Order.key, query)
    
    const len = data.length
    let list = []
    for (let i = 0; i < len; i++) {
      list.push(new Order(data[i]), true)
    }
    return list
  }
}

Order.key = 'order'

module.exports = Order