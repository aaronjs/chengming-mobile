const dateUtils = {
  // 获取时间戳，offset为当前时间戳的偏移量，单位为秒
  nowStamp (offset = 0) {
    // 时间戳的单位是毫秒，offset单位是秒，偏移量若要转为毫秒需要乘以1000
    return new Date().getTime() + offset * 1000
  },
  
  // 时间戳偏移
  stampOffset (stamp, offset = 0) {
    // 时间戳的单位是毫秒，offset单位是秒，偏移量若要转为毫秒需要乘以1000
    return stamp + offset * 1000
  }
}

module.exports = dateUtils