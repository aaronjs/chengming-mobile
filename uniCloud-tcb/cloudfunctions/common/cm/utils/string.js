const stringUtils = {
  // 输出空格
  gap (num = 0) {
    let res = ''
    for (let i = 0; i < num; i++) {
      res = res + ' '
    }
    return res
  },
}

module.exports = stringUtils