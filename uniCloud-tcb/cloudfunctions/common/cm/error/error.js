const log = require('../log/log.js')

const errList = {
  // 0
  'OK': { code: 0, msg: '成功' },
  
  // 3开头，token类
  'TOKEN_TIMEOUT': { code: 30203, msg: 'token已过期' },
  'TOKEN_ERROR': { code: 30204, msg: 'token校验未通过' },
  
  // 8开头，基础功能
  'NO_USER_DATA': { code: 80301, msg: '未查询到用户信息' },
  
  // 9开头，共用码
  // 90 数据库系列
  'DB_ERROR': { code: 90001, msg: '数据库读写异常' },
  'NO_DB_DATA': { code: 90002, msg: '查找不到相关数据' },
  'DATA_ERROR': { code: 90003, msg: '非法操作数据' },
  // 91 支付系列
  'PAY_INIT_FAILED': { code: 91001, msg: '支付模块初始化失败' },
  'PAY_FAILED': { code: 91002, msg: '支付出错' },
  // 99 其他错误
  'INVALID_REQUEST': { code: 99403, msg: '非法访问' },
  'UNKOWN_ERROR': { code: 99999, msg: '未知错误' }
}

/*
  key 查找报错对象的key
  detail 错误stack，一般传入catch到的e
  isBreak 对于错误是否跳出，若为true返回Promise的reject对象，否则返回普通对象，默认状况下，捕捉到错误就要跳出
*/
module.exports = (key = 'OK', detail = '', isBreak = true) => {
  if (key === 'OK') {
    return { ...errList['OK'] }
  }
  
  let errorObj
  if (!errList[key]) {
    errorObj = { ...errList['UNKOWN_ERROR'] }
  }
  else {
    errorObj = { ...errList[key] }
  }
  errorObj.detail = detail
  
  if (isBreak) {
    return Promise.reject(errorObj)
  }
  return errorObj
}