const Common = require('../common/common.js')

const db = uniCloud.database()

// 单个对象一般表示单个用户
class User extends Common {
  constructor (data = {}) {
    super(data, User.key)
  }
  
  // 根据查询条件，获取首个满足条件的用户
  static async getUser (query) {
    const data = await User.get(User.key, query)
    return new User(data[0])
  }
  
  // 根据查询条件，获取所有满足条件的用户
  static async getUsers (query) {
    const data = await User.get(User.key, query)
    
    const len = data.length
    let list = []
    for (let i = 0; i < len; i++) {
      list.push(new User(data[i]))
    }
    return list
  }
}

// 静态变量定义
User.key = 'user'

module.exports = User