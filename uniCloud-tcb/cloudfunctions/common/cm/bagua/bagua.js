const Common = require('../common/common.js')

const db = uniCloud.database()

// 单个对象一般表示单个用户
class Bagua extends Common {
  constructor (data = {}) {
    super(data, Bagua.key)
  }
  
  // 根据复杂的查询条件，获取满足条件的局
  
  // 根据查询条件，获取首个满足条件的局
  static async getBagua (query) {
    const data = await Bagua.get(Bagua.key, query)
    return new Bagua(data[0], true)
  }
  
  // 根据查询条件，获取所有满足条件的局
  static async getBaguas (query) {
    const data = await Bagua.get(Bagua.key, query)
    
    const len = data.length
    let list = []
    for (let i = 0; i < len; i++) {
      list.push(new Bagua(data[i]), true)
    }
    return list
  }
}

// 静态变量定义
Bagua.key = 'bagua'

module.exports = Bagua