const logUtils = {
  output (type = 'LOG', title = '', data = {}) {
    console.log('------------------------------------------------' + type + ': ' + title + '------------------------------------------------')
    console.log(data)
    console.log('------------------------------------------------------------------------------------------------')
  },
  
  // 常规打印
  log (data = {}, title = '打印信息') {
    logUtils.output('LOG', title, data)
  },
  
  // 打印警告
  warn (data = {}, title = '警告信息') {
    logUtils.output('WARN', title, data)
  },
  
  // 打印数据库事项
  dbLog (data = {}, title = '数据库信息') {
    logUtils.output('DB', title, data)
  },
  
  // 错误打印
  error (e, title = '错误信息') {
    logUtils.output('ERROR', title, e)
  }
}

module.exports = logUtils