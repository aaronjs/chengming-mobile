const path = require('path')

module.exports = {
  // 订单超时时间，单位为秒，1800为30min
  orderTimeout: 1800,
  // 订单支付后通知地址
  notifyUrl: 'https://cmplanning-c116fd.service.tcloudbase.com/payback',
  
  // 微信支付配置
  wx: {
    // 商户号ID
    mchId: '1419250302',
    // 微信商户API秘钥
    key: 'OhWSymbw38qLANevjNaDLVrvlU6XclbO',
    // 商户API证书，退款时必须
    pfx: path.resolve(__dirname, './cert/wxPay/apiclient_cert.p12'),
  },
  
  // 各种APPid
  appId: {
    // 起局APP小程序的id
    baguaWxMp: 'wxa25ee4c54fa8112b'
  }
}