module.exports = {
  // 数据库表名
  tableNames: {
    // 用户表
    user: 'uni-id-users',
    // 订单表
    order: 'user-bagua-app-order',
    // 八卦局表
    bagua: 'bagua-options'
  }
}