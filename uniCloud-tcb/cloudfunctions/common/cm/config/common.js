// 常规设置
module.exports = {
  // 不需要验证token是否有效的行为
  noCheckActions: [
    // 来自user-center云函数的行为
    'register', 'checkToken', 'encryptPwd',
    'login', 'loginByWeixin', 'sendSmsCode',
		'setVerifyCode', 'loginBySms', 'loginByEmail',
  ]
}