'use strict';

const uniID = require('uni-id')
const {
  log,
  error,
  utils
} = require('cm')

exports.main = async (event) => {
	/* 如果你通过云函数Url访问
	 * 使用GET时参数位于event.queryStringParameters
	 * 使用POST时参数位于event.body
	 * 请自行处理上述场景
	 */  
  try {
    const query = utils.common.solveParams(event)
    const action = query.action || ''
    const token = query.uniIdToken
    const params = query.params || {}
    
    let res = error('OK')
    
    // 校验token
    const tokenRes = await utils.common.checkToken(action, token, params)
    
    switch (action) {
      case 'register':
        res = await uniID.register(params)
        break
      case 'login':
        res = await uniID.login({
          ...params,
          // 不指定queryField的情况下只会查询username
          queryField: ['username', 'email', 'mobile']
        })
        break
      case 'checkToken':
        // checkToken和login类似，可看作自动登录功能
        // 注意1.1.0版本会返回userInfo，请不要返回全部信息给客户端
        /*
        const checkTokenRes = await uniID.checkToken(event.uniIdToken)
        res = {
          code: checkTokenRes.code,
          msg: checkTokenRes.msg
        }
        */
        res = await uniID.checkToken(event.uniIdToken)
        break
      case 'logout':
        res = await uniID.logout(event.uniIdToken)
        break
      case 'updatePwd':
        res = await uniID.updatePwd(params)
        break
      case 'setAvatar':
        res = await uniID.setAvatar(params)
        break
      case 'bindMobile':
        res = await uniID.bindMobile(params)
        break;
      case 'unbindMobile':
        res = await uniID.unbindMobile(params)
        break
      case 'bindEmail':
        res = await uniID.bindEmail(params)
        break
      case 'unbindEmail':
        res = await uniID.unbindEmail(params)
        break
      case 'loginByWeixin':
        res = await uniID.loginByWeixin(params.code)
        break
      case 'bindWeixin':
        res = await uniID.bindWeixin(params)
        break
      case 'unbindWeixin':
        res = await uniID.unbindWeixin(params.uid)
        break
      case 'loginByAlipay':
        res = await uniID.loginByAlipay(params.code)
        break
      case 'bindAlipay':
        res = await uniID.bindAlipay(params)
        break
      case 'unbindAlipay':
        res = await uniID.unbindAlipay(params.uid)
        break
      case 'resetPwd':
        res = await uniID.resetPwd({
          uid: params.uid,
          password: '123456'
        })
        break
      case 'encryptPwd':
        const password = await uniID.encryptPwd('123456')
        res = {
          code: 0,
          msg: '密码加密完成',
          password
        }
        break
      case 'sendSmsCode':
        res = await uniID.sendSmsCode(params)
        break
      case 'setVerifyCode':
        res = await uniID.setVerifyCode(params)
        break
      case 'loginBySms':
        res = await uniID.loginBySms(params)
        break
      case 'loginByEmail':
        res = await uniID.loginByEmail(params)
        break
      case 'updateUser':
        res = await uniID.updateUser(params)
        break
      case 'setUserInviteCode':
        res = await uniID.setUserInviteCode(params)
        break
      case 'acceptInvite':
        res = await uniID.acceptInvite(params)
        break
      default:
        return error('INVALID_REQUEST', '', false)
    }
    log.log(res, '输出结果')
    return res
  }
  catch (e) {
    log.error(e)
    return e
  }
};
