// 路由名称
const routes = {
  // 起局首页
  baguaIndex: 'pages/index/index',
  // 万年历页面
  calendar: '/pages/calendar/calendar',
  // 购买页面
  buy: '/pages/buy/buy',
  // 登录页面
  login: '/pages/login/login',
  // 绑定手机页面
  bindMobile: '/pages/bindMobile/bindMobile',
  // 八卦页面
  bagua: '/pages/bagua/bagua',
  // 起局排盘页面
  baguaRecords: '/pages/baguaRecords/baguaRecords',
  // 外部网页页面
  webView: '/pages/webViewPage/webViewPage',
  // 应用配置页面
  config: '/pages/config/config'
}

export default routes