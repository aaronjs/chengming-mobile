import appConfig from './appConfig.js'
import sysConfig from './sysConfig.js'
import storageUtils from '../common/storage.js'

const CONFIG_CACHE_KEY = '__config__'

let config = {
  // 初始化应用设定
  app: appConfig,
  
  set (key, val) {
    config.app[key] = val
    storageUtils.set(CONFIG_CACHE_KEY, config.app).then(() => {
      console.log('设置应用配置成功', config.app)
    })
    .catch((e) => {
      console.log(e)
    })
  },
  
  load () {
    storageUtils.get(CONFIG_CACHE_KEY).then((res) => {
      console.log('读取应用配置记录', res)
      if (res && typeof res === 'object') {
        config.app = res
      }
    })
    .catch((e) => {
      console.log(e)
    })
  },
  
  // 保存系统设定
  sys: sysConfig,
}

// 初始化,读取已保存的应用设定

export default config