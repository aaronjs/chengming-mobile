
// 应用设定,用户可以自行更改,存储在storage中
const appConfig = {
  // 是否显示阴阳角标
  isYinyangShow: false,
  // 是否显示五行颜色
  isWuxingColor: false,
  // 遁甲排盘是否显示地支
  isDunjiaZhiShow: true,
  // 六壬排盘是否显示地支
  isLiurenZhiShow: true,
  
  // 选择器开始年
  timePickerStart: 1900,
  // 选择器终止年
  timePickerEnd: 2100,
  
  // 时令颜色，旺相休囚死
  seasonStateColors: ['red', 'yellow', 'forest', 'purple', 'grey'],
  
  // 每次获取排盘的数量
  baguaRecordsOnceNum: 20,
}

export default appConfig