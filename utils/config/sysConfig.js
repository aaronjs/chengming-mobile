// 系统设定,如存储桶等信息,不可更改

let sysConfig = {
  // 版本号
  version: '1.0.3',
  // 重要路径设置
  // 对象存储路径,临时用 承明策划的,以后修改
  filePath: 'https://chengming-1255355961.cos.ap-beijing.myqcloud.com/',
  // 服务接口路径
	apiPath: 'https://cmplaning.cn/server/',
  // 应用更新服务路径
  updatePath: 'app/updateTianzun.php',
  // 应用主路径
	clientPath: '',
  // 临时,APP的路径,待新项目成型后废除
  appPath: '',
  
  // 短信发送间隔，单位秒
  smsColdTime: 60,
  
  // 腾讯云COS设置
  cos: {
    bucket: '',
    region: ''
  },
  
  // 高德地图设置
  maps: {
    apiPath: 'https://restapi.amap.com/v3/',
    key: '5aa17770826fbeaf9f16570e0c36a662',
  },
  
  // 天气服务设置
  weathers: {
    // H5插件地址，要注意添加lc定位字段
    h5Path: 'https://m.cmplanning.cn/weather.html?bg=1&md=0123456&key=714d87687b954dffbd28e0bf0b4c9341',
  }
}

export default sysConfig