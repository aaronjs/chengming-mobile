// 引入常规方法
import objUtils from './common/obj.js'
import stringUtils from './common/string.js'
import commonUtils from './common/common.js'
import requestUtils from './common/request.js'
import storageUtils from './common/storage.js'
import calculateUtils from './common/calculate.js'
import massageUtils from './common/massage.js'
import error from './common/error.js'
// 引入路由方法
import routerUtils from './router/router.js'
// 引入config中的工具类
import config from './config/config.js'
// 引入常数
import consts from './consts.js'

const utils = {
  obj: objUtils,
  str: stringUtils,
  common: commonUtils,
  storage: storageUtils,
  cal: calculateUtils,
  msg: massageUtils,
  req: requestUtils,
  err: error,
  
  router: routerUtils,
  
  config: config,
  
  consts: consts
}

export default utils