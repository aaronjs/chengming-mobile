// 定义一些常量
const consts = {
  colors: {
    primary: '#007aff',
    success: '#4cd964',
    warning: '#fbbd08',
    error: '#dd524d'
  }
}

export default consts