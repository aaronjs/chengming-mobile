// 用于进行逻辑判断
class Judge {
  constructor (con = () => {}) {
    this.result = this.cal(con)
  }
  
  /*
    执行表达式获取结果
  */
  cal (con = () => {}) {
    let sign = 0
    if (typeof con === 'function') {
      sign = con()
      sign = sign === true || sign === 1 ? 1 : 0
    }
    else if (typeof con === 'boolean' || typeof con === 'number') {
      sign = con ? 1 : 0
    }
    return sign
  }
  
  /*
    进行与的逻辑判断
    con 条件，若返回true，则该表达式结果为真，否则为假
  */
  and (con = () => {}) {
    const sign = this.cal(con)
    this.result = this.result & sign
    return this
  }
  
  /*
    进行或的逻辑判断
  */
  or (con = () => {}) {
    const sign = this.cal(con)
    this.result = this.result | sign
    return this
  }
  
  /*
    异或运算，两位只有一位为1时
  */
  xor (con = () => {}) {
    const sign = this.cal(con)
    this.result = this.result ^ sign
    return this
  }
  
  /*
    非运算，直接取反
  */
  not () {
    this.result = this.result ? 0 : 1
    return this
  }
  
  /*
    结束判断，获取逻辑判断结果
  */
  end () {
    return this.result
  }
}

export default Judge