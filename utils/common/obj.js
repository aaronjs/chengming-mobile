
// 对象方法
const objUtils = {
  /* @section 类别判断 */
  isDate (obj) {
    // Invalid Date 不合法，将不被识别
    return Object.prototype.toString.call(obj) === '[object Date]' && !isNaN(obj.getTime())
  },
  
  isNumber (obj) {
    // NaN不合法，将不被识别
    return Object.prototype.toString.call(obj) === '[object Number]' && !isNaN(obj)
  },
  
  isObject (obj) {
    return Object.prototype.toString.call(obj) === '[object Object]'
  },
  
  isString (obj) {
    return Object.prototype.toString.call(obj) === '[object String]'
  },
  
  isNull (obj) {
    return Object.prototype.toString.call(obj) === '[object Null]'
  },
  
  isUndefined (obj) {
    return Object.prototype.toString.call(obj) === '[object Undefined]'
  },
  
  isArray (obj) {
    return Object.prototype.toString.call(obj) === '[object Array]'
  },
  
  /* @section 常规对象操作方法 */
  // 通过JSON实现的对象深度拷贝，但只能拷贝对象的属性
  jsonClone (obj) {
    return JSON.parse(JSON.stringify(obj))
  },
  
  // 对于一个Object，补齐指定的key和默认值后返回
  fillObject (obj, options = {}) {
    if (!objUtils.isObject(obj)) {
      obj = {}
    }
    
    if (!objUtils.isObject(options)) {
      return
    }
    
    for (let key in options) {
      if (!obj.hasOwnProperty(key)) {
        obj[key] = options[key]
      }
    }
  },
  
  // 对于对象数组，对其中的每一个字段，补齐指定的key和默认值后返回
  fillObjectArr (arr, options = {}) {
    if (!objUtils.isArray(arr)) {
      arr = []
    }
    
    if (!objUtils.isObject(options)) {
      return
    }
    
    const len = arr.length
    for (let i = 0; i < len; i++) {
      objUtils.fillObject(arr[i], options)
    }
  }
}

export default objUtils