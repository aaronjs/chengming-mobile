const ERR_LIST = {
  'OK': { errno: '000', errmsg: '成功' },
  
  // 系统错误
  'STORAGE_SET_FAILED': { errno: '001', errmsg: '缓存数据出现错误' },
  'STORAGE_REMOVE_FAILED': { errno: '002', errmsg: '删除缓存出错' },
  'STORAGE_INFO_FAILED': { errno: '003', errmsg: '获取缓存信息失败' },
  'INVALID_STORAGE_GETTING': { errno: '004', errmsg: '获取缓存方式不合法' },
  'ROUTE_FAILED': { errno: '010', errmsg: '页面跳转出错' },
  'ROUTE_NO_EXISTS': { errno: '011', errmsg: '不存在的路由' },
  'CLOUD_FUNC_ERROR': { errno: '020', errmsg: '云函数调用错误' },
  'REQUEST_ERROR': { errno: '021', errmsg: '网络请求错误' },
  
  // 用户错误
  'UCENTER_ERROR': { errno: '101', errmsg: '请求用户中心出现问题' },
  'LOGIN_CODE_ERROR': { errno: '102', errmsg: '获取登录凭证出现问题' },
  'GET_INFO_ERROR': { errno: '103', errmsg: '获取用户信息出现错误' },
  'PLATFORM_LOGIN_ERROR': { errno: '104', errmsg: '平台登录出现问题' },
  'LOGIN_CHECK_FAILED': { errno: '105', errmsg: '操作失败，用户需要先登录' },
  'PAY_ERROR': { errno: '106', errmsg: '用户支付出现问题' },
  
  // 地址定位错误
  'LOCATE_ERROR': { errno: '901', errmsg: '获取定位时出现错误' },
  'LOCATE_API_ERROR': { errno: '902', errmsg: '获取地理信息出现错误' },
  
  
  
  'UNKOWN_ERROR': { errno: '999', errmsg: '其他错误' }  
}
  
const error = (key = 'OK', detail = '') => {
  let errorObj
  if (!ERR_LIST[key]) {
    errorObj = { ...ERR_LIST['UNKOWN_ERROR'] }
  }
  else {
    errorObj = { ...ERR_LIST[key] }
  }
  errorObj.detail = detail
  return errorObj  
}

export default error