import consts from '../consts.js'

const messageUtils = {
  // 简化版的uni.showToast
  toast (msg = '', icon = 'none') {
    uni.showToast({
      title: msg,
      icon: icon
    })
  },
  
  // 专门打印错误对象
  errToast (err) {
    if (err.detail) {
      // 优先打印服务器返回的错误信息
      if (err.detail.msg) {
        messageUtils.toast(err.detail.msg)
        return
      }
    }
    
    if (err.errmsg) {
      // 其次打印本应用的错误信息
      messageUtils.toast(err.errmsg)
      return
    }
    
    messageUtils.toast('未知错误')
  },
  
  // 简化版的showModal
  modal (obj) {
    return new Promise((resolve, reject) => {
      const type = obj.hasOwnProperty('type') ? obj.type : 'primary'
      const options = {
        title: obj.hasOwnProperty('title') ? obj.title : '',
        content: obj.hasOwnProperty('content') ? obj.content : '',
        showCancel: obj.hasOwnProperty('showCancel') && !obj.showCancel ? false : true,
        cancelText: obj.hasOwnProperty('cancelText') ? obj.cancelText : '取消',
        confirmText: obj.hasOwnProperty('confirmText') ? obj.cancelText : '确定',
        confirmColor: consts.colors[type] ? consts.colors[type] : consts.colors.primary,
      }
      uni.showModal({
        ...options,
        success: (res) => {
          // 返回用户是否点击确定
          resolve(res.confirm)
        },
        fail: (e) => {
          console.log(e)
        }
      })
    })
  },
  
  // 简化版的showLoading
  loading (txt) {
    uni.showLoading({
      title: txt,
      mask: true
    })
  },
  
  hideLoading () {
    uni.hideLoading()
  }
}

export default messageUtils