import Zhi from '../bagua/zhi.js'

import dictionary from '../bagua/dictionary.js'

// 根据农历日定太阴
const taiyinStartList = [
  9, 9, 8, 8, 7, 7, 7, 6, 6, 5,
  5, 5, 4, 4, 3, 3, 3, 2, 2, 1,
  1, 1, 0, 0, 11, 11, 11, 10, 10, 9, 9
]
// 旺相休囚死列表
const seasonPowerList = [
  // 木月，春季
  ['旺', '相', '死', '囚', '休'],
  // 火月，夏季
  ['休', '旺', '相', '死', '囚'],
  // 土月，四季交替
  ['囚', '休', '旺', '相', '死'],
  // 金月，秋季
  ['死', '囚', '休', '旺', '相'],
  // 水月，冬季
  ['相', '死', '囚', '休', '旺'],
]
const seasonStateList = ['旺', '相', '休', '囚', '死']

// 神煞
class TimeGod {
  constructor () {}
  
  /* @section 根据局的具体需要计算神煞 */
  
  // 计算四柱驿马
  initTimeHorses (date) {
    this.timeHorses = []
    const len = date.ganzhi.length
    for (let i = 0; i < len; i++) {
      const horse = this.horse(date.ganzhi[i])
      this.timeHorses.push(horse)
    }
  }
  
  // 计算四柱空亡
  initTimeEmpty (date) {
    this.timeEmpty = []
    const len = date.ganzhi.length
    for (let i = 0; i < len; i++) {
      const empty = this.empty(date.ganzhi[i])
      this.timeEmpty.push(empty)
    }
  }
  
  // 计算四柱贵神
  initTimeGuiGods (date) {
    this.timeGuiGod = []
    const len = date.ganzhi.length
    for (let i = 0; i < len; i++) {
      const guiGod = this.guiGod(date.ganzhi[i])
      this.timeGuiGod.push(guiGod)
    }
  }
  
  // 计算太阴
  initTaiyin (date) {
    const zhiIndex = taiyinStartList[date.lunarDayIndex]
    this.taiyin = new Zhi(zhiIndex, {
      description: dictionary.TAIYIN
    })
  }
  
  // 计算旺相休囚死列表
  initSeasonPower (date) {
    const index = date.ganzhi[1].zhi.wuxing
    this.seasonPower = seasonPowerList[index]
  }
  
  // 根据一个对象的五行元素，以及当前月令，获取时令状态
  static getSeasonState (wuxing, seasonIndex) {
    const seasonPower = seasonPowerList[seasonIndex]
    const seasonState = seasonPower[wuxing]
    return seasonStateList.indexOf(seasonState)
  }
  
  // 计算关键干支驿马
  initKeyHorse (ganzhi) {
    this.keyHorse = this.horse(ganzhi)
  }
  
  // 计算关键干支空亡
  initKeyEmpty (ganzhi) {
    this.keyEmpty = this.empty(ganzhi)
  }
  
  
  /* @section 计算神煞的基本方法 */
  /*
    计算驿马
    ganzhi 参照干支
    寅午戌在申 亥卯未在巳 申子辰在寅 巳酉丑在亥
  */
  horse (ganzhi) {
    const horseIndexList = [2, 11, 8, 5]
    let zhiIndex = ganzhi.zhi.index
    zhiIndex = horseIndexList[zhiIndex % horseIndexList.length]
    return new Zhi(zhiIndex, {
      description: ganzhi.description + dictionary.HORSE
    })
  }
  
  // 计算空亡
  empty (ganzhi) {
    const xunZhi = ganzhi.xun.name[1]
    const zhiIndex = Zhi.zhiList.indexOf(xunZhi)
    const zhiLen = Zhi.zhiList.length
    let result = []
    result.push((zhiIndex + zhiLen - 1) % zhiLen)
    result.push((zhiIndex + zhiLen - 2) % zhiLen)
    return result
  }
  
  /* 
    计算贵人
    ganzhi为参照干支
    isObjReturn 决定是返回对象还是单纯返回地支索引
    甲戊庚牛羊 乙己鼠猴乡 丙丁猪鸡位 壬癸蛇兔藏 六辛逢马虎
  */
  guiGod (ganzhi, isObjReturn = true) {
    const guiGodList = [[1, 7], [0, 8], [11, 9], [11, 9], [1, 7], [0, 8], [1, 7], [6, 2], [5, 3], [5, 3]]
    if (!isObjReturn) {
      return guiGodList[ganzhi.gan.index]
    }
    return guiGodList[ganzhi.gan.index].map((item, index) => {
      return new Zhi(item, {
        description: ganzhi.description + dictionary.TIANYIGUIGOD
      })
    })
  }
}

Object.assign(TimeGod, {
  seasonStateList
})

export default TimeGod