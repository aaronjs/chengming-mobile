/*
  txtPos规则
  未指定 - 0 1 txt
  center - 0 txt 1
  belonging - 0 是 1 的 txt
*/

const relationSettings = {
  // 五行关系
  // 1 对于 0 是什么关系，1为主，0位客
  wuxing: {
    key: 'wuxing',
    group: 'A',
    num: 2,
    list: ['克', '受克于', '生', '受生于', '同'],
    offsetList: [3, 2, 4, 1, 0],
    method (objs) {
      if ((!objs[0].wuxing && objs[0].wuxing !== 0) || (!objs[1].wuxing && objs[1].wuxing !== 0)) {
        return null
      }
      
      const len = relationSettings.wuxing.list.length
      const delta = objs[1].wuxing >= objs[0].wuxing ? objs[1].wuxing - objs[0].wuxing : objs[1].wuxing + len - objs[0].wuxing
      const index = relationSettings.wuxing.offsetList.indexOf(delta)
      
      return {
        objs,
        group: relationSettings.wuxing.group,
        txt: relationSettings.wuxing.list[index],
        txtIndex: index,
        model: '$1 $txt $0'
      }
    }
  },
  // 阴阳关系
  yinyang: {
    key: 'yinyang',
    group: 'C',
    num: 2,
    list: ['阴阳相异', '阴阳相同'],
    method (objs) {
      if ((!objs[0].yinyang && objs[0].yinyang !== 0) || (!objs[1].yinyang && objs[1].yinyang !== 0)) {
        return null
      }
      
      let type = '阴阳相异'
      if (objs[0].yinyang === objs[1].yinyang) {
        type = '阴阳相同'
      }
      return {
        objs,
        group: relationSettings.yinyang.group,
        txt: type,
        txtIndex: relationSettings.yinyang.list.indexOf(type),
        model: '$0 $1 $txt'
      }
    }
  },
  
  // 地支相刑
  xing: {
    key: 'xing',
    // A代表排列,C代表组合
    group: 'C',
    // 对象数
    num: 2,
    // 类型限制
    types: ['Zhi', 'Zhi'],
    // 参考列表
    list: ['卯', '戌', '巳', '子', '辰', '申', '午', '丑', '寅', '酉', '未', '亥'],
    // 击刑类型
    typeList: ['无礼', '恃势', '无恩', '无礼', '自刑', '无恩', '自刑', '恃势', '无恩', '自刑', '恃势', '自刑'],
    // 判定方法
    method (objs) {
      if (relationSettings.xing.list[objs[0].index] === objs[1].name) {
        // 返回对象说明判定成功,否则判定失败
        let result = {
          objs,
          group: relationSettings.xing.group,
        }
        const index = objs[0].index
        const typeList = relationSettings.xing.typeList
        if (typeList[objs[0].index] === '无礼' || typeList[objs[0].index] === '自刑') {          
          // txtPos未指定,关键字会处在尾部
          // 指定为center,关键字会在中部
          result.txt = `相刑(${typeList[index]})`
          result.model = '$0 $1 $txt'
        }
        else {
          // txtPos未指定,关键字会处在尾部
          // 指定为center,关键字会在中部
          result.txtPos = 'center'
          result.txt = `刑(${typeList[index]})`
          result.model = '$0 $txt $1'
        }
        return result
      }
      return null
    }
  },
  // 地支相冲
  zhiChong: {
    key: 'zhiChong',
    group: 'C', 
    types: ['Zhi', 'Zhi'],
    num: 2,
    list: ['午', '未', '申', '酉', '戌', '亥', '子', '丑', '寅', '卯', '辰', '巳'],
    method (objs) {
      if (relationSettings.zhiChong.list[objs[0].index] === objs[1].name) {
        // 返回对象说明判定成功,否则判定失败
        return {
          objs,
          group: relationSettings.zhiChong.group,
          txt: '相冲',
          model: '$0 $1 $txt'
        }
      }
      return null
    }
  },
  // 地支相破
  po: {
    key: 'po',
    group: 'C',
    types: ['Zhi', 'Zhi'],
    num: 2,
    list: ['酉', '辰', '亥', '午', '丑', '申', '卯', '戌', '巳', '子', '未', '寅'],
    method (objs) {
      if (relationSettings.po.list[objs[0].index] === objs[1].name) {
        // 返回对象说明判定成功,否则判定失败
        return {
          objs,
          group: relationSettings.po.group,
          txt: '相破',
          model: '$0 $1 $txt'
        }
      }
      return null
    }
  },
  // 地支相害
  hai: {
    key: 'hai',
    group: 'C',
    types: ['Zhi', 'Zhi'],
    num: 2,
    list: ['未', '午', '巳', '辰', '卯', '寅', '丑', '子', '亥', '戌', '酉', '申'],
    method (objs) {
      if (relationSettings.hai.list[objs[0].index] === objs[1].name) {
        // 返回对象说明判定成功,否则判定失败
        return {
          objs,
          group: relationSettings.hai.group,
          txt: '相害',
          model: '$0 $1 $txt'
        }
      }
      return null
    }
  },
  // 鬼，我是你鬼
  gui: {
    key: 'gui',
    group: 'C',
    num: 2,
    method (objs) {
      const method = relationSettings.tenGod.method
      const relation = method([objs[0], objs[1]])
      if (relation === null) {
        return null
      }
      if (relation.txt === '七煞') {
        // 返回对象说明判定成功,否则判定失败
        return {
          objs,
          group: relationSettings.gui.group,
          txt: '鬼',
          model: '$0 是 $1 的 $txt',
          txtPos: 'belonging'
        }
      }
      return null
    }
  },
  // 地支六合
  zhiHe: {
    type: 'zhiHe',
    types: ['Zhi', 'Zhi'],
    group: 'C', 
    num: 2,
    list: ['丑', '子', '亥', '戌', '酉', '申', '未', '午', '巳', '辰', '卯', '寅'],
    method (objs) {
      if (relationSettings.zhiHe.list[objs[0].index] === objs[1].name) {
        // 返回对象说明判定成功,否则判定失败
        return {
          objs,
          group: relationSettings.zhiHe.group,
          txt: '相合',
          model: '$0 $1 $txt'
        }
      }
      return null
    }
  },
  // 地支三合
  zhiThreeHe: {
    key: 'zhiThreeHe',
    group: 'C',
    types: ['Zhi', 'Zhi', 'Zhi'],
    num: 3,
    // 顺序为了算法妥协
    list: [
      '寅午戌', '亥卯未', '巳酉丑', '申子辰'
    ],
    method (objs) {
      const len = relationSettings.zhiThreeHe.list.length
      for (let i = 0; i < len; i++) {
        const desStr = relationSettings.zhiThreeHe.list[i]
        
        let posStr = ''
        const objsLen = objs.length
        for (let j = 0; j < objsLen; j++) {
          const index = desStr.indexOf(objs[j].name)
          if (index < 0) {
            // -1当做x
            posStr = posStr + 'x'
          }
          else {
            posStr = posStr + index
          }
        }

        if (posStr.indexOf('0') >= 0 && posStr.indexOf('1') >= 0 && posStr.indexOf('2') >= 0) {
          // 完全相等,完美三合
          return {
            objs,
            group: relationSettings.zhiThreeHe.group,
            txt: '三合',
            model: '$0 $1 $2 $txt'
          }
        }
      }
      return null
    }
  },
  // 地支三会
  zhiHui: {
    key: 'zhiHui',
    group: 'C',
    types: ['Zhi', 'Zhi', 'Zhi'],
    num: 3,
    // 顺序为了算法妥协
    list: [
      '寅卯辰', '巳午未', '申酉戌', '亥子丑'
    ],
    method (objs) {
      const len = relationSettings.zhiHui.list.length
      for (let i = 0; i < len; i++) {
        const desStr = relationSettings.zhiHui.list[i]
        
        let posStr = ''
        const objsLen = objs.length
        for (let j = 0; j < objsLen; j++) {
          const index = desStr.indexOf(objs[j].name)
          if (index < 0) {
            // -1当做x
            posStr = posStr + 'x'
          }
          else {
            posStr = posStr + index
          }
        }

        if (posStr.indexOf('0') >= 0 && posStr.indexOf('1') >= 0 && posStr.indexOf('2') >= 0) {
          // 完全相等,完美三会
          return {
            objs,
            group: relationSettings.zhiHui.group,
            txt: '三会',
            model: '$0 $1 $2 $txt'
          }
        }
      }
      return null
    }
  },
  
  // 天干五合
  ganHe: {
    key: 'ganHe',
    group: 'C', 
    types: ['Gan', 'Gan'],
    num: 2,
    list: ['己', '庚', '辛', '壬', '癸', '甲', '乙', '丙', '丁', '戊'],
    method (objs) {
      if (relationSettings.ganHe.list[objs[0].index] === objs[1].name) {
        // 返回对象说明判定成功,否则判定失败
        return {
          objs,
          group: relationSettings.ganHe.group,
          txt: '相合',
          model: '$0 $1 $txt'
        }
      }
      return null
    }
  },
  // 天干相冲
  ganChong: {
    key: 'ganChong',
    group: 'C', 
    types: ['Gan', 'Gan'],
    num: 2,
    list: ['庚', '辛', '壬', '癸', null, null, '甲', '乙', '丙', '丁'],
    method (objs) {
      if (relationSettings.ganChong.list[objs[0].index] === objs[1].name) {
        // 返回对象说明判定成功,否则判定失败
        return {
          objs,
          group: relationSettings.ganChong.group,
          txt: '相冲',
          model: '$0 $1 $txt'
        }
      }
      return null
    }
  },
  // 门迫
  doorBroken: {
    key: 'doorBroken',
    group: 'C', 
    types: ['DunjiaDoor', 'Bagua'],
    num: 2,
    method (objs) {
      const method = relationSettings.wuxing.method
      const relation = method([objs[0], objs[1]])
      if (relation === null) {
        return null
      }
      if (relation.txt === '受克于') {
        // 返回对象说明判定成功,否则判定失败
        return {
          objs,
          group: relationSettings.doorBroken.group,
          txt: '门迫',
          model: '$0 在 $1 $txt'
        }
      }
      return null
    }
  },
  // 遁甲击刑
  dunjiaXing: {
    key: 'dunjiaXing',
    group: 'C',
    types: ['Gan', 'Bagua'],
    num: 2,
    list: [null, null, null, null, '震', '坤', '艮', '离', '巽', '巽'],
    method (objs) {
      if (relationSettings.dunjiaXing.list[objs[0].index] === objs[1].name) {
        // 返回对象说明判定成功,否则判定失败
        return {
          objs,
          group: relationSettings.dunjiaXing.group,
          txt: '击刑',
          model: '$0 在 $1 $txt'
        }
      }
      return null
    }
  },
  // 遁甲入墓
  dunjiaToob: {
    key: 'dunjiaToob',
    group: 'C',
    types: ['Gan|DunjiaGod', 'Bagua'],
    num: 2,
    list: ['坤', '乾', '乾', '艮', '乾', '艮', '艮', '巽', '巽', '坤'],
    method (objs) {
      if (objs[0].types.indexOf('Gan') < 0 && objs[0].name !== '值符') {
        return null
      }
      if (relationSettings.dunjiaToob.list[objs[0].index] === objs[1].name
        || (objs[0].name === '值符' && objs[1].name === '坤')
      ) {
        // 返回对象说明判定成功,否则判定失败
        return {
          objs,
          group: relationSettings.dunjiaToob.group,
          txt: '入墓',
          model: '$0 在 $1 $txt'
        }
      }
      return null
    }
  },
  
  // 十神关系
  // 计算目标: obj[0]是obj[1]的什么十神
  tenGod: {
    key: 'tenGod',
    group: 'A',
    num: 2,
    list: ['正财', '偏财', '正官', '七煞', '伤官', '食神', '正印', '偏印', '劫财', '比肩'],
    simList: ['财', '才', '官', '杀', '伤', '食', '印', '枭', '劫', '比'],
    method (objs) {
      const wuxingMethod = relationSettings.wuxing.method
      const yinyangMethod = relationSettings.yinyang.method
      const wuxingRelation = wuxingMethod([objs[0], objs[1]])
      const yinyangRelation = yinyangMethod([objs[0], objs[1]])
      if (wuxingRelation === null || yinyangRelation === null) {
        return null
      }

      const index = wuxingRelation.txtIndex * 2 + yinyangRelation.txtIndex
      return {
        objs,
        group: relationSettings.tenGod.group,
        txt: relationSettings.tenGod.list[index],
        simTxt: relationSettings.tenGod.simList[index],
        model: '$0 是 $1 的 $txt',
      }
    }
  },
  // 十二长生关系
  twelveState: {
    key: 'twelveState',
    group: 'A',
    types: ['Gan|Zhi', 'Zhi'],
    num: 2,
    list: ['长生', '沐浴', '冠带', '临官', '帝旺', '衰', '病', '死', '墓', '绝', '胎', '养'],
    method (objs) {
      const len = objs[1].nameList.length
      let offset = 0
      if (objs[0].types.indexOf('Gan') >= 0) {
        // 天干与地支的十二长生，阳干顺，阴干逆
        if (objs[0].isTwelveStateClock) {
          offset = objs[1].index >= objs[0].bornZhi ? objs[1].index - objs[0].bornZhi : objs[1].index + len - objs[0].bornZhi
        }
        else {
          offset = objs[1].index <= objs[0].bornZhi ? objs[0].bornZhi - objs[1].index : objs[0].bornZhi + len - objs[1].index
        }
      }
      else {
        // 地支之间的十二长生
        offset = objs[1].index >= objs[0].bornZhi ? objs[1].index - objs[0].bornZhi : objs[1].index + len - objs[0].bornZhi
      }

      return {
        objs,
        group: relationSettings.twelveState.group,
        txt: relationSettings.twelveState.list[offset],
        model: '$0 是 $1 的 $txt',
      }
    }
  }
}

export default relationSettings