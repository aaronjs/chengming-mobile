import utils from '@/utils/index.js'

const urlHead = utils.config.sys.maps.apiPath

const api = {
  userKey: utils.config.sys.maps.key,
  
  // 逆地理编码
  regeo: {
    path: urlHead + 'geocode/regeo',
    method: 'GET'
  },
  // 地理编码
  geo: {
    path: urlHead + 'geocode/geo',
    method: 'GET'
  },
  // 天气
  weather: {
    path: urlHead + 'weather/weatherInfo',
    method: 'GET'
  }
}


export default api