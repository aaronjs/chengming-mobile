import Dunjia from './dunjia.js'

// 时局的类型
const typeList = ['year', 'month', 'day', 'hour', 'min']

class TimeDunjia extends Dunjia {
  constructor(options = {}) {
    super(options)
    
    this.types.push('TimeDunjia')
  }
  
  // 重写，还要初始化二十四山向
  initStruct () {
    // 初始化九宫八卦
    this.palaces = this.initPalaces()
    // 初始化地支十二神
    this.zhiPalaces = this.initZhiPalaces()
    // 初始化二十四山向
    this.mountainPalaces = this.initMountainPalaces()
    // 初始化太乙十六神
    this.taiyiPalaces = this.initTaiyiPalaces()
    
    // 联立九宫和地支
    this.connectZhiPalaces()
    // 联立九宫和太乙
    this.connectTaiyiPalaces()
  }
  
  // 重写起干支的方法
  initGanzhi () {
    let index = typeList.indexOf(this.options.timeDunjiaType)
    // 默认为时局
    if (index < 0) { index = 3 }
    
    this.keyGanzhi = this.date.ganzhi[index]
    // 定局干支一共有几柱
    this.keyGanzhiIndex = index
  }
  
  // 重写定局数的方法
  initNum () {
    let index = this.keyGanzhiIndex
    
    let count = 0
    let countList = []
    const len = Dunjia.palacesBaguaList.length
    for (let i = 0; i <= index; i++) {
      let num = null
      if (i === 1) {
        // 月柱加农历月月建
        num = this.date.lunarMonthSign
      }
      else if (i === 2) {
        // 日柱加农历日数
        num = this.date.lunarDayIndex + 1
      }
      else {
        // 其他情况都是加地支序数
        num = this.date.ganzhi[i].zhi.index + 1
      }
      if (num !== null) {
        countList.push(num)
        count = count + num
      }
    }
    
    this.num = count % len === 0 ? len : count % len
    this.countList = countList
  }
}

export default TimeDunjia