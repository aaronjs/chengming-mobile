import Base from './base.js'

// 纳音列表
const nayinList = [
  '海中金', '炉中火', '大林木', '路旁土', '剑锋金',
  '山头火', '涧下水', '城头土', '白蜡金', '杨柳木',
  '泉中水', '屋上土', '霹雳火', '松柏木', '长流水',
  '沙中金', '山下火', '平地木', '壁上土', '金箔金',
  '覆灯火', '天河水', '大驿土', '钗钏金', '桑拓木',
  '大溪水', '沙中土', '天上火', '石榴木', '大海水'
]
const wuxingList = [
  3, 1, 0, 2, 3,
  1, 4, 2, 3, 0,
  4, 2, 1, 0, 4,
  3, 1, 0, 2, 3,
  1, 4, 2, 3, 0,
  4, 2, 1, 0, 4
]

class Nayin extends Base {
  constructor (nayin = 0, options = {}) {
    super(options)
    
    this.initNayin(nayin)
  }
  
  initNayin (nayin = 0) {
    this.types.push('Nayin')
    this.index = Base.solve(nayinList, nayin)
    if (this.index === null) {
      this.enabled = false
      return
    }
    
    this.name = nayinList[this.index]
    this.nameList = nayinList
    this.wuxing = wuxingList[this.index]
  }
}

Object.assign(Nayin, { nayinList })

export default Nayin