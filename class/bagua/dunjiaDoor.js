import Base from './base.js'

const doorSimList = ['休', '生', '伤', '杜', '景', '死', '惊', '开', '中']
const doorList = ['休门', '生门', '伤门', '杜门', '景门', '死门', '惊门', '开门', '中门']
// 五行列表
const doorWuxingList = [4, 2, 0, 0, 1, 2, 3, 3, 2]
// 原始后天宫位数列表
const afterOriginList = [1, 8, 3, 4, 9, 2, 7, 6, 5]

class DunjiaDoor extends Base {
  constructor (door = 0, options = {}) {
    super(options)
    
    this.initDoor(door)
  }
  
  initDoor (door = 0) {
    this.types.push('DunjiaDoor')
    if (typeof door === 'string' && door.length === 1) {
      // 用简称转换
      this.index = Base.solve(doorSimList, door)
    }
    else {
      this.index = Base.solve(doorList, door)
    }
    
    if (this.index === null) {
      this.enabled = false
      return
    }
    
    this.name = doorList[this.index]
    this.simName = doorSimList[this.index]
    this.nameList = doorList
    this.simList = doorSimList
    // 五行
    this.wuxing = doorWuxingList[this.index]
    // 原始所在的后天宫位数
    this.afterOrigin = afterOriginList[this.index]
    // 天禽星的特殊标记
    this.isSpecialStar = this.index === doorList.length - 1 ? true : false
  }
  
  // 下一个索引，isSpecial为true时，考虑 天禽星/中门 的特殊情况
  nextIndex (offset = 1, isSpecial = true) {
    const len = this.nameList.length
    if (!isSpecial) {
      return (this.index + offset) % (len - 1)
    }
    
    // 处理特殊情况
    if (this.index === len - 1) {
      // 当前为 天禽星/中门，下一个前往 天柱星/惊门
      return len - 3
    }
    else if (this.index === 4) {
      // 当前为 天英星/景门，下一个前往 天禽星/中门
      return len - 1
    }
    // 其他情况照常
    return (this.index + offset) % (len - 1)
  }
  // 上一个索引，isSpecial为true时，考虑 天禽星/中门 的特殊情况
  prevIndex (offset = 1, isSpecial = true) {
    const len = this.nameList.length
    const newLen = len - 1
    if (!isSpecial) {
      return (this.index + newLen - (offset % newLen)) % newLen
    }
    
    // 处理特殊情况
    if (this.index === len - 1) {
      // 当前为 天禽星/中门，上一个前往 天英星/景门
      return 4
    }
    else if (this.index === len - 3) {
      // 当前为 天柱星/惊门，上一个前往 天禽星/中门
      return len - 1
    }
    // 其他情况照常
    return (this.index + newLen - (offset % newLen)) % newLen
  }
  
  /* @section static */
  // 将后天宫位数转换为八门索引
  static afterToIndex (num) {
    const index = afterOriginList.indexOf(num)
    return index >= 0 ? index : 0
  }
}

Object.assign(DunjiaDoor, { doorList })

export default DunjiaDoor