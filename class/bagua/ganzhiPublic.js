// 定义干支功用部分

const ganzhiPublic = {
  zhiList: ['子', '丑', '寅', '卯', '辰', '巳', '午', '未', '申', '酉', '戌', '亥'],
  zhiWuxing: [4, 2, 0, 0, 2, 1, 1, 2, 3, 3, 2, 4],
  
  ganList: ['甲', '乙', '丙', '丁', '戊', '己', '庚', '辛', '壬', '癸'],
  ganWuxing: [0, 0, 1, 1, 2, 2, 3, 3, 4, 4]
}

export default ganzhiPublic