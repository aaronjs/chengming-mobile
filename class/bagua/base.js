import utils from '@/utils/index.js'

import Relation from '../relation/relation.js'

const wuxingList = ['木', '火', '土', '金', '水']
const yinyangList = ['阴', '阳']

// 所有八卦基础元素
class Base {
  constructor (options = {}) {
    // 基础属性初始化
    this.initBase(options)
    // 最后记录参数
    this.options = options
  }
  
  initBase (options = {}) {
    // 属性赋值
    this.types = ['Base']
    // 合法性
    this.enabled = true
    // id,唯一性
    this.id = typeof options.id === 'string' ? options.id : new Date().getTime() + utils.str.randomStr()
    // 索引
    this.index = typeof options.index === 'number' ? options.index : null
    // 名称
    this.name = typeof options.name === 'string' ? options.name : null
    // 简称
    this.simName = typeof options.simName === 'string' ? options.simName : null
    // 描述
    this.description = typeof options.description === 'string' ? options.description : ''
    // 名称列表
    this.nameList = Array.isArray(options.nameList) ? options.simName : []
    // 简称列表
    this.simList = Array.isArray(options.simList) ? options.simName : []
    // 阴阳五行
    this.yinyang = typeof options.yinyang === 'number' ? options.yinyang : null
    this.wuxing = typeof options.wuxing === 'number' ? options.wuxing : null
    
    // 关系对象
    this.relations = {}
  }
  
  /*
    根据输入类型和输出类型进行数据转化
  */
  static solve (dataList = [], input = 0, outputType = 'index') {
    let index = null
    if (typeof input === 'string') {
      // 文字输入,要在列表中寻找
      index = dataList.indexOf(input)
    }
    
    if (typeof input === 'number') {
      // 数字输入,直接获取索引
      index = input
    }
    
    if (index >= 0 && index < dataList.length) {
      // 在合理范围,根据输出类型进行输出返回
      if (outputType === 'index') {
        return index
      }
      else {
        return dataList[index]
      }
    }
    return null
  }
  
  nextIndex (offset = 1) {
    return (this.index + offset) % this.nameList.length
  }
  
  prevIndex (offset = 1) {
    const len = this.nameList.length
    return (this.index + len - (offset % len)) % len
  }
  
  /* @section static */
  // 获取五行生克关系
  // 计算目标: a是b的什么关系
  static wuxingRelation (a, b) {
    const relation = Relation.checkRelation('wuxing', [a, b])
    return relation
  }
  
  // 获取阴阳关系
  // 计算目标: a是b的什么关系
  static yinyangRelation (a, b) {
    const relation = Relation.checkRelation('yinyang', [a, b])
    return relation
  }
  
  /* @section set */
  // 记录十神关系
  setTenGod (obj, key) {
    const relation = this.getTenGod(obj)
    if (relation) {
      this.relations[key] = {
        txt: relation.txt,
        simTxt: relation.simTxt
      }
    }
  }
  
  /* @section get  */
  // 获取五行生克关系
  // 计算目标: this是obj的什么关系
  getWuxingRelation (obj) {
    return Base.wuxingRelation(this, obj)
  }
  
  // 获取阴阳关系
  getYinyangRelation (obj) {
    return Base.yinyangRelation(this, obj)
  }
  
  // 获取十神关系
  getTenGod (obj) {
    const relation = Relation.checkRelation('tenGod', [this, obj])
    return relation
  }
}

Object.assign(Base, { wuxingList, yinyangList })

export default Base