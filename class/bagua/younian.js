import Base from './base.js'

const younianList = ['贪狼生气', '天医巨门', '祸害禄存', '六煞文曲', '五鬼廉贞', '武曲延年', '绝命破军', '伏位']
const younianSimList = ['生', '医', '祸', '六', '鬼', '延', '绝', '伏']
const younianWuxing = [0, 2, 2, 4, 1, 3, 3, 0]

class Younian extends Base {
  constructor (data = 0, options) {
    super(options)
    
    this.initYounian(data)
  }
  
  /*
    data若为数组，则数组前两位为来比较的八卦
    若为数字，则由数字按照列表顺序初始化
  */
  initYounian (data = 0) {
    this.types.push('Younian')
    if (Array.isArray(data)) {
      this.index = Younian.younian(data[0], data[1], data[2])
    }
    else if (typeof data === 'number') {
      this.index = Base.solve(younianList, data)
    }
    
    if (this.index === null) {
      this.enabled = false
      return
    }
    
    this.nameList = younianList
    this.simList = younianSimList
    this.name = younianList[this.index]
    this.simName = younianSimList[this.index]
    
    this.wuxing = younianWuxing[this.index]
  }
  
  // 根据两个八卦，判断游年，返回游年索引
  static younian (a, b, isSmall = false) {
    if (a.types.indexOf('Bagua') < 0 || b.types.indexOf('Bagua') < 0) {
      return null
    }
    if (!isSmall) {
      return Younian.bigYounian(a, b)
    }
    return Younian.smallYounian(a, b)
  }
  
  // 大游年，返回游年索引
  static bigYounian (a, b) {
    const text = a.compare(b)
    
    switch (text) {
      case '100':
        // 上变生气
        return 0
        break
      case '011':
        // 下二医
        return 1
        break
      case '111':
        // 全变延年是大吉
        return 5
        break
      case '010':
        // 绝中
        return 6
        break
      case '001':
        // 祸下
        return 2
        break
      case '101':
        // 两头六
        return 3
        break
      case '110':
        // 上二变之为五鬼
        return 4
        break
      default:
    }
    return 7
  }
  
  // 小游年，返回游年索引
  static smallYounian (a, b) {
    const text = a.compare(b)
    
    switch (text) {
      case '100':
        // 上变生气
        return 0
        break
      case '011':
        // 下二武
        return 5
        break
      case '111':
        // 全变祸害禄存土
        return 2
        break
      case '010':
        // 绝中
        return 6
        break
      case '001':
        // 廉下
        return 4
        break
      case '101':
        // 两头文
        return 3
        break
      case '110':
        // 上二变之是巨门
        return 1
        break
      default:
    }
    return 7
  }
}

Object.assign(Younian, {
  younianList
})

export default Younian