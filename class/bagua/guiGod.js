import Base from './base.js'
import Zhi from './zhi.js'

import dictionary from './dictionary.js'

const godSimList = ['贵', '螣', '朱', '六', '勾', '青', '空', '白', '常', '玄', '阴', '后']
const godList = ['贵人', '螣蛇', '朱雀', '六合', '勾陈', '青龙', '天空', '白虎', '太常', '玄武', '太阴', '天后']
// 贵神转地支列表
const godZhiList = [1, 5, 6, 3, 4, 2, 10, 8, 7, 0, 9, 11]

class GuiGod extends Base {
  constructor (god = 0, options = {}) {
    super(options)
    
    this.initGod(god)
  }
  
  initGod (god = 0) {
    this.types.push('GuiGod')
    if (typeof god === 'string' && god.length === 1) {
      // 用简称转换
      this.index = Base.solve(godSimList, god)
    }
    else {
      this.index = Base.solve(godList, god)
    }
    
    if (this.index === null) {
      this.enabled = false
      return
    }
    
    this.name = godList[this.index]
    this.simName = godSimList[this.index]
    this.nameList = godList
    this.simList = godSimList
    // 贵神转地支
    this.zhi = new Zhi(godZhiList[this.index], {
      description: this.description + dictionary.ZHI
    })
    // 阴阳五行都随地支
    this.wuxing = this.zhi.wuxing
    this.yinyang = this.zhi.yinyang
  }
}

Object.assign(GuiGod, { godList })

export default GuiGod
