import Base from './base.js'

// 十二神列表
const godList = {
  // 十二建除
  jianchu: {
    list: ['建', '除', '满', '平', '定', '执', '破', '危', '成', '收', '开', '闭'],
    isGroundDoorList: [false, true, false, false, true, false, false, true, false, false, true, false]
  },
  // 十二贵神
  guiGod: {
    list: ['贵人', '螣蛇', '朱雀', '六合', '勾陈', '青龙', '天空', '白虎', '太常', '玄武', '太阴', '天后'],
    simList: ['贵', '螣', '朱', '六', '勾', '青', '空', '白', '常', '玄', '阴', '后']
  },
  // 十二宫
  twelvePalace: {
    list: ['命', '兄弟', '夫妻', '子女', '财帛', '疾厄', '迁移', '仆役', '官禄', '田宅', '福德', '父母']
  },
  // 十二长生
  twelveState: {
    list: ['长生', '沐浴', '冠带', '临官', '帝旺', '衰', '病', '死', '墓', '绝', '胎', '养']
  },
}

// 只要是有12个的神煞，都可以在指定key的情况下获取
class God12 extends Base {
  constructor (god12 = 0, key = 'jianchu', options = {}) {
    super(options)
    
    this.initGod12(god12, key)
  }
  
  initGod12 (god12 = 0, key = 'jianchu') {
    this.types.push('God12')
    
    const list = godList[key].list
    if (!list) {
      this.enabled = false
      return
    }
    
    this.index = Base.solve(list, god12)
    if (this.index === null) {
      this.enabled = false
      return
    }
    
    this.godKey = key
    this.name = list[this.index]
    this.nameList = list
    if (godList[key].simList) {
      this.simList = godList[key].simList
    }
    
    this.initOthers()
  }
  
  // 神煞特有的内容
  initOthers () {
    if (this.godKey === 'jianchu') {
      // 是否为第四户
      this.isGroundDoor = godList[this.godKey].isGroundDoorList[this.index]
      // 第四户醒目表示
      this.isAlert = this.isGroundDoor
    }
  }
 
  next (offset = 1) {
    this.index = this.nextIndex(offset)
    this.initGod12(this.index)
  }
  
  prev (offset = 1) {
    this.index = this.prevIndex(offset)
    this.initGod12(this.index)
  }
}

Object.assign(God12, {
  godList
})

export default God12