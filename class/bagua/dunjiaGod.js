import Base from './base.js'

const godSimList = ['符', '蛇', '阴', '六', '白', '玄', '地', '天']
const godList = ['值符', '腾蛇', '太阴', '六合', '白虎', '玄武', '九地', '九天']
// 五行列表
const godWuxingList = [0, 1, 3, 0, 3, 4, 2, 1]

class DunjiaGod extends Base {
  constructor (god = 0, options = {}) {
    super(options)
    
    this.initGod(god)
  }
  
  initGod (god = 0) {
    this.types.push('DunjiaGod')
    if (typeof god === 'string' && god.length === 1) {
      // 用简称转换
      this.index = Base.solve(godSimList, god)
    }
    else {
      this.index = Base.solve(godList, god)
    }
    
    if (this.index === null) {
      this.enabled = false
      return
    }
    
    this.name = godList[this.index]
    this.simName = godSimList[this.index]
    this.nameList = godList
    this.simList = godSimList
    // 五行
    this.wuxing = godWuxingList[this.index]
  }
}

Object.assign(DunjiaGod, { godList })

export default DunjiaGod