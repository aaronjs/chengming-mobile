// 起局常用描述的词典
const dictionary = {
  TIME: ['年', '月', '日', '时', '刻'],
  YINYANG: ['阴', '阳'],
  TIMEKEY: '柱',
  GAN: '天干',
  ZHI: '地支',
  GANZHI: '干支',
  XUN: '旬',
  NAYIN: '纳音',
  
  // 遁甲设定
  BAGUA: '八卦',
  SKYGAN: '天盘',
  GROUNDGAN: '地盘',
  EXTRAGAN: '寄宫',
  DUNJIA_GOD: '八神',
  DUNJIA_STAR: '九星',
  DUNJIA_DOOR: '八门',
  OUTGAN: '隐干',
  HEADSTAR: '值符星',
  HEADDOOR: '值使门',
  BIG_YOUNIAN: '大游年',
  
  // 山向设定
  MOUNTAIN: '山',
  DIRECTION: '向',
  GROUNDDRAGON: '透地龙',
  TWELVE_STATE: '十二长生',
  JIANG_GOD: '将神',
  HUANGQUAN: '黄泉煞',
  SMALL_YOUNIAN: '小游年',
  
  // 外圈神煞
  JIANCHU: '建神',
  TWELVEPALACE: '十二宫',
  
  // 六壬设定
  LIUREN_KEY_GANZHI: '用神',
  LIUREN_LIVE: '宿命',
  LIUREN_SEASON: '天时',
  LIUREN_DESTINY: '运势',
  LIUREN_LEGEND: '三传',
  LIUREN_GUIGOD: '贵人',
  LIUREN_YUEJIANG: '月将',
  GUOUND_ZHI: '地盘',
  SKY_ZHI: '天盘',
  GUIGOD: '神将',
  LIURENGAN: '六壬天干',
  
  // 太乙设定
  TAIYI_GROUND: '地盘神',
  TAIYI_SKY: '天盘神',
  
  // 八字设定
  HIDDENGAN: '藏干',
  BIG_DESTINY: '大运',
  BAZI_YEAR: '流年',
  BAZI_MONTH: '流月',
  
  // 时间神煞设定
  TAIYIN: '太阴',
  TIANYIGUIGOD: '天乙贵人',
  HORSE: '驿马',
  
}

export default dictionary