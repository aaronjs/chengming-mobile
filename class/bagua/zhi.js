import Base from './base.js'
// 干支的公共设定
import ganzhiPublic from './ganzhiPublic.js'
// relation设定，获取五行关系表
import relationSettings from '../relation/relationSettings.js'

const zhiList = ganzhiPublic.zhiList
const zhiWuxing = ganzhiPublic.zhiWuxing

// 生肖
const shengxiaoList = ['鼠', '牛', '虎', '兔', '龙', '蛇', '马', '羊', '猴', '鸡', '狗', '猪']

// 月将
const yuejiangList = ['神后子', '大吉丑', '功曹寅', '太冲卯', '天罡辰', '太乙巳', '胜光午', '小吉未', '传送申', '从魁酉', '河魁戌', '登明亥']
// 月将是否为天三门
const isSkyDoorList = [false, false, false, true, false, false, false, true, false, true, false, false]

// 常规地支藏干信息
const hiddenGans = [
  ['癸'], ['癸', '辛', '己'], ['戊', '丙', '甲'], ['乙'], ['乙', '癸', '戊'], ['戊', '庚', '丙'],
  ['己', '丁'], ['丁', '乙', '己'], ['戊', '壬', '庚'], ['辛'], ['辛', '丁', '戊'], ['甲', '壬']
]

// 长生位
const bornZhiList = [8, 5, 2, 11, 8, 5, 2, 11, 8, 5, 2, 11]


class Zhi extends Base {
  constructor (zhi = 0, options = {}) {
    super(options)
    
    this.initZhi(zhi)
  }
  
  initZhi (zhi = 0) {
    this.types.push('Zhi')
    this.index = Base.solve(zhiList, zhi)
    if (this.index === null) {
      this.enabled = false
      return
    }
    
    this.name = zhiList[this.index]
    this.nameList = zhiList
    // 异或取反，因为0代表阴，1代表阳,天干中02468为阳,13579为阴
    this.yinyang = (this.index % 2) ^ 1
    // 天干五行的顺序为木火土金水,每两个五行相同
    this.wuxing = zhiWuxing[this.index]
    // 生肖
    this.shengxiao = shengxiaoList[this.index]
    // 藏干列表
    this.hiddenGansText = hiddenGans[this.index]
    this.hiddenGans = []
    
    // 地支对应的时间(24小时制)
    this.hourStart = (this.index * 2 + 23) % 24
    this.hourEnd = (this.index * 2 + 25) % 24
    
    // 长生位记录
    this.bornZhi = bornZhiList[this.index]
    // 十二长生是否顺时针排列，地支都是顺排
    this.isTwelveStateClock = true
    
    // 月将的特殊处理
    if (this.options.yuejiangKey) {
      this.isYuejiang = true
      // 是否天三门
      this.isSkyDoor = isSkyDoorList[this.index]
      // 名称改为月将全名，简称变成地支名
      this.name = yuejiangList[this.index]
      this.simName = zhiList[this.index]
      this.nameList = yuejiangList
      this.simList = zhiList
      // 天三门标红
      this.isAlert = this.isSkyDoor
    }
  }
 
  next (offset = 1) {
    this.index = this.nextIndex(offset)
    this.initZhi(this.index)
  }
  
  prev (offset = 1) {
    this.index = this.prevIndex(offset)
    this.initZhi(this.index)
  }
  
  // 给定目标地支序数，计算十二长生数
  // 地支的十二长生都是顺着走的
  getTwelveStateIndex (zhiIndex) {
    const len = zhiList.length
    const offset = zhiIndex >= this.bornZhi ? zhiIndex - this.bornZhi : zhiIndex + len - this.bornZhi
    return offset
  }
  
  // 给定十二长生位置，计算地支位置
  getZhiByTwelveState (stateIndex) {
    const len = zhiList.length
    return (this.bornZhi + stateIndex) % len
  }
  
  // 给定十神序数，求对应的干支，返回所有符合条件的序数
  getGanzhiByTenGod (tenGodIndex, isGan = true) {
    const wuxingList = isGan ? ganWuxing : ganzhiPublic.zhiWuxing
    const yinyangRelation = tenGodIndex % 2
    const wuxingRelation = Math.floor(tenGodIndex / 2)
    // 获取五行生克数字表
    const wuxingRelationOffsetList = relationSettings.wuxing.offsetList
    const wuxingLen = wuxingRelationOffsetList.length
    // 开始计算
    const offset = wuxingRelationOffsetList[wuxingRelation]
    let wuxing = (this.wuxing + offset) % wuxingLen
    const len = wuxingList.length
    let result = []
    for (let i = 0; i < len; i++) {
      const yinyangJudge = (yinyangRelation && this.yinyang === (i % 2) ^ 1) || (!yinyangRelation && this.yinyang !== (i % 2) ^ 1)
      if (yinyangJudge && wuxingList[i] === wuxing) {
        result.push(i)
      }
    }
    return result
  }
  
  // 获取合支索引
  getHeIndex () {
    const heList = relationSettings.zhiHe.list
    return zhiList.indexOf(heList[this.index])
  }
}

Object.assign(Zhi, { zhiList })

export default Zhi