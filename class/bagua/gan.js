import Base from './base.js'
// 干支的公共设定
import ganzhiPublic from './ganzhiPublic.js'
// relation设定，获取五行关系表
import relationSettings from '../relation/relationSettings.js'

const ganList = ganzhiPublic.ganList
const ganWuxing = ganzhiPublic.ganWuxing
// 六仪顺序
const liuyiList = ['戊', '己', '庚', '辛', '壬', '癸', '丁', '丙', '乙']

// 长生位
const bornZhiList = [11, 6, 2, 9, 2, 9, 5, 0, 8, 3]
// 六壬寄宫
const liurenZhiList = [2, 4, 5, 7, 5, 7, 8, 10, 11, 1]

// 地支数
const ZHI_LEN = 12

class Gan extends Base {
  constructor (gan = 0, options = {}) {
    super(options)
    
    this.initGan(gan)
  }
  
  initGan (gan = 0) {
    this.types.push('Gan')
    this.index = Base.solve(ganList, gan)
    if (this.index === null) {
      this.enabled = false
      return
    }
    
    this.name = ganList[this.index]
    this.nameList = ganList
    // 异或取反，因为0代表阴，1代表阳,天干中02468为阳,13579为阴
    this.yinyang = (this.index % 2) ^ 1
    // 天干五行的顺序为木火土金水,每两个五行相同
    this.wuxing = ganWuxing[this.index]
    
    // 长生位记录
    this.bornZhi = bornZhiList[this.index]
    // 十二长生阳顺阴逆
    this.isTwelveStateClock = this.index % 2 === 0
  }
  
  next (offset = 1) {
    this.index = this.nextIndex(offset)
    this.initGan(this.index)
  }
  
  prev (offset = 1) {
    this.index = this.prevIndex(offset)
    this.initGan(this.index)
  }
  
  // 给定目标地支序数，计算十二长生序数
  getTwelveStateIndex (zhiIndex) {
    const len = ZHI_LEN
    let offset = 0
    // 天干的十二长生阳顺阴逆
    if (this.isTwelveStateClock) {
      offset = zhiIndex >= this.bornZhi ? zhiIndex - this.bornZhi : zhiIndex + len - this.bornZhi
    }
    else {
      offset = zhiIndex <= this.bornZhi ? this.bornZhi - zhiIndex : this.bornZhi + len - zhiIndex
    }
    return offset
  }
  
  // 给定十二长生位置，计算地支序数
  getZhiByTwelveState (stateIndex) {
    return this.isTwelveStateClock ? (this.bornZhi + stateIndex) % ZHI_LEN : (this.bornZhi - stateIndex + ZHI_LEN) % ZHI_LEN
  }
  
  // 给定十神序数，求对应的干支，返回所有符合条件的序数
  getGanzhiByTenGod (tenGodIndex, isGan = true) {
    const wuxingList = isGan ? ganWuxing : ganzhiPublic.zhiWuxing
    const yinyangRelation = tenGodIndex % 2
    const wuxingRelation = Math.floor(tenGodIndex / 2)
    // 获取五行生克数字表
    const wuxingRelationOffsetList = relationSettings.wuxing.offsetList
    const wuxingLen = wuxingRelationOffsetList.length
    // 开始计算
    const offset = wuxingRelationOffsetList[wuxingRelation]
    let wuxing = (this.wuxing + offset) % wuxingLen
    const len = wuxingList.length
    let result = []
    for (let i = 0; i < len; i++) {
      const yinyangJudge = (yinyangRelation && this.yinyang === (i % 2) ^ 1) || (!yinyangRelation && this.yinyang !== (i % 2) ^ 1)
      if (yinyangJudge && wuxingList[i] === wuxing) {
        result.push(i)
      }
    }
    return result
  }
  
  // 给定支,以此干为基础,用五鼠遁推出干,返回干的索引值
  getMouseGanIndex (zhiIndex) {    
    var len = ganList.length
    var ganIndex = this.index
    // 每组合干为一组
    if (ganIndex >= len / 2) {
      ganIndex = ganIndex - Math.floor(len / 2)
    }
    ganIndex = ganIndex * 2
    ganIndex = (ganIndex + zhiIndex) % len
    
    return ganIndex
  }
  
  // 给定支,以此干为基础,用五虎遁推出干,返回干的索引值
  getTigerGanIndex (zhiIndex) {
    const tiger = 2
    const zhiLen = ZHI_LEN
    const zhiOffset = (zhiIndex + zhiLen - tiger) % zhiLen
    
    var len = ganList.length
    var ganIndex = this.index
    // 每组合干为一组
    if (ganIndex >= len / 2) {
      ganIndex = ganIndex - Math.floor(len / 2)
    }
    ganIndex = ganIndex * 2 + tiger
    ganIndex = (ganIndex + zhiOffset) % len
    
    return ganIndex
  }
}

Object.assign(Gan, { ganList, liuyiList, liurenZhiList })

export default Gan