import Base from './base.js'

// 太乙十六神
const taiyiGroundList = ['地主', '阳德', '和德', '吕申', '高从', '太阳', '大炅', '大神', '大威', '天道', '大武', '武德', '大簇', '阴主', '阴德', '大义']
// 太乙十六神简称
const taiyiGroundSimList = ['子', '丑', '艮', '寅', '卯', '辰', '巽', '巳', '午', '未', '坤', '申', '酉', '戌', '乾', '亥']
// 太乙十六神属性表
const taiyiGroundWuxing = [4, 2, 2, 0, 0, 2, 0, 1, 1, 2, 2, 3, 3, 2, 3, 4]

class TaiyiGround extends Base {
  constructor (taiyiGround = 0, options = {}) {
    super(options)
    
    this.inittaiyiGround(taiyiGround)
  }
  
  inittaiyiGround (taiyiGround = 0) {
    this.types.push('taiyiGround')
    this.index = Base.solve(taiyiGroundList, taiyiGround)
    if (this.index === null) {
      this.index = Base.solve(taiyiGroundSimList, taiyiGround)
    }
    if (this.index === null) {
      this.enabled = false
      return
    }
    
    this.name = taiyiGroundList[this.index]
    this.simName = taiyiGroundSimList[this.index]
    this.nameList = taiyiGroundList
    this.simList = taiyiGroundSimList
    
    // 阴阳五行
    this.yinyang = (this.index % 2) ^ 1
    this.wuxing = taiyiGroundWuxing[this.index]
  }
}

Object.assign(TaiyiGround, {
  taiyiGroundList,
  taiyiGroundSimList
})

export default TaiyiGround