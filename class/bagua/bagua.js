import Base from './base.js'

// 后天卦表：后天卦宫的先天卦名
const afterBaguaList = ['坎', '坤', '震', '巽', '乾', '兑', '艮', '离']
// 先天卦表：先天卦宫的后天卦名
const beforeBaguaList = ['兑', '坎', '艮', '坤', '离', '巽', '乾', '震']

// 八卦阴阳，以后天为准
const yinyangList = [1, 0, 1, 0, 1, 0, 1, 0]
// 八卦五行，以后天为准
const wuxingList = [4, 2, 0, 0, 3, 3, 2, 1]
// 卦象，以后天为准
const guaList = ['010', '000', '001', '110', '111', '011', '100', '101']

class Bagua extends Base {
  constructor (bagua = 0, options = {}) {
    super(options)
    
    this.bagua(bagua)
  }
  
  bagua (bagua = 0) {
    this.types.push('Bagua')
    // 以后天卦表为准
    this.index = Base.solve(afterBaguaList, bagua)
    if (this.index === null) {
      this.enabled = false
      return
    }
    
    // 卦名
    this.name = afterBaguaList[this.index]
    this.nameList = afterBaguaList
    // 该卦的先天卦的名称
    this.before = beforeBaguaList[this.index]
    // 该卦的后天卦的名称
    this.after = afterBaguaList[beforeBaguaList.indexOf(this.name)]
    // 阴阳五行
    this.yinyang = yinyangList[this.index]
    this.wuxing = wuxingList[this.index]
    // 卦象，0位阴爻，1为阳爻
    this.gua = guaList[this.index]
  }
  
  // 卦象比较
  compare (bagua) {
    const len = this.gua.length
    let result = ''
    for (let i = 0; i < len; i++) {
      if (this.gua[i] === bagua.gua[i]) {
        // 相同的为0
        result = result + '0'
      }
      else {
        result = result + '1'
      }
    }
    return result
  }
  
}

Object.assign(Bagua, {
  baguaList: afterBaguaList
})

export default Bagua