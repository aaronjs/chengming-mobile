import Base from './base.js'
import Gan from './gan.js'

const xunList = ['甲子戊', '甲戌己', '甲申庚', '甲午辛', '甲辰壬', '甲寅癸']

class Xun extends Base {
  constructor (xun = 0, options = {}) {
    super(options)
    
    this.initXun(xun)
  }
  
  initXun (xun = 0) {   
    this.types.push('Xun')
    this.index = Xun.solve(xun)
    if (this.index === null) {
      this.enabled = false
      return
    }
    
    this.name = xunList[this.index]
    this.nameList = xunList
    // 旬首天干
    this.head = new Gan(this.name[2])
  }
  
  /* @Override */
  static solve (input = 0, outputType = 'index') {
    let index = null
    if (typeof input === 'string') {
      // 文字输入,要在列表中寻找
      index = xunList.findIndex((xun) => {
        // 匹配全部,或者匹配前面两个字,或者匹配最后一个字,都能初始化成功
        return input === xun || input === xun.slice(0, 2) || input === xun[2]
      })
    }
    
    if (typeof input === 'number') {
      // 数字输入,直接获取索引
      index = input
    }
    
    if (index >= 0 && index < xunList.length) {
      // 在合理范围,根据输出类型进行输出返回
      if (outputType === 'index') {
        return index
      }
      else {
        return xunList[index]
      }
    }
    return null
  }
  
  next (offset = 1) {
    this.index = this.nextIndex(offset)
    this.initXun(this.index)
  }
  
  prev (offset = 1) {
    this.index = this.prevIndex(offset)
    this.initXun(this.index)
  }
}

Object.assign(Xun, { xunList })

export default Xun