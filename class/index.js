import CMDate from './date/cmDate.js'

import Base from './bagua/base.js'
import Bagua from './bagua/bagua.js'
import Gan from './bagua/gan.js'
import Zhi from './bagua/zhi.js'
import Xun from './bagua/xun.js'
import Ganzhi from './bagua/ganzhi.js'
import Mountain from './bagua/mountain.js'

import Bazi from './stage/bazi.js'
import Taiyi from './stage/taiyi.js'
import Liuren from './stage/liuren.js'
import TimeDunjia from './stage/timeDunjia.js'
import PosDunjia from './stage/posDunjia.js'

import Relation from './relation/relation.js'
import TimeGod from './relation/timeGod.js'

// 只是导出和时间、八卦相关的模块
const CM = {
  Date: CMDate,
  
  Base,
  Bagua,
  Gan,
  Zhi,
  Xun,
  Ganzhi,
  Mountain,
  
  Bazi,
  TimeDunjia,
  PosDunjia,
  Taiyi,
  Liuren,
  
  Relation,
  TimeGod
}

export default CM