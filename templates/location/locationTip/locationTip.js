import { mapGetters } from 'vuex'

import cmStyleMixin from '@/mixins/cmStyle.js'

export default {
  name: 'locationTip',
  mixins: [
    cmStyleMixin
  ],
  props: {
    iconSize: {
      default: 18,
      type: [Number, String]
    }
  },
  data () {
    return {
    }
  },
  computed: {
    ...mapGetters({
      selectedLocation: 'location/selectedLocation',
    }),
    
    // 地址名称
    locationName () {
      if (!this.selectedLocation) {
        return ''
      }
      return this.selectedLocation.getSimAddress()
    },
    
    // 地址备注
    typeText () {
      if (!this.selectedLocation) {
        return ''
      }
      if (this.selectedLocation.type === 'gps') {
        return '[定位]'
      }
      if (this.selectedLocation.type === 'default') {
        return '[默认]'
      }
      return ''
    }
  },
  created () {
    this.$store.dispatch('location/init').then(() => {})
    .catch((e) => {
      uni.showToast({
        title: e.errmsg,
        icon: 'none'
      })
    })
  }
}