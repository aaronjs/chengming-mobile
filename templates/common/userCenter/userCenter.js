import pageTemplateMixin from '@/mixins/pageTemplate.js'

import User from '@/class/user/user.js'
import global from '@/global.js'

export default {
  name: 'userCenter',
  mixins: [
    pageTemplateMixin
  ],
  data () {
    return {
      CustomBar: this.CustomBar,
      StatusBar: this.StatusBar,
      
      // 用于隐藏购买链接，通过审核
      productEnv: global.productEnv,
      
      user: new User(),
      // 当前需要修改的内容
      currentSet: 'id',
      tagNames: {
        nickname: '昵称',
        motto: '简介信息'
      }
    }
  },
  computed: {    
    isLogin () {
      return this.user.uid !== null
    },
    
    // 个人信息页面顶部标题栏样式
    infoBarStyle () {
      return {
        height: (this.CustomBar - this.StatusBar) + 'px'
      }
    }
  },
  methods: {
    // 登录回调
    loginHandler () {
      if (this.isLogin) { return }
      
      // #ifdef MP
      // 小程序登录，相当于获取用户信息的注册操作
      this.$u.msg.loading('登录中')
      this.user.mpLogin().then(() => {
        this.$u.msg.hideLoading()
        console.log('登录成功', this.user)
        this.$u.msg.toast('登录成功')
        
        this.user.checkMobile()
      })
      .catch((e) => {
        this.$u.msg.hideLoading()
        console.log('小程序登录失败', e)
        this.$u.msg.errToast(e)
      })
      // #endif
    },
    
    // 显示账户信息回调
    infoClickHandler () {
      this.$refs.infoPopup.show()
    },
    
    // 隐藏账户信息回调
    infoCloseHandler () {
      this.$refs.infoPopup.hide()
    },
    
    // 修改用户信息回调
    setUserHandler (val) {
      if (val !== this.currentSet) {
        this.currentSet = val
        this.$refs.inputModal.inputClear()        
      }
      this.$refs.inputModal.show()
    },
    
    // 更新用户信息的回调
    updateUserHandler (res) {
      const txt = res.inputTxt
      if (this.currentSet === 'nickname' && txt === '') {
        this.$u.msg.toast('昵称不得为空')
        return
      }
      
      this.$refs.inputModal.hide()
      
      const data = {}
      data[this.currentSet] = txt
      this.$u.msg.loading('设置中')
      this.user.update(data).then(() => {
        this.$u.msg.hideLoading()
        console.log('设置成功', this.user)
        this.$u.msg.toast('设置成功')
      })
      .catch((e) => {
        this.$u.msg.hideLoading()
        console.log('设置失败', e)
        this.$u.msg.errToast(e)
      })
    },
    
    // 更改绑定手机的回调
    bindMobileHandler () {
      this.$u.router.push('bindMobile')
    },
    
    // 点击购买回调
    buyHandler () {
      this.$u.router.push('buy')
    },
    
    // 点击排盘回调
    baguaHandler () {
      this.$u.router.push('baguaRecords')
    },
    
    // 点击设置的回调
    configHandler () {
      this.$u.router.push('config')
    }
  }
}