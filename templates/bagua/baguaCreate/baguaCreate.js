import baguaTimeCard from '@/templates/bagua/baguaTimeCard/baguaTimeCard.vue'

import pageTemplateMixin from '@/mixins/pageTemplate.js'

import CM from '@/class/index.js'
import utils from '@/utils/index.js'
import User from '@/class/user/user.js'

// 输入框事件触发间隔
const INPUT_INTERVAL = 100
// 开发中的功能
const DEV_FUNC = ['freeDunjia', 'taiyi']

export default {
  name: 'baguaCreate',
  mixins: [
    pageTemplateMixin
  ],
  components: {
    baguaTimeCard
  },
  data () {
    return {
      // 起局悬浮按钮
      createFloat: [
        {
          icon: 'iconfont icon-cm-yinyang',
          text: '排盘'
        }
      ],
      
      // options默认必须拥有的字段
      defaultKeys: ['title', 'date', 'type'],
      // speKeys字段指定了不同排盘options特有的字段
      speKeys: ['timeDunjiaType'],
      
      // 起局类型选项框，其中speKeys字段指定了该种排盘options特有的字段
      typeRadio: [
        { value: 'timeDunjia', text: '时家奇门', icon: 'iconfont icon-clock', speKeys: ['timeDunjiaType'] },
        { value: 'posDunjia', text: '山向奇门', icon: 'iconfont icon-home', speKeys: ['posDunjiaType', 'angle'] },
        { value: 'freeDunjia', text: '自定奇门', icon: 'iconfont icon-survey' },
        { value: 'bazi', text: '道家八字', icon: 'iconfont icon-cm-yinyang', speKeys: ['sex', 'name']  },
        { value: 'liuren', text: '大六壬', icon: 'iconfont icon-content_qinglong', speKeys: ['guiGodType', 'liurenKeyGanzhi', 'liurenShengxiao'] },
        { value: 'taiyi', text: '太乙神数', icon: 'iconfont icon-content_xuanwu' },
        { value: 'records', text: '排盘记录', icon: 'iconfont icon-feeds' }
      ],
      
      // 时局切入时空选项
      timeTypeRadio: [
        { value: 'year', text: '以年定局' },
        { value: 'month', text: '以月定局' },
        { value: 'day', text: '以日定局' },
        { value: 'hour', text: '以时定局' },
        { value: 'min', text: '以刻定局' }
      ],
      
      // 山向切入时空选项
      posTypeRadio: [
        { value: 'year', text: '以年定局' },
        { value: 'month', text: '以月定局' },
        { value: 'day', text: '以日定局' },
        { value: 'dragon', text: '以透地龙定局' },
      ],
      
      // 自定义遁甲局
      freeDunjiaRadio: [
        { value: '2', text: '两柱定局' },
        { value: '1', text: '单柱定局' }
      ],
      
      // 八字性别选项框
      sexRadio: [
        { value: '男', text: '男' },
        { value: '女', text: '女' }
      ],
      
      // 六壬贵神规则选项框
      guiGodRadio: [
        { value: 'auto', text: '自动(卯酉分)' },
        { value: 'solar', text: '阳贵人' },
        { value: 'lunar', text: '阴贵人' }
      ],
      
      // 用户数据
      user: new User(),
      
      // 时间对象
      date: null,
      
      // 起局信息表
      createData: {
        title: '',
        type: 'timeDunjia',
        date: {},
        // 时局定局切入点
        timeDunjiaType: 'hour',
        
        // 山向定局切入点/基准
        posDunjiaType: 'year',
        // 山向角度
        angle: '0',
        
        // 八字性别
        sex: '男',
        // 八字的姓名
        name: '',
        
        // 六壬贵神类型
        guiGodType: 'auto',
        // 六壬用神干支
        liurenKeyGanzhi: '甲子',
        // 六壬用神宿命
        liurenShengxiao: '子'
      },
      // 当前已经显示过的模块，初始默认为时局
      loadedList: ['timeDunjia'],
      
      // 临时记录的山向信息
      posMountain: null,
      // 山向角度输入
      posAngleInput: '0',
      // 自定义遁甲局选项
      freeDunjiaValue: '2',
      // 八字姓名输入
      baziNameInput: '',
    }
  },
  computed: {
    // 页面容器样式
    pageStyle () {
      // 页面较长，还要空出底部浮动按钮的位置
      const bottom = parseInt(this.tabHeight) + this.paddingStandard + parseInt(this.floatSize)
      return {
        paddingBottom: bottom + 'px'
      }
    },
    
    // 修正后的山向数据
    posText () {
      if (this.posMountain === null) {
        return '无效的山向信息'
      }
      
      let result = `${this.createData.angle}° / ${this.posMountain.mountain.name}山${this.posMountain.direction.name}向`
      if (this.createData.posDunjiaType === 'dragon') {
        result = result + ' / ' + this.posMountain.mountain.groundDragon.name + '(龙)'
      }
      return result
    },
  },
  methods: {
    // 点击起局类型发生的变化
    clickHandler (item) {
      if (item.value === 'records') {
        // 点击排盘记录进入记录页面
        this.$u.router.push('baguaRecords')
        return
      }
      
      if (DEV_FUNC.indexOf(item.value) >= 0) {
        this.$u.msg.toast('功能正在开发中')
      }
      else {
        if (this.loadedList.indexOf(item.value) < 0) {
          this.loadedList.push(item.value)
        }
        this.createData.type = item.value
        this.speKeys = item.speKeys ? item.speKeys : []
      }
    },
    
    // 角度输入触发回调
    angleInputHandler: utils.common.debounce(function (val) {
      if (!this.$u.str.isNumberString(val) || val[0] === '-') {
        // 非法的角度输入
        this.posMountain = null
        return
      }
      this.createData.angle = (parseFloat(val) % 360).toFixed(2)
      // 更新山向显示信息
      this.updateMountain()
    }, INPUT_INTERVAL),
    
    /* 
      数据发生变化时，改变值
      index 不为-1时，说明createData[key]可能是数组，要改变数组指定索引的值
    */
    changeHandler (val, key) {
      if (key === 'date') {
        this.date = val
        // 日期数据只留下公历数据就好
        this.createData.date = val.getSolarOptions()
        // 日期改变，六壬的关键干支也重置为日干支
        this.createData.liurenKeyGanzhi = val.ganzhi[2].name
      }
      else if (key === 'posDunjiaType') {
        this.createData[key] = val
        this.updateMountain()
      }
      else {
        this.createData[key] = val
      }
    },
    
    // 自定义遁甲局选项突变
    freeDunjiaChangeHandler (val) {
      this.freeDunjiaValue = val
    },
    
    // 点击起局按钮
    createClickHandler () {
      if (this.createData.type === 'posDunjia' && this.posMountain === null) {
        // 山向没有输入有效的角度
        this.$u.msg.toast('请输入有效的山向角度')
        return
      }
      // 山向起局需要权限
      if (this.createData.type === 'posDunjia' && !this.user.baguaAppCheck()) {
        return
      }
      
      const keys = this.defaultKeys.concat(this.speKeys)
      let options = {}
      const len = keys.length
      for (let i = 0; i < len; i++) {
        options[keys[i]] = this.createData[keys[i]]
      }
      
      // 八字起局的标题与姓名相关联
      if (this.createData.type === 'bazi') {
        if (this.createData.name === '') {
          this.$u.msg.toast('请输入姓名')
          return
        }
        options.title = this.createData.name + '的生辰八字'
      }
      
      console.log(options)
      this.$u.router.push('bagua', [options])
    },
    
    // 更新当前的山向信息
    updateMountain () {
      // 根据角度定向
      const direction = new CM.Mountain(this.createData.angle)
      // 向的对面就是山
      const oppositeAngle = CM.Mountain.getOppositeAngle(direction.angle)
      const mountain = new CM.Mountain(oppositeAngle.toString(), {
        // 地龙只有选定地龙起局时才需要
        groundDragon: this.createData.posDunjiaType === 'dragon'
      })
      this.posMountain = {
        mountain,
        direction
      }
    },
    
    // 选六壬的用神
    selectLiurenKeyGanzhiHandler () {
      this.$refs.liurenKeyGanzhiPicker.show()
    },
    
    // 选六壬的宿命
    selectLiurenShengxiaoHandler () {
      this.$refs.liurenShengxiaoPicker.show()
    }
  }
}