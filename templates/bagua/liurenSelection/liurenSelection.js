import CM from '@/class/index.js'

export default {
  name: 'liurenSelection',
  props: {
    // 六壬用神干支
    ganzhi: {
      type: String,
      default: '甲子'
    },
    // 六壬宿命
    zhi: {
      type: String,
      default: '子'
    },
    // 六壬贵神模式 auto/yang/yin 对应 自动/阳贵人/阴贵人
    guiGod: {
      type: String,
      default: 'auto'
    }
  },
  data () {
    return {
      keyGanzhi: this.ganzhi,
      keyZhi: this.zhi,
      guiGodType: this.guiGod,
      
      // 六壬贵神规则选项框
      guiGodRadio: [
        { value: 'auto', text: '自动(卯酉分)' },
        { value: 'yang', text: '阳贵人' },
        { value: 'yin', text: '阴贵人' }
      ],
    }
  },
  watch: {
    ganzhi (val) {
      this.keyGanzhi = val
    },
    zhi (val) {
      this.keyZhi = val
    }
  },
  methods: {
    onChange (key) {
      this.$emit('change', this[key], key)
    }
  }
}