import baguaElement from '../baguaElement/baguaElement.vue'

export default {
  name: 'baguaRelation',
  components: {
    baguaElement
  },
  props: {
    size: {
      type: [Number, String],
      default: 16
    },
    relation: {
      type: Object,
      default () {
        return {}
      }
    }
  },
  data () {
    return {
      style: {
        font: {
          fontSize: this.size + 'px',
          paddingLeft: this.size + 'px',
        },
        header: {
          top: (parseInt(this.size) * (1 / 4)) + 'px'
        }
      },
      // 展示模块
      model: []
    }
  },
  computed: {
    nbsp () {
      return (num = 1) => {
        let result = ''
        for (let i = 0; i < num; i++) {
          result = result + '&nbsp;'
        }
        return result
      }
    }
  },
  methods: {
    initModel () {
      const modelArr = this.relation.model.split(' ')
      const len = modelArr.length
      for (let i = 0; i < len; i++) {
        const textLen = modelArr[i].length
        if (modelArr[i][0] !== '$') {
          // 普通文字
          this.model.push({ type: 'text', text: modelArr[i] })
        }
        else {
          const key = modelArr[i].slice(1, textLen)
          if (this.$u.str.isNumberString(key)) {
            const index = parseInt(key)
            // 若结构为 $数字，则展示对象
            this.model.push({ type: 'obj', obj: this.relation.objs[index] })
          }
          else {
            // 若结构为 $key，则展示对应key值
            this.model.push({ type: 'key', text: this.relation[key] })
          }
        }
      }
    }
  },
  created () {
    this.initModel()
  }
}