import baguaElement from '@/templates/bagua/baguaElement/baguaElement.vue'
import baguaRelation from '@/templates/bagua/baguaRelation/baguaRelation.vue'
import baguaTool from '@/templates/bagua/baguaTool/baguaTool.vue'

import commonMixin from '@/mixins/common.js'
import baguaElementSignMixin from '@/mixins/baguaElementSign.js'

import CM from '@/class/index.js'

const SCROLL_DURATION = 150
// 滑动区域边界的偏移量
const SCROLL_BORDER_OFFSET = 3
// 吸顶模块的索引
const STICKY_POS_INDEX = 1

// 八字工具栏功能
const TOOL_LIST = [
  { key: 'seasonCheck' }
]

export default {
  name: 'baziTemplate',
  mixins: [
    commonMixin,
    baguaElementSignMixin
  ],
  components: {
    baguaElement,
    baguaTool,
    baguaRelation
  },
  props: {
    options: {
      default () {
        return {}
      },
      type: Object
    }
  },
  data () {
    return {
      // 无论是statusBar还是customBar都要再重复赋值，避免小程序出问题
      CustomBar: this.CustomBar,
      StatusBar: this.StatusBar,
      windowWidth: this.windowWidth,
      windowHeight: this.windowHeight,
      
      // 八字id前缀
      idPrefix: 'baziTemplate-',
      // 吸顶模块ID
      stickyId: 'baziSticky',
      // 导航内容
      navData: [
        {
          text: '基本信息',
          id: 'base'
        },   
        {
          text: '命造信息',
          id: 'ganzhi'
        },
        {
          text: '流年大运',
          id: 'years'
        },
      ],
      // 当前导航索引
      curIndex: 0,
      
      isLoaded: false,
      // 八字局对象
      bazi: null,
      // 是否显示大运流年
      isDestinyShow: false,
      
      // 遁甲工具设定
      toolOptions: [],
      // 遁甲工具设定信息
      toolSetting: null,
    }
  },
  computed: {
    // 模板样式, 与工具栏的高度相关
    templateStyle () {
      if (!this.toolSetting) {
        return {}
      }
      return {
        paddingBottom: this.toolSetting.height + 'px'
      }
    },
    
    baziProxy () {
      return {
        // 大运
        bigDestinyAge: !this.bazi ? '' : this.bazi.bigDestiny.startAge,
        bigDestinyList: !this.bazi ? [] : this.bazi.bigDestiny.list,
        bigDestinyCurrent: !this.bazi ? -1 : this.bazi.bigDestiny.curSelected,
        bigDestinySelected: !this.bazi ? 0 : this.bazi.bigDestiny.selected,
        // 流年
        baziYearsList: !this.bazi ? [] : this.bazi.baziYears.list,
        baziYearsCurrent: !this.bazi ? -1 : this.bazi.baziYears.curSelected,
        baziYearsSelected: !this.bazi ? 0 : this.bazi.baziYears.selected,
        // 流月
        baziMonthsList: !this.bazi ? [] : this.bazi.baziMonths.list,
        baziMonthsCurrent: !this.bazi ? -1 : this.bazi.baziMonths.curSelected,
        baziMonthsSelected: !this.bazi ? 0 : this.bazi.baziMonths.selected,
      }
    },
    
    // 重要干支
    baziGanzhi () {
      let ganzhi = []
      if (this.bazi) {
        // 加入四柱
        ganzhi = this.bazi.date.ganzhi.slice(0, 4)
        // 加入大运流年流月
        if (this.isDestinyShow) {
          if (this.bazi.bigDestiny.selected === 0) {
            ganzhi.push({
              // 运前特殊标记
              beforeDestiny: true,
              zhi: {
                name: '',
                hiddenGans: []
              }
            })
          }
          else {
            ganzhi.push(this.bazi.bigDestiny.list[this.bazi.bigDestiny.selected].ganzhi)
          }
          ganzhi.push(this.bazi.baziYears.list[this.bazi.baziYears.selected].ganzhi)
          ganzhi.push(this.bazi.baziMonths.list[this.bazi.baziMonths.selected])          
        }
      }
      return ganzhi
    },
    
    // 公历生日
    solarTimeText () {
      if (!this.bazi) {
        return ''
      }
      return this.bazi.date.getSolarName() + ' ' + this.bazi.date.getWeekDay() + ' ' + this.bazi.date.getTime()
    },
    
    // 农历生日
    lunarTimeText () {
      if (!this.bazi) {
        return ''
      }
      return this.bazi.date.getLunarName() + ' ' + this.bazi.date.ganzhi[3].zhi.name + '时'
    },
    
    // 起运时间
    bigDestinyStartText () {
      if (!this.bazi) {
        return ''
      }
      const startTime = this.bazi.bigDestiny.startTime
      return CM.Date.transDay(startTime.year, startTime.month, startTime.day)
    },
  },
  methods: {
    // 初始化
    init (onSuccess, onError) {
      // 初始化遁甲工具栏
      this.toolOptions = this.$u.obj.jsonClone(TOOL_LIST)
      
      this.bazi = new CM.Bazi(this.options)
      
      console.log(this.bazi)
      
      // 显示主界面，结束加载
      this.isLoaded = true
      
      this.$nextTick(() => {
        this.initPos().then(() => {
          onSuccess()
        })
        .catch((err) => {
          console.log(err)
        })
      })
    },
    
    // 初始化页面区域
    async initPos () {
      if (!this.posList) {
        this.posList = []
      }
      
      const stickyData = await this.getNodePos(this.stickyId)
      
      this.stickyHeight = stickyData.height
      
      // 当前是否处于激活状态
      this.posActive = []
      
      let top = 0
      const len = this.navData.length
      for (let i = 0; i < len; i++) {
        const id = this.idPrefix + this.navData[i].id
        const data = await this.getNodePos(id)
        
        // 记录页面区域位置
        if (i === 0) {
          top = data.top
        }
        
        if (i <= STICKY_POS_INDEX) {
          this.posList.push({
            // 增加3像素的容错
            start: data.top - top + SCROLL_BORDER_OFFSET
          })          
        }
        else {
          this.posList.push({
            // 增加3像素的容错
            start: data.top - top - this.stickyHeight + SCROLL_BORDER_OFFSET
          })
          // 从流年大运开始往下记录相交点
          this.createIntersectionObserver()
          .relativeToViewport({
            bottom: (this.windowHeight - this.stickyHeight - this.CustomBar - SCROLL_BORDER_OFFSET) * (-1)
          })
          .observe('#' + id, (res) => {
            if (this.scrollLock) {
              return
            }
            if (res.intersectionRatio === 1 && this.curIndex !== i) {
              this.curIndex = i
              this.$refs.scrollBar.scrollToIndex(i)
            }
            else if (res.intersectionRatio === 0 && this.curIndex === i && res.boundingClientRect.top >= 0) {
              this.curIndex = i - 1
              this.$refs.scrollBar.scrollToIndex(this.curIndex)
            }
          })
        }
        
        if (i > 0) {
          this.posList[i - 1].end = this.posList[i].start
        }
      }
    },
    
    // 获取指定id的位置
    getNodePos (id) {
      return new Promise((resolve, reject) => {
        let item = uni.createSelectorQuery().in(this).select('#' + id)
        item.boundingClientRect((data) => {
          if (data) {
            resolve(data)
          }
          else {
            reject('获取节点' + id + '信息失败')
          }
        }).exec()
      })
    },
    
    // 选定并跳转到指定位置
    scrollToPart (index, callback = () => {}) {
      // console.log(this.posList, index)
      this.curIndex = index
      const height = this.posList[index].start
      this.$refs.scrollBar.scrollToIndex(index)
      uni.pageScrollTo({
        duration: SCROLL_DURATION,
        scrollTop: height
      })
      // 开启滑动锁
      this.scrollLock = true
      this.wait(SCROLL_DURATION, 'bazi-scroll-finished').then(() => {
        // 动画时间结束后，关闭滑动锁
        this.scrollLock = false
      })
    },
    
    // 选择了导航
    navSelectHandler (item, index) {
      if (!this.scrollLock) {
        this.scrollToPart(index)
      }
    },
    
    // 吸顶触发
    stickyHandler (res) {
      if (this.scrollLock) { return }
      
      if (res && this.curIndex !== STICKY_POS_INDEX) {
        // 吸顶，聚焦到八字部分
        this.curIndex = STICKY_POS_INDEX
        this.$refs.scrollBar.scrollToIndex(this.curIndex)
      }
      else if (!res && this.curIndex !== STICKY_POS_INDEX - 1) {
        // 吸顶接触，聚焦到前一个部分
        this.curIndex = STICKY_POS_INDEX - 1
        this.$refs.scrollBar.scrollToIndex(this.curIndex)
      }
    },
    
    // 大运选择
    bigDestinySetHandler (index) {      
      if (index !== this.baziProxy.bigDestinySelected) {
        this.bazi.selectBigDestiny(index)
      }
    },
    
    // 流年选择
    baziYearsSetHandler (index) {
      if (index !== this.baziProxy.baziYearsSelected) {
        this.bazi.selectBaziYears(index)
      }
    },
    
    // 流月选择
    baziMonthsSetHandler (index) {
      if (index !== this.baziProxy.baziMonthsSelected) {
        this.bazi.selectBaziMonths(index)
      }
    },
    
    // 是否显示流年切换触发，因为小程序端v-model无效
    destinyShowHandler (val) {
      this.isDestinyShow = val
    },
    
    // 对象比较工具触发的特殊事件
    onSpecialChange (data) {
      // 不要忘记更新八卦工具
      this.$refs.baguaTool.setCompare(data.type, data.item)
    },
    
    // 八卦工具设定信息变化
    toolChangeHandler (setting) {
      this.toolSetting = setting
      // 更新遁甲局的状态
      if (this.isLoaded) {
        // 处理八卦标记事件
        this.baguaElementEvent(this.toolSetting, 'bazi')
      }
    },
  },
  mounted () {

  }
}