
// 容器边距
const PADDING = 10

// 每一个字的宽高
const SIZE = 35

// 最大层数
const MAX_LAYER = 5

export default {
  name: 'liurenCircle',
  props: {
    // 索引，0对应子位，11对应亥位，与手掌上十二地支位置相同
    index: {
      type: [Number, String],
      default: 0
    },
    // 深度，代表层数
    layer: {
      type: [Number, String],
      default: 1,
      validator (value) {
        const num = parseInt(value)
        return num >= 1 && num <= MAX_LAYER
      }
    },
  },
  data () {
    return {
    }
  },
  computed: {
    style () {
      const index = parseInt(this.index)
      let newIndex
      switch (index) {
        case 0: case 1: case 2: case 11:
          newIndex = 3 - (index + 1) % 12
          return this.sideStyle('bottom', newIndex)
          break
        case 3: case 4:
          newIndex = 4 - index
          return this.sideStyle('left', newIndex)
          break
        case 5: case 6: case 7: case 8:
          newIndex = index - 5
          return this.sideStyle('top', newIndex)
          break
        case 9: case 10:
          newIndex = index - 9
          return this.sideStyle('right', newIndex)
          break
        default:
      }
      return { display: 'none' }
    }
  },
  methods: {
    /* 
      计算正规位置的样式
      direction 处于那个方向 左上右下
      index 从上到下，或从左到右的索引数
    */
    sideStyle (direction = 'top', index = 0) {
      let result = {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: SIZE + 'px',
        height: SIZE + 'px'
      }
      const layer = parseInt(this.layer)
      const offset = MAX_LAYER - layer
      result[direction] = (offset * SIZE + PADDING) + 'px'
      
      // 其他样式
      if (direction === 'top' || direction === 'bottom') {
        result.left = (index * SIZE + (MAX_LAYER - 1) * SIZE + PADDING) + 'px'    
      }
      else {
        // 左右，垂直方向
        result.flexDirection = 'column'
        // 垂直方向也要确保文字居中
        result.textAlign = 'center'
        result.top = (index * SIZE + MAX_LAYER * SIZE  + PADDING) + 'px'  
      }
      return result
    }
  }
}