
export default {
  name: 'termTag',
  props: {
    /* 
      类型是气还是朔(节气和月相)
      气是term 朔是moon
    */
    type: {
      default: 'term',
      type: String
    },
    // 节气的索引值或者朔的索引值
    term: {
      default () {
        return {}
      },
      type: Object
    }
  },
  data () {
    return {
    }
  },
  computed: {
    // 标签是否镂空
    isOutlined () {
      return this.type === 'moon' ? true : false
    },
    
    // 标签是否浅色
    isLight () {
      return this.type === 'term' ? true : false
    },
    
    nameText () {
      if (this.type === 'term') {
        // 节气名称
        return this.term.name
      }
      // 月相名称
      return this.term.name + '月'
    },
    
    // 标签颜色
    tagColor () {
      if (this.type === 'term') {
        // 节气标签颜色
        return this.termColor(this.term.index)
      }
      // 月相标签颜色
      return this.moonColor(this.term.index)
    }
  },
  methods: {    
    // 节气对应的颜色
    termColor (index) {
      const colorList = [
        'sea', 'grey', 'background',
        'grass', 'sky', 'forest', 'grass', 'sky', 'forest',
        'red', 'pink', 'mauve', 'red', 'pink', 'mauve',
        'yellow', 'orange', 'brown', 'yellow', 'orange', 'brown',
        'sea', 'grey', 'background'
      ]
      return colorList[index]
    },
    
    // 月相对应颜色
    moonColor (index) {
      const colorList = [
        'purple', 'sea', 'yellow', 'forest'
      ]
      return colorList[index]
    },
  }
}