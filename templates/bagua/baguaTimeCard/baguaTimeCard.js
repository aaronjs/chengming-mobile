import baguaElement from '../baguaElement/baguaElement.vue'

import datePopupMixin from '@/mixins/datePopup.js'
import cmStyleMixin from '@/mixins/cmStyle.js'

import CM from '@/class/index.js'

/*
  @event change 日期变更时触发
*/

export default {
  name: 'baguaTimeCard',
  components: {
    baguaElement
  },
  mixins: [
    datePopupMixin,
    cmStyleMixin
  ],
  props: {
    /* @section 卡片标题样式 */
    // 卡片标题
    title: {
      default: '时间选择',
      type: String
    },
    
    /* @section 卡片额外样式 */
    // 卡片额外内容文字
    extra: {
      default: '',
      type: String
    },
    
    /* @section 卡片整体样式 */
    // 是否占满宽度
    isFull: {
      default: true,
      type: Boolean
    },
    // 是否有阴影
    isShadow: {
      default: true,
      type: Boolean
    },
    
    /*
      @section 样式选择
      chengming 承明板式
      tianzun 天尊板式 
    */
    mode: {
      default: 'chengming',
      type: String
    },
    // 干支显示几柱，4显示五柱到刻柱，3显示四柱到时柱
    ganzhiShow: {
      default: 4,
      type: [String, Number]
    },
  },
  data () {
    return {
      // 显示的日期
      date: new CM.Date(),
    }
  },
  computed: {
  },
  methods: {
    // 初始化
    init () {      
      // 初始化选择器默认时间
      this.initDatePicker()
      // 初始触发一次时间事件
      this.onDateChange()
    },
    
    // 将时间置为当前时间
    nowTimeHandler () {
      this.date = new CM.Date()
      this.onDateChange()
    },
    
    // 选定了时间
    confirmHandler (type, values, text, indexes) {
      let options = {}
      if (type === 'lunar') {
        // 农历定时
        options = {
          rulue: values[2],
          hour: values[3],
          min: values[4]
        }
      }
      else {
        options = {
          year: values[0],
          month: values[1],
          day: values[2],
          hour: values[3],
          min: values[4]
        }
      }
      const newDate = new CM.Date(options)
      if (newDate.enabled) {
        this.date = newDate
        this.onDateChange()
      }
      else {
        this.$u.msg.toast('不合法的日期')
      }
      // this.date.init(options)  
    },
    
    extraClickHandler () {
      this.$u.router.push('calendar').then(() => {})
      .catch((e) => {
        console.log(e)
      })
    },
    
    onDateChange () {
      this.$emit('change', this.date)
    }
  },
  created () {
    this.init()
  }
}