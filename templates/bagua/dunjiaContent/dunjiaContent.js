import baguaElement from '@/templates/bagua/baguaElement/baguaElement.vue'
import dunjiaExtra from '@/templates/bagua/dunjiaExtra/dunjiaExtra.vue'

import baguaElementSignMixin from '@/mixins/baguaElementSign.js'

import CM from '@/class/index.js'

// 九宫定位参数，需要和dunjiaExtra组件中的数据保持一致
// 容器边距
const PADDING = 10
// 宫位大小
const PALACE_SIZE = 80

// 外圈(正)定位参数，能放三个字
// 外圈(正)内容长度
const EXTRA_LENGTH = 80
// 外圈(正)内容宽度
const EXTRA_WIDTH = 30

export default {
  name: 'dunjiaContent',
  mixins: [
    baguaElementSignMixin
  ],
  components: {
    baguaElement,
    dunjiaExtra
  },
  props: {
    // 额外设定
    setting: {
      default: null,
      type: Object
    }
  },
  data () {
    return {
      // 遁甲对象
      dunjia: null,
      
      // 外圈神煞引导信息
      outerGodKeys: {},
      // 额外显示信息
      extraInfo: {},
      // 是否显示太乙
      isTaiyiFirstShow: false,
      isTaiyiShow: false,
      
      // 是否显示地支
      isZhiShow: this.$u.config.app.isDunjiaZhiShow
    }
  },
  computed: {
    // 代理对象，用于读取dunjia中的数据，避免报错
    dunjiaProxy () {
      return {
        // 神煞
        timeGod: !this.dunjia ? null : this.dunjia.timeGod,
        // 遁甲九宫
        palaces: !this.dunjia ? [] : this.dunjia.palaces,
        // 地支十二宫
        zhiPalaces: !this.dunjia ? [] : this.dunjia.zhiPalaces,
        // 二十四山向
        mountainPalaces: !this.dunjia ? [] : this.dunjia.mountainPalaces,
        // 太乙十六神
        taiyiPalaces: !this.dunjia || !this.isTaiyiFirstShow ? [] : this.dunjia.taiyiPalaces,
      }
    },
    
    // 当前局是否为山向局
    isPosDunjia () {
      if (!this.dunjia) {
        return false
      }
      
      return this.dunjia.types.indexOf('PosDunjia') >= 0
    },
    
    // 如果不显示地支，所有外圈层数都减1
    layerOffset () {
      return this.isZhiShow ? 0 : 1
    },
    
    // 第二圈的对象，山向显示24山向，时局显示十二地支
    secondLayerList () {
      if (!this.dunjia) {
        return []
      }

      return this.isPosDunjia ? this.dunjiaProxy.mountainPalaces : this.dunjiaProxy.zhiPalaces
    },
    
    secondLayerDivide () {
      if (!this.dunjia) {
        return 12
      }
      
      return this.isPosDunjia ? 24 : 12
    },
    
    // 外圈神煞代理对象
    outerGodProxy () {
      return {
        divide: this.outerGodKeys.divide ? this.outerGodKeys.divide : 12,
        key: this.outerGodKeys.key ? this.outerGodKeys.key : '',
        isSim: this.outerGodKeys.isSim ? true : false
      }
    },
    
    // 获取外圈神煞的对象列表
    outerGodsList () {
      // 根据引导信息获得
      if (!this.outerGodKeys.divide) {
        return []
      }
      
      if (this.outerGodKeys.divide === 8) {
        return this.dunjiaProxy.palaces
      }
      else if (this.outerGodKeys.divide === 12) {
        return this.dunjiaProxy.zhiPalaces
      }
      else if (this.outerGodKeys.divide === 24) {
        return this.dunjiaProxy.mountainPalaces
      }
      
      return []
    }
  },
  methods: {
    // 挂载遁甲对象，必须用此方法，因为prop传值会丢失对象
    setDunjia (dunjia) {
      this.dunjia = dunjia
      console.log(this.dunjia)
    },
    
    // 遁甲局的节点加载完成，通知父组件
    finishHandler () {
      this.$emit('finish')
    },
    
    // 遁甲对象发生改变，通知父组件
    onDunjiaChange () {
      this.$emit('change', this.dunjia, this.extraInfo)
    },
    
    // 特殊事件，通知父组件dunjiaTemplate
    onSpecialChange (data) {
      this.$emit('special', data)
    },
    
    // 响应父组件dunjiaTemplate的变动
    dunjiaSpecialHandler (data) {
      if (data.action === 'baguaCompare') {
        // 同步 对象比较选项
        this.compareSign = data.type
        this.compareSelected = data.selected
      }
    },
    
    // 根据工具参数设置遁甲局
    toolSetDunjia (setting) {
      console.log(setting)
      
      // 处理八卦标记事件 baguaCompare、seasonCheck
      this.baguaElementEvent(setting, 'dunjia')
      
      if (setting.type === 'outerGods' && setting.outerGods) {
        this.showOuterGod(setting.outerGods.god, setting.outerGods.extra, setting.type)
        return
      }
      
      if (setting.type === 'moveStar' && setting.hasOwnProperty('moveStar')) {
        this.moveStar(setting.moveStar, setting.type)
        return
      }
      
      if (setting.type === 'selectPos' && setting.selectPos) {
        this.selectPos(setting.selectPos.angle, setting.selectPos.posDunjiaTrans)
        return
      }
    },
    
    // 控制遁甲外圈神煞的显示
    showOuterGod (god, extra, type) {
      if (god === 'none') {
        this.outerGodKeys = {}
        return
      }
      // 首先控制遁甲对象获取数据，并返回外圈神煞引导信息
      this.outerGodKeys = this.dunjia.setOuterGod(god, extra)
      console.log(god, extra, this.outerGodKeys)
      // 更新额外信息
      if (this.outerGodKeys.extraInfo) {
        this.extraInfo[type] = this.outerGodKeys.extraInfo
      }
      // 之后触发遁甲对象改变事件
      this.onDunjiaChange()
    },
    
    // 移星换斗
    moveStar (options, type) {
      this.dunjia.moveStar(options.index)
      if (options.index !== 0) {
        this.extraInfo[type] = [{ text: options.name }]
      }
      else {
        this.extraInfo[type] = []
      }
      this.onDunjiaChange()
    },
    
    // 山向换局
    selectPos (angle, trans) {
      if (parseFloat(angle) !== this.dunjia.angle || trans !== this.dunjia.posDunjiaTrans) {
        // 重新起局
        const options = this.$u.obj.jsonClone(this.dunjia.options)
        options.angle = angle
        options.posDunjiaTrans = trans
        this.dunjia = new CM.PosDunjia(options)
        this.onDunjiaChange()
      }
    },
    
    // 显示太乙
    setTaiyi (isTaiyiShow) {
      this.isTaiyiShow = isTaiyiShow
      if (!this.isTaiyiFirstShow && isTaiyiShow) {
        this.isTaiyiFirstShow = isTaiyiShow
      }
    },
    
    // 更新额外信息
    updateExtraInfo (key, info) {
      this.extraInfo[key] = info
    },
    
    // 九宫定位
    palacePos (index) {
      // 获取九宫的行列
      const row = Math.floor(index / 3)
      const col = index % 3
      // 获取定位
      const top = 3 * EXTRA_WIDTH + row * PALACE_SIZE + PADDING
      const left = 3 * EXTRA_WIDTH + col * PALACE_SIZE + PADDING
      
      return {
        width: PALACE_SIZE + 'px',
        height: PALACE_SIZE + 'px',
        top: top + 'px',
        left: left + 'px'
      }
    },
    
    // 九宫上下边框
    palaceBorder (index) {
      let result = ['app-color-black_bd-bottom', 'app-color-black_bd-right']
      const row = Math.floor(index / 3)
      const col = index % 3
      if (row === 0) {
        result.push('app-color-black_bd-top')
      }
      if (col === 0) {
        result.push('app-color-black_bd-left')
      }
      return result.join(' ')
    },
    
    // 太乙十六宫定位
    taiyiPalacePos (index) {
      // 上、左
      const posData = [
        [2 * PADDING + PALACE_SIZE * 4, 2 * PADDING + PALACE_SIZE * 2],
        [2 * PADDING + PALACE_SIZE * 4, 2 * PADDING + PALACE_SIZE * 1],
        [2 * PADDING + PALACE_SIZE * 4, 2 * PADDING],
        [2 * PADDING + PALACE_SIZE * 3, 2 * PADDING],
        [2 * PADDING + PALACE_SIZE * 2, 2 * PADDING],
        [2 * PADDING + PALACE_SIZE * 1, 2 * PADDING],
        [2 * PADDING, 2 * PADDING],
        [2 * PADDING, 2 * PADDING + PALACE_SIZE * 1],
        [2 * PADDING, 2 * PADDING + PALACE_SIZE * 2],
        [2 * PADDING, 2 * PADDING + PALACE_SIZE * 3],
        [2 * PADDING, 2 * PADDING + PALACE_SIZE * 4],
        [2 * PADDING + PALACE_SIZE * 1, 2 * PADDING + PALACE_SIZE * 4],
        [2 * PADDING + PALACE_SIZE * 2, 2 * PADDING + PALACE_SIZE * 4],
        [2 * PADDING + PALACE_SIZE * 3, 2 * PADDING + PALACE_SIZE * 4],
        [2 * PADDING + PALACE_SIZE * 4, 2 * PADDING + PALACE_SIZE * 4],
        [2 * PADDING + PALACE_SIZE * 4, 2 * PADDING + PALACE_SIZE * 3],
      ]
      
      return {
        width: PALACE_SIZE + 'px',
        height: PALACE_SIZE + 'px',
        top: posData[index][0] + 'px',
        left: posData[index][1] + 'px'
      }
    },
    
    // 太乙九宫上下边框
    taiyiBorder (index) {
      let result = []
      if (index < 6 || index > 10) {
        result.push('app-color-black_bd-bottom')
      }
      if (index >= 5 && index <= 11) {
        result.push('app-color-black_bd-top')
      }
      if (index < 2 || index > 6) {
        result.push('app-color-black_bd-right')
      }
      if (index >= 1 && index <= 7) {
        result.push('app-color-black_bd-left')
      }
      return result.join(' ')
    },
    
    // 控制 门迫击刑入墓 的基础加粗显示
    basicDiseaseClass (item, bagua) {
      if (!bagua) {
        return ''
      }

      // 筛选出门迫、击刑、入墓的对象
      const diseases = item.basicRelation.relations.filter((relation) => {
        return relation.objs[0].id === bagua.id
      })
      // 没有问题
      if (diseases.length === 0) { return '' }
      
      let result = 'cm-bold'
      // 检查是否有击刑
      const isXing = diseases.some((relation) => {
        return relation.type === 'dunjiaXing'
      })
      // 击刑还要加上斜体
      return isXing ? result + ' cm-italic' : result
    }
  }
}