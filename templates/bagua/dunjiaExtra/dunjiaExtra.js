// 容器边距
const PADDING = 10

// 外圈(正)内容长度
const EXTRA_LENGTH = 80
// 外圈(正)内容宽度
const EXTRA_WIDTH = 30

// 最大层数
const MAX_LAYER = 3



export default {
  name: 'dunjiaExtra',
  props: {
    /*
      索引
      八分法时，左上角为0号，同九宫编号
      十二分法时，子位(坎卦处)为0号，同地支十二神编号
      十六分法时，子位(坎卦处)为0号，同太乙十六神编号
      二十四分法时，壬位(坎卦处)为0号，同二十四山向编号
    */
    extraIndex: {
      type: [Number, String],
      default: 0
    },
    // 深度，代表层数
    layer: {
      type: [Number, String],
      default: 1,
      validator (value) {
        const num = parseInt(value)
        return num >= 1 && num <= MAX_LAYER
      }
    },
    // 分法，只有8、12、16、24四种分法
    divide: {
      type: [Number, String],
      default: 12,
      validator (value) {
        const num = parseInt(value)
        return [8, 12, 16, 24].indexOf(num) !== -1
      }
    },
    /*
      align 文字排版，按照flex布局
      flex-start flex-end center 强制指定 居首 居尾 居中
      default 自适应排版
    */
    align: {
      type: String,
      default: 'default',
      validator (value) {
        return ['flex-start', 'flex-end', 'center', 'default'].indexOf(value) !== -1
      }
    }
  },
  computed: {
    extraStyle () {
      const divide = parseInt(this.divide)
      if (divide === 8) {
        return this.divide8
      }
      else if (divide === 12) {
        return this.divide12
      }
      else if (divide === 16) {
        
      }
      else if (divide === 24) {
        return this.divide24
      }
      return {}
    },
    
    // 八分法布局
    divide8 () {
      const index = parseInt(this.extraIndex)
      switch (index) {
        case 0:
          return this.cornerStyle(['left', 'top'])
          break
        case 2:
          return this.cornerStyle(['right', 'top'])
          break
        case 6:
          return this.cornerStyle(['left', 'bottom'])
          break
        case 8:
          return this.cornerStyle(['right', 'bottom'])
          break
        case 1: 
          return this.sideStyle('top', 1)
          break
        case 3: 
          return this.sideStyle('left', 1)
          break
        case 5: 
          return this.sideStyle('right', 1)
          break
        case 7: 
          return this.sideStyle('bottom', 1)
          break
        default:
      }
      return { display: 'none' }
    },
    
    // 12分法布局
    divide12 () {
      // 12分
      const divide = 12
      // 每个方向上有3个
      const groupItemsNum = 3
      const index = (parseInt(this.extraIndex) + 1) % divide
      const group = Math.floor(index / groupItemsNum)
      const offset = index % groupItemsNum
      if (group === 0) {
        return this.sideStyle('bottom', groupItemsNum - offset - 1)
      }
      else if (group === 1) {
        return this.sideStyle('left', groupItemsNum - offset - 1)
      }
      else if (group === 2) {
        return this.sideStyle('top', offset)
      }
      else if (group === 3) {
        return this.sideStyle('right', offset)
      }
      return { display: 'none' }
    },
    
    // 24分法布局
    divide24 () {
      // 24 分
      const divide = 24
      const index = parseInt(this.extraIndex)
      switch (index) {
        case 2:
          return this.cornerStyle(['left', 'bottom'])
          break
        case 8:
          return this.cornerStyle(['left', 'top'])
          break
        case 14:
          return this.cornerStyle(['right', 'top'])
          break
        case 20:
          return this.cornerStyle(['right', 'bottom'])
          break
        case 9: 
          return this.sideStyle('top', 0)
          break
        case 13:
          return this.sideStyle('top', 2)
          break
        case 10: case 11: case 12:
          return this.sideStyle('top', 1, 'center', index - 10)
        case 3: 
          return this.sideStyle('left', 2)
          break
        case 7:
          return this.sideStyle('left', 0)
          break
        case 4: case 5: case 6:
          return this.sideStyle('left', 1, 'center', 6 - index)
          break
        case 15:
          return this.sideStyle('right', 0)
          break
        case 19:
          return this.sideStyle('right', 2)
          break
        case 16: case 17: case 18:
          return this.sideStyle('right', 1, 'center', index - 16)
          break
        case 21:
          return this.sideStyle('bottom', 2)
          break
        case 1:
          return this.sideStyle('bottom', 0)
          break
        case 0:
          return this.sideStyle('bottom', 1, 'center', 0)
          break
        case 22: case 23:
          return this.sideStyle('bottom', 1, 'center', 24 - index)
          break
        default:
      }
      return { display: 'none' }
    }
  },
  methods: {
    // 计算边角的样式
    cornerStyle (corner = ['left', 'top']) {
      // 文字排版，分别为第3、2、1层
      let alignList = ['flex-start', 'center', 'flex-end']
      // 右边和左边相反
      if (corner[0] === 'right') {
        alignList = alignList.reverse()
      }
      
      const layer = parseInt(this.layer)
      const offset = MAX_LAYER - layer
      let result = {
        width: (EXTRA_WIDTH * 3) + 'px',
        height: EXTRA_WIDTH + 'px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: this.align === 'default' ? alignList[offset] : this.align
      }
      result[corner[0]] = PADDING + 'px'
      result[corner[1]] = (offset * EXTRA_WIDTH + PADDING) + 'px'
      return result
    },
    
    /* 
      计算正规位置的样式
      direction 处于那个方向 左上右下
      index 从上到下，或从左到右的索引数
      align 文字排版方向
      partIndex 在24分法中，四正位置还要被一分为三。此为单块元素索引，索引顺序从上到下，或从左到右。
    */
    sideStyle (direction = 'top', index = 0, align = 'center', partIndex = -1) {
      let result = {
        display: 'flex',
        alignItems: 'center'
      }
      const layer = parseInt(this.layer)
      const offset = MAX_LAYER - layer
      const margin = MAX_LAYER * EXTRA_WIDTH - EXTRA_LENGTH
      result[direction] = (offset * EXTRA_WIDTH + PADDING) + 'px'
      
      // 文字排版
      result.justifyContent = this.align === 'default' ? align : this.align
      
      // 其他样式
      if (direction === 'top' || direction === 'bottom') {
        // 上下，水平方向
        result.height = EXTRA_WIDTH + 'px'
        if (partIndex < 0) {
          result.width = EXTRA_LENGTH + 'px'
          result.left = (index * EXTRA_LENGTH + 3 * EXTRA_WIDTH + PADDING) + 'px'          
        }
        else {
          const partOffset = partIndex * (EXTRA_LENGTH / 3)
          result.width = (EXTRA_LENGTH / 3) + 'px'
          result.left = (index * EXTRA_LENGTH + 3 * EXTRA_WIDTH + PADDING + partOffset) + 'px'  
        }
      }
      else {
        // 左右，垂直方向
        result.flexDirection = 'column'
        // 垂直方向也要确保文字居中
        result.textAlign = 'center'
        result.width = EXTRA_WIDTH + 'px'
        if (partIndex < 0) {
          result.height = EXTRA_LENGTH + 'px'
          result.top = (index * EXTRA_LENGTH + 3 * EXTRA_WIDTH  + PADDING) + 'px'          
        }
        else {
          const partOffset = partIndex * (EXTRA_LENGTH / 3)
          result.height = (EXTRA_LENGTH / 3) + 'px'
          result.top = (index * EXTRA_LENGTH + 3 * EXTRA_WIDTH  + PADDING + partOffset) + 'px'   
        }
      }
      return result
    }
  }
}