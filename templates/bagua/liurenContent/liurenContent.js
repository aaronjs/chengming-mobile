import liurenCircle from '@/templates/bagua/liurenCircle/liurenCircle.vue'
import baguaElement from '@/templates/bagua/baguaElement/baguaElement.vue'

import baguaElementSignMixin from '@/mixins/baguaElementSign.js'

import CM from '@/class/index.js'

export default {
  name: 'liurenContent',
  components: {
    liurenCircle,
    baguaElement
  },
  mixins: [
    baguaElementSignMixin
  ],
  props: {
    // 额外设定
    setting: {
      default: null,
      type: Object
    }
  },
  data () {
    return {
      // 六壬对象
      liuren: null,
      // 外圈神煞引导信息
      outerGodKeys: {},
      // 额外显示信息
      extraInfo: {},
      
      // 是否显示地支
      isZhiShow: this.$u.config.app.isLiurenZhiShow
    }
  },
  computed: {
    // 代理对象，用于读取dunjia中的数据，避免报错
    dunjiaProxy () {
      return {
        // 神煞
        timeGod: !this.liuren ? null : this.liuren.timeGod,
        // 地支十二宫
        zhiPalaces: !this.liuren ? [] : this.liuren.zhiPalaces,
      }
    },
    
    // 空亡元素
    keyEmpties () {
      return this.dunjiaProxy.timeGod ? this.dunjiaProxy.timeGod.keyEmpty : []
    },
    
    // 如果不显示地支，所有外圈层数都减1
    layerOffset () {
      return this.isZhiShow ? 0 : 1
    }
  },
  methods: {
    // 挂载遁甲对象，必须用此方法，因为prop传值会丢失对象
    setLiuren (liuren) {
      this.liuren = liuren
      console.log(this.liuren)
    },
    
    // 遁甲局的节点加载完成，通知父组件
    finishHandler () {
      this.$emit('finish')
    },
    
    // 响应父组件dunjiaTemplate的变动
    liurenSpecialHandler (data) {
      if (data.action === 'baguaCompare') {
        // 同步 对象比较选项
        this.compareSign = data.type
        this.compareSelected = data.selected
      }
    },
    
    // 遁甲对象发生改变，通知父组件
    onLiurenChange () {
      this.$emit('change', this.liuren, this.extraInfo)
    },
    
    // 特殊事件，通知父组件dunjiaTemplate
    onSpecialChange (data) {
      this.$emit('special', data)
    },
    
    // 根据工具参数设置六壬
    toolSetLiuren (setting) {
      console.log(setting)
      
      // 处理八卦标记事件 baguaCompare、seasonCheck
      this.baguaElementEvent(setting, 'liuren')
      
      /*
      if (setting.type === 'outerGods' && setting.outerGods) {
        this.showOuterGod(setting.outerGods.god, setting.outerGods.extra, setting.type)
        return
      }
      
      if (setting.type === 'moveStar' && setting.hasOwnProperty('moveStar')) {
        this.moveStar(setting.moveStar, setting.type)
        return
      }
      
      if (setting.type === 'selectPos' && setting.selectPos) {
        this.selectPos(setting.selectPos.angle, setting.selectPos.posDunjiaTrans)
        return
      }
      */
    },
  }
}