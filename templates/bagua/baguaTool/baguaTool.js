import outerGods from './tools/outerGods/outerGods.vue'
import moveStar from './tools/moveStar/moveStar.vue'
import selectPos from './tools/selectPos/selectPos.vue'
import baguaCompare from './tools/baguaCompare/baguaCompare.vue'
import seasonCheck from './tools/seasonCheck/seasonCheck.vue'

import utils from '@/utils/index.js'
import consts from './consts.js'
import User from '@/class/user/user.js'

/*
  @event change 返回工具当前的指令
*/

// 初始默认的功能列表
const INIT_TOOLS = [
  // 无功能
  { key: 'none' },
  // 对象查看
  // { key: 'baguaShow' },
  // 对象比较
  { key: 'baguaCompare' },
]

const TOOL_LIST = {
  none: { name: '选择功能', icon: 'iconfont icon-reeor' },
  baguaShow: { name: '对象信息' },
  seasonCheck: { name: '时令查看', icon: 'iconfont icon-ontimeshipment' },
  baguaCompare: { name: '对象比较', icon: 'iconfont icon-libra' },
  outerGods: { name: '外圈神煞', icon: 'iconfont icon-add-fill' },
  taiyi: { name: '穿太乙', icon: 'iconfont icon-content_xuanwu' },
  moveStar: { name: '移星换斗', icon: 'iconfont icon-collection-fill' },
  selectPos: { name: '山向选局', icon: 'iconfont icon-home' }
}

export default {
  name: 'baguaTool',
  components: {
    outerGods,
    moveStar,
    selectPos,
    baguaCompare,
    seasonCheck
  },
  props: {
    // 工具总共具有的功能列表
    options: {
      type: Array,
      default () {
        return []
      }
    },
    // 在功能列表中，初始选中的功能
    current: {
      type: Number,
      default: 0
    }
  },
  data () {
    return {
      toolList: TOOL_LIST,
      
      // 用户数据
      user: new User(),
      
      // 当前选中的功能
      currentIndex: this.current,
      
      // 当前的功能列表
      currentOptions: INIT_TOOLS.concat(this.options),
      
      // 设定参数
      currentSetting: {
        // 工具分类
        type: this.options[this.current] ? this.options[this.current].key : 'none',
        // 工具功能进一步细分，一般为起局参数中的type类别字段
        subType: '',
        name: this.options[this.current] ? TOOL_LIST[this.options[this.current].key].name : '',
        height: 0
      },
      
      // 已经加载完成的工具
      loadedTools: []
    }
  },
  watch: {
    options (newv, old) {
      this.currentOptions = INIT_TOOLS.concat(this.options)
      this.changeTool(this.currentIndex)
    }
  },
  methods: {
    init () {
      if (this.currentOptions && Array.isArray(this.currentOptions) && this.currentOptions.length > 0 
        && this.currentIndex >= 0 && this.currentIndex < this.currentOptions.length) {
        this.changeTool(this.currentIndex)
      }
    },
    
    // 因为工具切换，工具栏的高度发生变化，需要更新setting
    changeTool (index) {
      this.currentIndex = index
      this.currentSetting.type = this.currentOptions[index].key
      this.currentSetting.name = TOOL_LIST[this.currentOptions[index].key].name
      if (this.currentOptions[index].type) {
        // 二级分类
        this.currentSetting.subType = this.currentOptions[index].type
      }
      if (this.currentOptions[index].hasOwnProperty('angle')) {
        // 山向初始角度
        this.currentSetting.srcAngle = this.currentOptions[index].angle
      }
     
      
      if (this.loadedTools.indexOf(this.currentSetting.type) < 0) {
        // 若工具没有加载，则进行加载
        this.loadedTools.push(this.currentSetting.type)
      }
      
      
      // 界面处理高度
      this.$nextTick(() => {        
        // 一定要等到视图更新后再获取高度
        let item = uni.createSelectorQuery().in(this).select('.bagua-tool-wrapper')
        item.boundingClientRect((data) => {
          // 获取高度以后
          this.currentSetting.height = data.height
          // 最终确定样式
          this.onChange()
        }).exec()        
      })
    },
    
    // 判断工具是否加载
    isToolLoaded (key) {
      return this.loadedTools.indexOf(key) >= 0
    },
    
    onChange () {
      this.$emit('change', this.currentSetting)
    },
    
    // 工具状态改变触发
    changeHandler (obj) {
      if (obj && typeof obj === 'object') {
        this.currentSetting[this.currentSetting.type] = {
          ...obj
        }        
      }
      else {
        this.currentSetting[this.currentSetting.type] = obj 
      }
      this.onChange()
    },
    
    // 切换工具
    toolOffsetHandler: utils.common.throttle(function (offset) {
      if (!this.user.baguaAppCheck()) {
        // 未解锁玩家不能使用进阶工具
        return
      }
      
      const len = this.currentOptions.length
      this.currentIndex = (this.currentIndex + len + (offset % len)) % len
      this.changeTool(this.currentIndex)
    }, consts.changeInterval),
    
    // 工具切换面板弹起
    toolSelectionHandler () {
      this.$nextTick(() => {
        this.$refs.toolsPopup.show()
      })
    },
    
    // 选中工具
    selectToolHandler (item, index) {
      if (!this.user.baguaAppCheck() && item.key !== 'none') {
        // 未解锁玩家不能使用进阶工具
        return
      }
      
      this.currentIndex = index
      this.changeTool(this.currentIndex)
      this.$nextTick(() => {
        this.$refs.toolsPopup.hide()
      })
    },
    
    /* @section 以下为子工具特有方法 */
    setCompare (type, data) {
      if (this.$refs.baguaCompare) {
        this.$refs.baguaCompare.setCompare(type, data)
      }
    }
  },
  mounted () {
    this.init()
  }
}