import CM from '@/class/index.js'

import utils from '@/utils/index.js'
import consts from '../../consts.js'

export default {
  name: 'selectPos',
  props: {
    // 初始的角度
    angle: {
      type: Number,
      default: 0
    },
    // 类别，相当于posDunjiaType
    type: {
      type: String,
      default: 'year'
    }
  },
  data () {
    return {
      // initIndex 初始选中的索引
      
      // 所有局选项
      selectionsList: [],
      // 当前选中局角度的索引
      curIndex: 0,
      // 局类型选项
      typesList: ['正盘', '归一', '合十', '反转'],
      // 当前选中局类型索引
      curTypeIndex: 0,
    }
  },
  computed: {
    // 选中局角度
    angleText () {
      if (!this.selectionsList[this.curIndex]) {
        return ''
      }
      return `${this.selectionsList[this.curIndex].start.toFixed(2)}° ~ ${this.selectionsList[this.curIndex].end.toFixed(2)}°`
    },
    
    // 用于Picker的角度列表
    posPickerList () {
      const newList =  this.selectionsList.map((item) => {
        return {
          text: item.text,
          value: item.start
        }
      })
      return [newList]
    },
    
    // 对外输出的数据
    stateObj () {
      if (this.selectionsList.length === 0 || !this.selectionsList || !this.selectionsList[this.curIndex]) {
        return null
      }
      let result = {}
      // 山向局的变种
      result.posDunjiaTrans = this.typesList[this.curTypeIndex]
      // 角度
      result.angle = this.selectionsList[this.curIndex].start
      if (this.curIndex === this.initIndex) {
        // 若还原回了最初的局，则获取到最初的角度信息
        result.angle = this.angle
      }
      // 必须转换成字符串，否则报错
      result.angle = result.angle.toFixed(2)
      return result
    }
  },
  methods: {
    // 初始化组件
    init () {
      this.selectionsList = CM.PosDunjia.getNeighborSections(this.angle, this.type)
      this.curIndex = this.selectionsList.findIndex((item) => {
        return item.start <= this.angle && item.end >= this.angle
      })
      if (this.curIndex < 0) { this.curIndex = 0 }
      // 保存初始索引
      this.initIndex = this.curIndex
    },
    
    typeOffsetHandler: utils.common.throttle(function (offset) {
      const len = this.typesList.length
      this.curTypeIndex = (this.curTypeIndex + len + (offset % len)) % len
      this.onChange()
    }, consts.changeInterval),
     
    angleOffsetHandler: utils.common.throttle(function (offset) {
      const len = this.selectionsList.length
      this.curIndex = (this.curIndex + len + (offset % len)) % len
      this.onChange()
    }, consts.changeInterval),
    
    // 呼出选局Picker
    clickPosSetHandler () {
      // this.$refs.posPicker.setValues([this.curIndex])
      this.$refs.posPicker.show(null, () => {
        this.$refs.posPicker.setValues([this.curIndex])
      })
    },
    
    // 重置初始
    resetHandler () {
      const tempTypeIndex = this.curTypeIndex
      const tempIndex = this.curIndex
      this.curTypeIndex = 0
      this.curIndex = this.initIndex
      if (tempTypeIndex !== this.curTypeIndex || tempIndex !== this.curIndex) {
        this.onChange()
      }
    },
    
    // 选中局
    selectPosHandler (vals, text, indexes) {
      this.curIndex = indexes[0]
      this.onChange()
    },
    
    // 改变触发
    onChange () {
      this.$emit('change', this.stateObj)
    }
  },
  created () {
    this.init()
  }
}