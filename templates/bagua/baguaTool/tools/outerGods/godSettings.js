// 此处定义一些会多次用到的选项
const commonExtraList = {
  zhi: [
    { text: '定局支', value: 'key' },
    { text: '时支', value: 'hour' },
    { text: '日支', value: 'day' },
    { text: '月支', value: 'month' },
    { text: '年支', value: 'year' }
  ],
  gan: [
    { text: '定局干', value: 'key' },
    { text: '时干', value: 'hour' },
    { text: '日干', value: 'day' },
    { text: '月干', value: 'month' },
    { text: '年干', value: 'year' }
  ],
  ganzhi: [
    { text: '定局柱', value: 'key' },
    { text: '时柱', value: 'hour' },
    { text: '日柱', value: 'day' },
    { text: '月柱', value: 'month' },
    { text: '年柱', value: 'year' }
  ],
  palaces: [
    { text: '乾', value: '8' },
    { text: '坤', value: '2' },
    { text: '离', value: '1' },
    { text: '坎', value: '7' },
    { text: '震', value: '3' },
    { text: '兑', value: '5' },
    { text: '巽', value: '0' },
    { text: '艮', value: '6' }
  ],
  mountain: [
    { text: '以山定', value: 'mountain' },
    { text: '以向定', value: 'position' },
  ]
}

const commonSettingList = {
  none: { text: '无', value: 'none' },
  twelvePalace: { text: '十二宫', value: 'twelvePalace',
    extra: [
      {
        title: '参考地支位',
        selected: 0,
        list: commonExtraList.zhi
      }
    ]
  },
  yuejiang: { text: '月将', value: 'yuejiang',
    extra: [
      {
        title: '起排地支位',
        selected: 0,
        list: commonExtraList.zhi
      }
    ]
  },
  guiGod: { text: '神将', value: 'guiGod',
    extra: [
      {
        title: '阴阳贵人',
        selected: 0,
        list: [
          { text: '自动', value: 'auto' },
          { text: '阳贵人', value: 'solar' },
          { text: '阴贵人', value: 'lunar' }
        ]
      },
      {
        title: '贵人用神参考',
        // 默认日干为用神
        selected: 2,
        list: commonExtraList.gan
      },
      {
        title: '排布方式',
        selected: 0,
        list: [
          { text: '参考地盘', value: 'ground' },
          { text: '参考天盘', value: 'sky' }
        ]
      },
      {
        title: '天盘起排地支位',
        description: '排布方式为【参考天盘】时此选项有效',
        selected: 0,
        list: commonExtraList.zhi
      }
    ]
  },
  taiyin: { text: '太阴', value: 'taiyin',
    extra: [
      {
        title: '起排地支位',
        selected: 0,
        list: commonExtraList.zhi
      }
    ]
  },
  liurenGan: { text: '天干', value: 'liurenGan',
    extra: [
      {
        title: '排布方式',
        selected: 0,
        list: [
          // 把一天的十二个时辰套在地支上
          { text: '穿壬遁排法', value: 'dunjia' },
          // 根据用神和用神的旬，配合天盘套地支
          { text: '六壬排法', value: 'liuren' },
        ]
      },
      {
        title: '天干用神参考',
        description: '排布方式为【六壬排法】时此选项有效',
        // 默认日干支为用神
        selected: 2,
        list: commonExtraList.ganzhi
      },
      {
        title: '天盘起排参照',
        description: '排布方式为【六壬排法】时此选项有效',
        selected: 0,
        list: commonExtraList.zhi
      }
    ]
  },
  bigYounian: { text: '大游年', value: 'bigYounian',
    extra: [
      {
        title: '伏位选择',
        selected: 0,
        list: commonExtraList.palaces
      }
    ]
  },
}

const godSettings = {
  // 时局神煞选项
  timeDunjia: [
    commonSettingList.none,
    // 时局的十二建和山向十二建排法不同
    { text: '十二建星', value: 'jianchu',
      extra: [
        {
          title: '起排地支位',
          selected: 0,
          list: commonExtraList.zhi
        }
      ]
    },
    commonSettingList.twelvePalace,
    commonSettingList.yuejiang,
    commonSettingList.guiGod,
    commonSettingList.taiyin,
    commonSettingList.liurenGan,
    commonSettingList.bigYounian,
  ],
  // 山向神煞选项
  posDunjia: [
    commonSettingList.none,
    // 时局的十二建和山向十二建排法不同
    { text: '十二建星', value: 'jianchu',
      extra: [
        {
          title: '山向选择',
          selected: 0,
          list: commonExtraList.mountain
        }
      ]
    },
    { text: '十二长生', value: 'twelveState',
      extra: [
        {
          title: '山向选择',
          selected: 0,
          list: commonExtraList.mountain
        }
      ]
    },
    { text: '将神/黄泉煞', value: 'jiangGod' },
    { text: '关联八卦', value: 'relativeBagua' },
    { text: '小游年', value: 'smallYounian',
      extra: [
        {
          title: '山向选择',
          selected: 0,
          list: commonExtraList.mountain
        }
      ]
    },
    commonSettingList.bigYounian,
    { text: '其他神煞', value: 'otherPosGod',
      extra: [
        {
          title: '神煞选择',
          selected: 0,
          list: [
            { text: '桃花', value: 'taohua' },
            { text: '文昌', value: 'wenchang' }
          ]
        },
        {
          title: '山向选择',
          selected: 0,
          list: commonExtraList.mountain
        }
      ]
    }
  ]
}

export default godSettings