import godSettings from './godSettings.js'

import utils from '@/utils/index.js'
import consts from '../../consts.js'

export default {
  name: 'outerGods',
  props: {
    // 外圈分类
    type: {
      default: 'timeDunjia',
      type: String
    }
  },
  data () {
    return {
      // 选项列表
      godList: godSettings[this.type] ? this.$u.obj.jsonClone(godSettings[this.type]) : [],
      
      // 当前选中的选项
      currentIndex: 0,
      
      // 详细规格选项模态框的滚动位置
      modalScrollTop: 0,
    }
  },
  watch: {
    currentGod: {
      handler () {
        // 发出指令变更事件
      },
      deep: true
    }
  },
  computed: {
    // 神煞选择器
    godSelectionsList () {
      return [this.godList]
    },
    
    // 当前选中的对象
    currentGod () {
      if (!Array.isArray(this.godList) || this.godList.length <= 0) {
        return null
      }
      
      return this.godList[this.currentIndex]
    },
    
    // 选中对象的名称
    currentName () {
      if (!this.currentGod) {
        return ''
      }
      return this.currentGod.text
    },
    
    // 选中对象附加信息
    currentExtra () {
      if (!this.currentGod || !this.currentGod.extra) {
        return ''
      }
      
      // 组合附加信息
      const extraLen = this.currentGod.extra.length
      let result = ''
      for (let i = 0; i < extraLen; i++) {
        const index = this.currentGod.extra[i].selected ? this.currentGod.extra[i].selected : 0
        if (i === 0) {
          result = result + this.currentGod.extra[i].list[index].text
        }
        else {
          result = result + ' / ' + this.currentGod.extra[i].list[index].text
        }
      }
      return result
    },
    
    // 当前的状态对象
    stateObj () {
      if (!this.currentGod) {
        return {}
      }
      
      let result = {
        god: this.currentGod.value
      }
      
      if (!this.currentGod.extra) {
        return result
      }
      
      const extraLen = this.currentGod.extra.length
      let extraVal = ''
      for (let i = 0; i < extraLen; i++) {
        const index = this.currentGod.extra[i].selected ? this.currentGod.extra[i].selected : 0
        if (i === 0) {
          extraVal = extraVal + this.currentGod.extra[i].list[index].value
        }
        else {
          extraVal = extraVal + ' ' + this.currentGod.extra[i].list[index].value
        }
      }
      result.extra = extraVal
      return result
    }
  },
  methods: {
    // 切换外圈显示神煞
    currentOffsetHandler: utils.common.throttle(function (offset) {
      const len = this.godList.length
      this.currentIndex = (this.currentIndex + len + (offset % len)) % len
      this.onChange()
    }, consts.changeInterval),
    
    // 点击呼出神煞选择
    clickGodSetHandler () {
      this.$refs.godPicker.show(null, () => {
        this.$refs.godPicker.setValues([this.currentIndex])
      })
    },
    
    // 点击呼出神煞规格选择 
    extraHandler () {
      this.$refs.extraPicker.show()
    },
    
    // 神煞规格变化触发
    extraChangeHandler (value, index) {
      const extraIndex = this.currentGod.extra[index].list.findIndex((extra) => {
        return extra.value === value
      })
      if (extraIndex >= 0) {
        this.godList[this.currentIndex].extra[index].selected = extraIndex
        this.onChange()
      }
    },
    
    // 点击关闭神煞规格选择
    closeExtraHandler () {
      this.$refs.extraPicker.hide()
    },
    
    // 选中神煞
    selectGodHandler (vals, text, indexes) {
      this.currentIndex = indexes[0]
      this.onChange()
    },
    
    // 模态框滚动到顶
    modalScrollToTop () {
      // 滚到最上方
      // 为了填uni-app的坑，使得scrollTop能够变化，触发感应
      this.$nextTick(() => {
        this.modalScrollTop = 0
      })
      this.modalScrollTop = -1
    },
    
    // 状态变化触发
    onChange () {
      this.$emit('change', this.stateObj)
    }
  }
}