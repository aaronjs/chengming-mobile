export default {
  name: 'seasonCheck',
  data () {
    return {
      isSeasonCheck: false,
      
      colorList: this.$u.config.app.seasonStateColors,
      txtList: ['旺', '相', '休', '囚', '死']
    }
  },
  methods: {
    seasonCheckHandler (val) {
      this.isSeasonCheck = val
      this.$emit('change', {
        isSeasonCheck: val,
      })
    }
  }
}