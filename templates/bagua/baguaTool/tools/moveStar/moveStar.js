import utils from '@/utils/index.js'
import consts from '../../consts.js'

export default {
  name: 'moveStar',
  data () {
    return {
      moveStarList: ['本局', '顺转一宫', '顺转二宫', '顺转三宫', '顺转四宫', '顺转五宫', '顺转六宫', '顺转七宫'],
      curIndex: 0
    }
  },
  computed: {
    
  },
  methods: {
    currentOffsetHandler: utils.common.throttle(function (offset = 0) {
      const len = this.moveStarList.length
      this.curIndex = (this.curIndex + len + (offset % len)) % len
      this.onChange()
    }, consts.changeInterval),
    
    onChange () {
      this.$emit('change', {
        index: this.curIndex,
        name: this.moveStarList[this.curIndex]
      })
    }
  }
}