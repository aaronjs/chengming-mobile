import baguaElement from '@/templates/bagua/baguaElement/baguaElement.vue'

import CM from '@/class/index.js'

export default {
  name: 'baguaCompare',
  components: {
    baguaElement
  },
  data () {
    return {
      isBaseActive: false,
      isOppositeActive: false,
      
      baseObj: null,
      oppositeObj: null,
      relation: null
    }
  },
  watch: {
    baseObj (val) {
      this.updateRelation()
    },
    oppositeObj (val) {
      this.updateRelation()
    }
  },
  computed: {
    relationTxt () {
      if (!this.relation) {
        return ''
      }
      
      let typeList = []
      let txtList = []
      let txtIndex = -1
      const len = this.relation.relations.length
      for (let i = 0; i < len; i++) {
        const item = this.relation.relations[i]
        typeList.push(item.type)
        if (i - 1 >= 0 && typeList[i - 1] === item.type) {
          txtList[txtIndex] = txtList[txtIndex] + '/' + item.txt
        }
        else {
          txtList.push(item.txt)
          txtIndex++
        }
      }
      
      return txtList.join(' ')
    }
  },
  methods: {
    // 点击本位区域触发
    baseClickHandler () {
      this.isBaseActive = !this.isBaseActive
      if (this.isOppositeActive) {
        this.isOppositeActive = false
      }
      const active = this.isBaseActive ? 'base' : 'none'
      this.$emit('change', {
        active
      })
    },
    
    // 点击对位区域触发
    oppositeClickHandler () {
      this.isOppositeActive = !this.isOppositeActive
      if (this.isBaseActive) {
        this.isBaseActive = false
      }
      const active = this.isOppositeActive ? 'opposite' : 'none'
      this.$emit('change', {
        active
      })
    },
    
    // 查看详细触发
    detailClickHandler () {
    },
    
    // 获取比较对象
    setCompare (type, data) {
      if (type === 'base') {
        this.baseObj = data
      }
      else if (type === 'opposite') {
        this.oppositeObj = data
      }
      else {
        this.baseObj = null
        this.oppositeObj = null
      }
    },
    
    // 计算新的对象关系
    updateRelation () {
      if (!this.baseObj || !this.oppositeObj) {
        this.relation = null
        return
      }
      
      const objList = [this.baseObj, this.oppositeObj]
      // 方法列表
      const methods = [
        // 十神、十二长生
        'tenGod', 'twelveState',
        // 天干冲合
        'ganHe', 'ganChong',
        // 地支刑冲破害合
        'xing', 'zhiChong', 'po', 'hai', 'zhiHe', 'gui']
      this.relation = new CM.Relation(objList, methods)   
    }
  }
}