import dunjiaTemplate from '@/templates/bagua/dunjiaTemplate/dunjiaTemplate.vue'
import baziTemplate from '@/templates/bagua/baziTemplate/baziTemplate.vue'
import liurenTemplate from '@/templates/bagua/liurenTemplate/liurenTemplate.vue'

export default {
  name: 'baguaTemplate',
  components: {
    dunjiaTemplate,
    baziTemplate,
    liurenTemplate
  },
  props: {
    options: {
      type: Object,
      default: null
    }
  },
  data () {
    return {
      // 悬浮按钮距离顶部高度
      floatTop: this.CustomBar + 10,
      
      // 起局选项
      settingItems: [
        {
          icon: 'iconfont icon-save',
          text: '保存'
        },
        {
          icon: 'iconfont icon-calendar',
          text: '日历'
        }
      ]
    }
  },
  computed: {
    // 是否显示内容
    isShow () {
      if (this.options && typeof this.options === 'object' 
        && typeof this.options.type === 'string') {
        return true  
      }
      return false
    },
    
    // 是否是遁甲
    isDunjia () {
      if (!this.options || typeof this.options !== 'object') {
        return false
      }
      return this.options.type.indexOf('dunjia') >= 0 || this.options.type.indexOf('Dunjia') >= 0
    },
    
    // 是否是八字
    isBazi () {
      if (!this.options || typeof this.options !== 'object') {
        return false
      }
      return this.options.type.indexOf('bazi') >= 0 || this.options.type.indexOf('Bazi') >= 0
    },
    
    // 是否是六壬
    isLiuren () {
      if (!this.options || typeof this.options !== 'object') {
        return false
      }
      return this.options.type.indexOf('liuren') >= 0 || this.options.type.indexOf('Liuren') >= 0
    }
  },
  methods: {
    // 悬浮按钮点击
    floatClickHandler (item, index) {
      console.log(item, index)
      if (item.text === '保存') {
        
      }
    }
  }
}