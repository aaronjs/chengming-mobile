import baguaElement from '@/templates/bagua/baguaElement/baguaElement.vue'
import baguaTool from '@/templates/bagua/baguaTool/baguaTool.vue'
import liurenContent from '@/templates/bagua/liurenContent/liurenContent.vue'

import baguaElementSignMixin from '@/mixins/baguaElementSign.js'

import CM from '@/class/index.js'
import User from '@/class/user/user.js'

// 六壬工具栏功能
const TOOL_LIST = [
  { key: 'seasonCheck' },
]

export default {
  name: 'liurenTemplate',
  components: {
    baguaTool,
    baguaElement,
    liurenContent
  },
  mixins: [
    baguaElementSignMixin
  ],
  props: {
    options: {
      default () {
        return {}
      },
      type: Object
    }
  },
  data () {
    return {
      CustomBar: this.CustomBar,
      
      user: new User(),
      
      // 六壬工具设定
      toolOptions: [],
      // 六壬工具设定信息
      toolSetting: null,
      
      isLoaded: false,
      // 六壬局对象
      liuren: null,
      // 六壬起局配置
      liurenOptions: {...this.options},
      // 六壬局额外显示信息
      liurenExtraInfo: [],
      
      // 六壬贵神选择器
      guiGodPickerData: [
        { value: 'auto', text: '自动(卯酉分)' },
        { value: 'solar', text: '阳贵人' },
        { value: 'lunar', text: '阴贵人' }
      ]
    }
  },
  computed: {
    // 遁甲模板样式, 与工具栏的高度相关
    templateStyle () {
      if (!this.toolSetting) {
        return {}
      }
      return {
        paddingBottom: this.toolSetting.height + 'px'
      }
    },
    
    // 公历生日
    solarTimeText () {
      if (!this.liuren) {
        return ''
      }
      return this.liuren.date.getSolarName() + ' ' + this.liuren.date.getWeekDay() + ' ' + this.liuren.date.getTime()
    },
    
    // 农历生日
    lunarTimeText () {
      if (!this.liuren) {
        return ''
      }
      return this.liuren.date.getLunarName() + ' ' + this.liuren.date.ganzhi[3].zhi.name + '时'
    },
  },
  methods: {
    // 初始化
    init (onSuccess, onError) {
      // 初始化工具栏
      this.toolOptions = this.$u.obj.jsonClone(TOOL_LIST)
      // 排六壬局
      this.liuren = new CM.Liuren(this.options)
      
      // 显示主界面，结束加载
      this.isLoaded = true
      this.onLoadingSuccess = onSuccess
      
      if (this.liuren) {
        this.$nextTick(() => {
          // 与六壬主体同步
          this.$refs.liurenContent.setLiuren(this.liuren)
        })
      }
    },
    
    // 选六壬的用神
    selectLiurenKeyGanzhiHandler () {
      this.$refs.liurenKeyGanzhiPicker.show()
    },
    
    // 选六壬的宿命
    selectLiurenShengxiaoHandler () {
      this.$refs.liurenShengxiaoPicker.show()
    },
    
    // 选六壬的阴阳贵人
    selectGuiGodTypeHandler () {
      this.$refs.guiGodTypePicker.show()
    },
    
    // 修改用神触发
    changeKeyGanzhiHandler (val) {
      this.liurenOptions.liurenKeyGanzhi = val
      this.liuren.createLiuren(this.liurenOptions.liurenKeyGanzhi, this.liurenOptions.liurenShengxiao, this.liurenOptions.guiGodType)
    },
    
    // 修改宿命触发
    changeShengxiaoHandler (val) {
      this.liurenOptions.liurenShengxiao = val
      this.liuren.updateDestiny(this.liurenOptions.liurenShengxiao)
    },
    
    // 修改阴阳贵人触发
    changeGuiGodTypeHandler (val) {
      console.log(val)
      this.liurenOptions.guiGodType = val[0]
      this.liuren.createLiuren(this.liurenOptions.liurenKeyGanzhi, this.liurenOptions.liurenShengxiao, this.liurenOptions.guiGodType)
    },
    
    // 遁甲局节点加载完成的回调，这时才彻底完成加载
    liurenFinishedHandler () {
      this.onLoadingSuccess()
    },
    
    
    liurenChangeHandler (liuren, extraInfo) {
      this.liuren = liuren
      this.updateExtraInfo(extraInfo)
    },
    
    liurenSpecialHandler (data) {
      if (data.action === 'baguaCompare') {
        // 同步 对象比较选项
        this.compareSign = data.type
        this.compareSelected = data.selected
        // 对象比较事件
        this.$refs.liurenTool.setCompare(data.type, data.item)
      }
    },
    
    // 工具设定信息变化
    toolChangeHandler (setting) {
      this.toolSetting = setting
      // 更新遁甲局的状态
      if (this.isLoaded) {
        // 自己先处理工具事件
        this.toolEvents()
        // 子组件dunjiaContent处理工具事件
        this.$refs.liurenContent.toolSetLiuren(this.toolSetting)
      }
    },
    
    // 组件本身处理八卦工具的事件
    toolEvents () {
      const setting = this.toolSetting
      
      // 处理八卦标记事件
      this.baguaElementEvent(setting, 'liuren')
    },
    
    updateExtraInfo (extraInfo) {
      let result = []
      Object.keys(extraInfo).sort().forEach((key) => {
        result = result.concat(extraInfo[key])
      })
      this.liurenExtraInfo = result
    },
    
    // 特殊事件，通知子组件dunjiaContent
    onSpecialChange (data) {
      this.$refs.liurenContent.liurenSpecialHandler(data)
      // 不要忘记更新八卦工具
      this.$refs.liurenTool.setCompare(data.type, data.item)
    },
  }
}