import baguaElement from '@/templates/bagua/baguaElement/baguaElement.vue'
import baguaTool from '@/templates/bagua/baguaTool/baguaTool.vue'
import dunjiaContent from '@/templates/bagua/dunjiaContent/dunjiaContent.vue'

import baguaElementSignMixin from '@/mixins/baguaElementSign.js'

import CM from '@/class/index.js'
import User from '@/class/user/user.js'

// 遁甲容器边距
const DUNJIA_PADDING = 10
// 单宫尺寸
const DUNJIA_PALACE_SIZE = 60
// 一个外圈的尺寸
const DUNJIA_EXTRA_SIZE = 16
// 外圈之间的间隔
const DUNJIA_EXTRA_MARGIN = 5

// 遁甲工具栏设定
const TOOL_LIST = {
  timeDunjia: [
    // 时令查看
    { key: 'seasonCheck' },
    // 外圈神煞
    { key: 'outerGods', type: 'timeDunjia' },
    // 穿太乙
    // { key: 'taiyi' },
    // 移星换斗
    { key: 'moveStar' },
  ],
  posDunjia: [
    // 时令查看
    { key: 'seasonCheck' },
    // 外圈神煞
    { key: 'outerGods', type: 'posDunjia' },
    // 山向选局
    { key: 'selectPos' }
  ],
}

export default {
  name: 'dunjiaTemplate',
  mixins: [
    baguaElementSignMixin
  ],
  components: {
    baguaElement,
    baguaTool,
    dunjiaContent
  },
  props: {
    options: {
      default () {
        return {}
      },
      type: Object
    }
  },
  data () {
    return {
      CustomBar: this.CustomBar,
      
      user: new User(),
      
      // 遁甲工具设定
      toolOptions: [],
      // 遁甲工具设定信息
      toolSetting: null,
      
      isLoaded: false,
      // 遁甲局对象
      dunjia: null,
      // 遁甲局额外显示信息
      dunjiaExtraInfo: [],
      
      // 是否显示太乙
      isTaiyiShow: false,
      // 是否激活太乙，指初次显示
      isTaiyiActivated: false,
    }
  },
  /*
  watch: {
    dunjia: {
      handler (val) {
        this.$nextTick(() => {
          // 与局的遁甲同步
          this.$refs.dunjiaContent.setDunjia(val)
        })
      },
      deep: true
    }
  },
  */
  computed: {
    // 遁甲模板样式, 与工具栏的高度相关
    templateStyle () {
      if (!this.toolSetting) {
        return {}
      }
      return {
        paddingBottom: this.toolSetting.height + 'px'
      }
    },
    
    // 公历生日
    solarTimeText () {
      if (!this.dunjia) {
        return ''
      }
      return this.dunjia.date.getSolarName() + ' ' + this.dunjia.date.getWeekDay() + ' ' + this.dunjia.date.getTime()
    },
    
    // 农历生日
    lunarTimeText () {
      if (!this.dunjia) {
        return ''
      }
      return this.dunjia.date.getLunarName() + ' ' + this.dunjia.date.ganzhi[3].zhi.name + '时'
    },
    
    // 山向信息文字
    mountainText () {
      if (!this.dunjia || this.dunjia.options.type !== 'posDunjia') {
        return ''
      }
      return this.dunjia.getMountainText()
    },
    
    // 初始山向
    startMountainText () {
      const angle = parseFloat(this.options.angle)
      const detail = CM.Mountain.getMountainDetailFromAngle(angle)
      const mountainIndex = CM.Mountain.getOppositeMountain(detail.index)
      return `${CM.Mountain.mountainList[mountainIndex]}山${CM.Mountain.mountainList[detail.index]}向  ${angle.toFixed(2)}°`
    },
    
    // 时局干支显示程度
    ganzhiLimit () {
      // 一般情况下只显示四柱，刻局显示五柱
      if (this.dunjia && Array.isArray(this.dunjia.countList) && this.dunjia.countList.length === 5) {
        return 5
      }
      return 4
    },
    
    // 遁甲局数文字
    dunjiaNumText () {
      if (!this.dunjia) {
        return ''
      }
      const yinyang = this.dunjia.isSolar ? '阳' : '阴'
      return yinyang + this.dunjia.num + '局'
    },
    
    // 太乙显示是否可用
    taiyiDisabled () {
      if (this.user.uid === null) {
        return true
      }
      return !this.user.hasBaguaApp()
    }
  },
  methods: {
    // 初始化
    init (onSuccess, onError) {
      // 初始化遁甲工具栏
      this.toolOptions = this.$u.obj.jsonClone(TOOL_LIST[this.options.type])
      // 先进行遁甲分类
      if (this.options.type === 'timeDunjia') {
        this.initTimeDunjia()
      }
      else if (this.options.type === 'posDunjia') {
        this.initPosDunjia()
      }
      
      // 显示主界面，结束加载
      this.isLoaded = true
      this.onLoadingSuccess = onSuccess
      
      if (this.dunjia) {
        this.$nextTick(() => {
          // 与局的遁甲同步
          this.$refs.dunjiaContent.setDunjia(this.dunjia)
        })
      }
    },
    
    // 初始化时局
    initTimeDunjia () {
      this.dunjia = new CM.TimeDunjia(this.options)
    },
    
    // 初始化山向局
    initPosDunjia () {
      this.dunjia = new CM.PosDunjia(this.options)
      // 初始化山向选局参数
      let option = this.toolOptions.find(item => item.key === 'selectPos')
      if (option) {
        this.$set(option, 'angle', this.dunjia.angle)
        this.$set(option, 'type', this.options.posDunjiaType)
      }
    },
    
    // 遁甲局节点加载完成的回调，这时才彻底完成加载
    dunjiaFinishedHandler () {
      this.onLoadingSuccess()
    },
    
    // 遁甲局发生改变回调
    dunjiaChangeHandler (dunjia, extraInfo) {
      this.dunjia = dunjia
      this.updateExtraInfo(extraInfo)
    },
    
    // 特殊事件，通知子组件dunjiaContent
    onSpecialChange (data) {
      this.$refs.dunjiaContent.dunjiaSpecialHandler(data)
      // 不要忘记更新八卦工具
      this.$refs.baguaTool.setCompare(data.type, data.item)
    },
    
    // 遁甲局发生特殊事件回调，响应子组件dunjiaContent的变动
    dunjiaSpecialHandler (data) {
      if (data.action === 'baguaCompare') {
        // 同步 对象比较选项
        this.compareSign = data.type
        this.compareSelected = data.selected
        // 对象比较事件
        this.$refs.baguaTool.setCompare(data.type, data.item)
      }
    },
    
    updateExtraInfo (extraInfo) {
      let result = []
      Object.keys(extraInfo).sort().forEach((key) => {
        result = result.concat(extraInfo[key])
      })
      this.dunjiaExtraInfo = result
    },
    
    // 遁甲工具设定信息变化
    toolChangeHandler (setting) {
      this.toolSetting = setting
      // 更新遁甲局的状态
      if (this.isLoaded) {
        // 自己先处理工具事件
        this.toolEvents()
        // 子组件dunjiaContent处理工具事件
        this.$refs.dunjiaContent.toolSetDunjia(this.toolSetting)
      }
    },
    
    // 组件本身处理八卦工具的事件
    toolEvents () {
      const setting = this.toolSetting
      
      // 处理八卦标记事件
      this.baguaElementEvent(setting, 'dunjia')
    },
    
    // 显示太乙
    taiyiShowHandler (val) {      
      this.isTaiyiShow = val
      if (this.isTaiyiShow && !this.dunjia.hasOwnProperty('taiyiNum')) {
        // 初次激活太乙
        this.isTaiyiActivated = this.isTaiyiShow
        // 没有算过太乙，则排出太乙
        this.dunjia.setTaiyi()
        // 同步改变后的遁甲局
        this.$refs.dunjiaContent.setDunjia(this.dunjia)
      }
      this.$refs.dunjiaContent.setTaiyi(val)
    },
    
    // 点击显示太乙
    taiyiShowClickHandler () {
      this.user.baguaAppCheck()
    }
  }
}