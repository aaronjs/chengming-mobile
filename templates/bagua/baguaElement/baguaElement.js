import cmStyleMixin from '@/mixins/cmStyle.js'

import CM from '@/class/index.js'

// Chrome浏览器最小字体12px
const MIN_FONT = 12

// 显示遁甲元素，主体加上右上角的阴阳角标
export default {
  name: 'baguaElement',
  mixins: [
    cmStyleMixin
  ],
  props: {
    // 主体文字尺寸
    size: {
      default: 16,
      type: [Number, String]
    },
    // 角标文字尺寸
    tipSize: {
      default: null,
      type: [Number, String]
    },
    // 显示的对象
    bagua: {
      default () {
        return {}
      },
      type: Object
    },
    // 覆盖的样式
    cover: {
      default: 'none',
      type: String
    },
    // 额外样式类，只会对主体以及右上角角标有效
    eleClass: {
      default: '',
      type: String
    },
    // 额外样式对象，只会对主体以及右上角角标有效
    eleStyle: {
      default () {
        return {}
      },
      type: Object
    },
    // 是否显示简称
    simName: {
      default: false,
      type: Boolean
    },
    // 是否强制显示阴阳
    yinyang: {
      default: false,
      type: Boolean
    },
    // 是否强制显示五行
    wuxing: {
      default: false,
      type: Boolean
    },
    // 当前时节，决定时令显示(旺相休囚死)
    // -1时，不显示时令，01234分别代表 春夏土秋冬
    season: {
      default: -1,
      type: Number
    },
    // 对象比较，是否被选中，当对象中对应的id与所绑定的bagua对象id匹配，则显示对应的样式
    compareSelected: {
      default () {
        return {
          base: '', opposite: ''
        }
      },
      type: Object
    },
    // 更多标记 circle圆圈
    extraTips: {
      default: () => [],
      type: Array
    }
  },
  data () {
    return {
      wuxingKey: ['mu', 'huo', 'tu', 'jin', 'shui'],
    }
  },
  computed: {
    // 是否显示阴阳角标
    isYinyangShow () {
      if (!this.bagua || !this.$u.obj.isNumber(this.bagua.yinyang)) {
        // 本身不具备阴阳属性的元素，不显示
        return false
      }
      // 其他情况下根据 应用设置以及组件设定 显示
      return this.$u.config.app.isYinyangShow | this.yinyang
    },
    
    // 将阴阳转换为符号
    yinyangText () {
      if (!this.isYinyangShow) { return '' }
      
      if (this.bagua.yinyang === 1) {
        return '+'
      }
      else if (this.bagua.yinyang === 0) {
        return '-'
      }
      return ''
    },
    
    // 阴阳的样式
    yinyangStyle () {
      if (this.isYinyangShow) {
        // 阴阳因为 + 和 - 占位宽度不一样，此处进行统一，使用了cm-tip-text组件中相同的算法
        let size = isNaN(parseInt(this.tipSize)) ? (parseInt(this.size) * (2 / 3)) : parseInt(this.tipSize)
        if (size < MIN_FONT) {
          size = MIN_FONT
        }
        // 但是size的数值要减半
        size = size / 2
        return {
          width: size + 'px'
        }
      }
      return {}
    },
    
    // 五行颜色类
    wuxingClass () {
      if ((!this.$u.config.app.isWuxingColor && !this.wuxing) || !this.bagua || !this.$u.obj.isNumber(this.bagua.wuxing)) {
        return ''
      }
      return 'bagua-element-' + this.wuxingKey[this.bagua.wuxing]
    },
    
    // 显示的名称
    nameShow () {
      if (!this.bagua || !this.$u.obj.isObject(this.bagua)) {
        return ''
      }
      return this.simName ? this.bagua.simName : this.bagua.name
    },
    
    // 时令 旺相休囚死 样式
    seasonClass () {
      if (this.bagua.wuxing === null || this.season < 0) {
        return ''
      }
      
      const index = CM.TimeGod.getSeasonState(this.bagua.wuxing, this.season)
      const color = this.$u.config.app.seasonStateColors[index]
      return `app-color-${color}-opa_bg`
    }
  },
  methods: {
    clickHandler () {
      this.$emit('click')
    },
    
    /* 
      设置样式
      type 类别 border/background 边框/背景
      color 颜色，决定颜色CSS，使用名词
    */
    setStyle (type = 'border', color) {
      this.$refs.tipText.setStyle(type, color)
    },
    
    removeStyle (type) {
      this.$refs.tipText.removeStyle(type)
    }
  }
}