import User from '@/class/user/user.js'

import utils from '@/utils/index.js'

// 输入间隔
const SEARCH_INPUT_INTERVAL = 500

export default {
  name: 'baguaRecords',
  components: {
    
  },
  data () {
    return {
      // 无论是statusBar还是customBar都要再重复赋值，避免小程序出问题
      CustomBar: this.CustomBar,
      StatusBar: this.StatusBar,
      
      user: new User(),
      // 未登录的状态，显示对应的提示页面
      isLogout: false,
      // 是否上拉加载
      isLoadingMore: false,
      
      // 查询条件
      query: {
        searchKey: '',
        type: 'all',
        sortType: 'createdTime',
        sort: 'desc',
        start: 1,
      },
      
      // 过滤条件
      radioData: {
        type: [
          { value: 'all', text: '全部' },
          { value: 'timeDunjia', text: '时局' },
          { value: 'posDunjia', text: '山向' },
          { value: 'bazi', text: '八字' },
          { value: 'liuren', text: '六壬' }
        ],
        sortType: [
          { value: 'createdTime', text: '创建时间' },
          { value: 'updatedTime', text: '保存时间' },
          { value: 'title', text: '名称' },
        ],
        sort: [
          { value: 'desc', text: '降序' },
          { value: 'asc', text: '升序' }
        ]
      },
      
      // 阴阳贵人
      guiGodType: {
        auto: '自动',
        solar: '阳',
        lunar: '阴'
      },
      
      // 获取到的数据
      dataSet: []
    }
  },
  methods: {
    // 初始化
    init (onSuccess, onError) {
      this.isLogout = false      
      if (this.user.uid === null) {
        this.isLogout = true
        onSuccess()
        return
      }
      
      this.getData().then((data) => {
        console.log('获取数据成功', data)
        onSuccess()
      })
      .catch((e) => {
        console.log('获取数据失败', e)
        this.$u.msg.errToast(e)
        onSuccess()
      })
    },
    
    // 前往登录
    loginHandler () {
      this.$u.router.push('login')
    },
    
    // 重新加载
    reloadHandler () {
      this.$refs.recordsLoading.loading()
    },
    
    // 显示过滤选项
    queryShowHandler () {
      this.$refs.queryPopup.show()
    },
    
    // 文字过滤
    searchInputHandler: utils.common.debounce(function (val) {
      this.query.searchKey = utils.str.escapeSpecialSymbol(val)
      uni.startPullDownRefresh()
    }, SEARCH_INPUT_INTERVAL),
    
    // 选项变化
    queryChangeHandler (val, key) {
      this.query[key] = val
      uni.startPullDownRefresh()
    },
    
    // 选中局
    selectBaguaHandler (item) {
      this.$u.router.push('bagua', [item])
    },
    
    // 删除局
    deleteHandler (item, index) {
      this.$u.msg.modal({
        title: '删除',
        content: '是否删除选中的排盘？'
      })
      .then((res) => {
        if (res) {
          this.deleteData(item, index)
        }
      })
    },
    
    // 加载更多
    loadMoreHandler () {
      this.isLoadingMore = true
      this.getData(false).then((data) => {
        console.log('获取数据成功', data)
        this.isLoadingMore = false
      })
      .catch((e) => {
        console.log('获取数据失败', e)
        this.$u.msg.errToast(e)
        this.isLoadingMore = false
      })
    },
    
    deleteData (item, index) {
      this.$u.msg.loading('删除中')
      this.$u.req.cloud('bagua', {
        action: 'deleteBagua',
        params: {
          bagua: item
        }
      }).then((res) => {
        console.log('删除遁甲局成功', res)
        // 删除数据集里对应的内容
        this.dataSet.splice(index, 1)
        this.$u.msg.hideLoading()
        this.$u.msg.toast('删除成功')
      })
      .catch((e) => {
        console.log('删除失败', e)
        this.$u.msg.hideLoading()
        this.$u.msg.errToast(e)
      })
    },
    
    // 获取数据
    async getData (isRefresh = true) {
      if (isRefresh) {
        // 刷新后要初始化起始条目
        this.query.start = 1
      }
      this.query.uid = this.user.uid
      this.query.end = this.query.start + this.$u.config.app.baguaRecordsOnceNum - 1
      const res = await this.$u.req.cloud('bagua', {
        action: 'getBagua',
        params: this.query
      })
      this.query.start = this.query.start + res.data.length
      if (isRefresh) {
        this.dataSet = []
      }
      this.dataSet = this.dataSet.concat(res.data)
      return res.data
    },
    
    getDateStr (obj) {
      return this.$u.str.dateStrByObj(obj)
    },
    
    getStampStr (stamp) {
      return this.$u.str.dateStrByTimestamp(stamp)
    },
    
    getTypeText (type) {
      const typeDic = {
        'timeDunjia': '时局',
        'posDunjia': '山向',
        'bazi': '八字',
        'liuren': '六壬'
      }
      return typeDic[type] ? typeDic[type] : '其他'
    },
    
    getTimeDunjiaType (type) {
      const typeDic = {
        'year': '年局',
        'month': '月局',
        'day': '日局',
        'hour': '时局',
        'min': '刻局'
      }
      return typeDic[type] ? typeDic[type] : '其他'
    },
    
    getPosDunjiaType (type) {
      const typeDic = {
        'year': '年局',
        'month': '月局',
        'day': '日局',
        'dragon': '透地龙局',
      }
      return typeDic[type] ? typeDic[type] : '其他'
    },
    
    // 禁用/启用所有选项
    setRadioDisabled (val = true) {
      for (let key in this.radioData) {
        let data = this.radioData[key]
        const len = data.length
        for (let i = 0; i < len; i++) {
          this.$set(data[i], 'disabled', val)
        }
      }
    }
  },
  onPullDownRefresh () {
    this.setRadioDisabled(true)
    this.getData().then((data) => {
      console.log('获取数据成功', data)
      this.setRadioDisabled(false)
      uni.stopPullDownRefresh()
    })
    .catch((e) => {
      console.log('获取数据失败', e)
      this.$u.msg.errToast(e)
      this.setRadioDisabled(false)
      uni.stopPullDownRefresh()
    })
  },
}