
export default {
  name: 'webViewPage',
  data () {
    return {
      src: ''
    }
  },
  onLoad (option) {
    option = this.$u.router.analyseData(option)
    this.src = option.url
  }
}