import baguaTemplate from '@/templates/bagua/baguaTemplate/baguaTemplate.vue'

import User from '@/class/user/user.js'
import StorageDB from '@/class/storageDB/storageDB.js'

export default {
  name: 'bagua',
  components: {
    baguaTemplate
  },
  data () {
    return {
      StatusBar: this.StatusBar,
      CustomBar: this.CustomBar,
      
      // 起局参数列表
      options: [],
      // 局的数据列表
      bagua: [],
      // 用户信息
      user: new User(),
      
      // 当前选中的页面
      selectedIndex: 0,
      // 打开的局
      loadedBagua: [0]
    }
  },
  computed: {
    // 当前局的起局参数
    currentOptions () {
      if (this.options[this.selectedIndex]) {
        return this.options[this.selectedIndex]
      }
      return null
    },
    
    // 当前局的数据
    currentBagua () {
      if (this.bagua[this.selectedIndex]) {
        return this.bagua[this.selectedIndex]
      }
      return null
    },
    
    // 当前局的标题
    currentTitle () {
      if (this.options[this.selectedIndex]) {
        return this.options[this.selectedIndex].title === '' ? '无标题' : this.options[this.selectedIndex].title
      }
      return ''
    },
    
    // 页面内容的样式
    contentStyle () {
      return {
        minHeight: `calc(100vh - ${this.CustomBar}px)`
      }
    },
    contentClass () {
      return [
        'app-card-bg-color_bg'
      ].join(' ')
    }
  },
  methods: {
    // 局是否打开
    isPageLoaded (index) {
      return this.loadedBagua.indexOf(index) >= 0
    },
    
    // 局是否显示
    isPageShow (index) {
      return this.selectedIndex === index
    },
    
    // 点击保存后触发
    saveClickHandler () {      
      this.$refs.saveModal.show()
    },
    
    // 确认保存后触发
    saveBaguaHandler (res) {
      const title = res.inputTxt
      if (!this.user.loginCheck()) {
        return
      }
      if (title === '' || title === '无标题') {
        this.$u.msg.toast('必须有标题才能保存')
        return
      }
      
      // 开始保存
      let optionObj = this.$u.obj.jsonClone(this.options[this.selectedIndex])
      optionObj.title = title
      // 获取现在的时间戳
      const nowStamp = new Date().getTime()
      
      let isFirstSave = false
      // 如果是首次保存
      if (!optionObj.saveBaguaInfo) {
        let saveBaguaInfo = {
          createdTime: nowStamp,
          updatedTime: nowStamp,
          author: this.user.uid
        }
        optionObj.saveBaguaInfo = saveBaguaInfo
        isFirstSave = true
      }
      // 如果是已经保存过的，进行标题修改，时间覆盖
      else {
        optionObj.saveBaguaInfo.updatedTime = nowStamp
      }
      
      this.$u.msg.loading('保存中')
      this.$u.req.cloud('bagua', {
        action: 'saveBagua',
        params: {
          isFirstSave,
          bagua: optionObj
        }
      }).then((res) => {
        console.log('保存遁甲局成功', res)
        this.options[this.selectedIndex] = res.bagua
        this.$u.msg.hideLoading()
        this.$u.msg.toast('保存成功')
        this.$refs.saveModal.hide()
      })
      .catch((e) => {
        console.log('保存失败', e)
        this.$u.msg.hideLoading()
        this.$u.msg.errToast(e)
        this.$refs.saveModal.hide()
      })
    },
    
    async testDB () {
      const storage = new StorageDB()
      await storage.init()
      console.log(storage)
      
      const bagua = storage.collection('bagua')
      await bagua.remove()
      await bagua.add({ a: 2, b: 1, c: 2 })
      await bagua.add({ a: 3, b: 1, c: 1 })
      await bagua.add({ a: 1, b: 2, c: 3 })
      await bagua.add({ a: 4, b: 2, c: 3 })
      await bagua.add({ a: 5, b: 3, c: 3 })
      let res = await bagua.where({
        b: 2,
        c: 3
      }).get()
      console.log(res, bagua)
      
    }
  },
  onLoad (options) {
    this.options = this.$u.router.analyseData(options)
  }
}