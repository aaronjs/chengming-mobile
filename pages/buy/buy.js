import User from '@/class/user/user.js'

// 轮播图宽高比
const HEAD_RATIO = 16 / 9

export default {
  name: 'buy',
  data () {
    return {
      windowWidth: this.windowWidth,
      
      // 用户对象
      user: new User(),
      
      // 价格列表，分别对应免费获取，包月价格，永久解锁价格
      priceList: [0, 50, 600],
      // 持续时间，分别对应免费获取，包月价格，永久解锁的持续时间，forever的持续时间代表无限久
      durationList: [604800, 2592000, 'forever'],
      // 商品描述，分别对应免费获取，包月价格，永久解锁的持续时间
      descriptionList: [
        '免费试用进阶功能一周',
        '包月解锁进阶功能',
        '永久解锁进阶功能'
      ],
      // 商品描述前缀，用于生成订单名称
      descriptionHead: '承明遁甲排盘',
      // 功能列表：功能名/免费版/进阶版
      funcList: [
        ['时家奇门排盘', 1, 1],
        ['山向奇门排盘', 0, 1],
        // ['自定奇门排盘', 1, 1],
        ['道家八字排盘', 1, 1],
        // ['大六壬排盘', 1, 1],
        ['各种辅助工具', 0, 1],
        ['遁甲移星换斗', 0, 1],
        ['遁甲穿壬与神煞', 0, 1],
        ['遁甲穿太乙数', 0, 1],
        ['山向选局', 0, 1],
      ],
      // 选择的包月数
      monthNum: 1,
    }
  },
  computed: {
    // 轮播图样式
    headerStyle () {
      return {
        height: (this.windowWidth / HEAD_RATIO) + 'px'
      }
    },
    
    // 合法化包月数字
    fixedMonthNum () {
      if (!this.$u.str.isNumberString(this.monthNum.toString())) {
        return 0
      }
      
      const num = parseInt(this.monthNum)
      if (num <= 0) {
        return 0
      }
      return num
    },
    
    // 包月需要支付的金额
    monthBuyPrice () {
      return (this.priceList[1] * this.fixedMonthNum).toFixed(1)
    },
    
    // 到期时间
    endStr () {
      if (!this.user.baguaAppData.hasOwnProperty('expiredTime')) {
        // 尚未购买
        return 'hasnt'
      }
      if (this.user.baguaAppData.expiredTime === this.durationList[2]) {
        // 永久持有
        return this.durationList[2]
      }
      const nowStamp = new Date().getTime()
      if (this.user.baguaAppData.expiredTime < nowStamp) {
        return 'timeout'
      }
      // 到期时间字符串
      return this.$u.str.dateStrByTimestamp(this.user.baguaAppData.expiredTime)
    }
  },
  methods: {
    // 购买触发
    buyHandler (index) {
      if (this.user.baguaAppData.expiredTime === 'forever') {
        this.$u.msg.toast('已经永久开通进阶功能')
        return
      }
      if (this.user.baguaAppData.usedTrial && index === 0) {
        this.$u.msg.toast('每个用户只能免费试用一次')
        return
      }
      
      // 检查登录
      const loginCheck = this.user.loginCheck('必须登录后才可购买，是否登录')
      if (!loginCheck) {
        return
      }
      
      // 计算购买信息
      let duration = this.durationList[index]
      let description = this.descriptionHead + ' ' + this.descriptionList[index]
      let price = this.priceList[index] * 100
      let onTrial = index === 0 ? true : false
      if (index === 1) {
        // 包月特殊
        duration = duration * this.fixedMonthNum
        description = description + ' ' + this.fixedMonthNum + '个月'
        price = price * this.fixedMonthNum
      }
      this.$u.msg.loading('请稍后')
      this.user.getBaguaApp(description, price, duration, onTrial).then((msg) => {
        this.$u.msg.hideLoading()
        this.$u.msg.toast(msg)
      })
      .catch((e) => {
        console.log('购买失败', e)
        this.$u.msg.hideLoading()
        this.$u.msg.errToast(e)
      })
    },
    
    wxPay () {
      uni.requestPayment({
          provider: 'wxpay',
          timeStamp: String(Date.now()),
          nonceStr: 'A1B2C3D4E5',
          package: 'prepay_id=wx20180101abcdefg',
          signType: 'MD5',
          paySign: '',
          success: function (res) {
              console.log('success:' + JSON.stringify(res));
          },
          fail: function (err) {
              console.log('fail:' + JSON.stringify(err));
          }
      })
    }
  }
}