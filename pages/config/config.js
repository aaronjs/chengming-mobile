export default {
  name: 'config',
  data () {
    return {
      config: this.$u.config.app,
      
      configKeys: [
        { key: 'isYinyangShow', label: '是否显示阴阳角标' },
        { key: 'isWuxingColor', label: '是否显示五行颜色' },
        { key: 'isDunjiaZhiShow', label: '遁甲排盘是否显示地支' },
        { key: 'isLiurenZhiShow', label: '六壬排盘是否显示地支' },
      ]
    }
  },
  methods: {
    changeHandler (val, key) {
      this.$u.config.set(key, val)
    }
  },
  onLoad () {
  }
}