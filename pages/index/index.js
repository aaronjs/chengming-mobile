import baguaCreate from '@/templates/bagua/baguaCreate/baguaCreate.vue'
import userCenter from '@/templates/common/userCenter/userCenter.vue'

export default {
  name: 'index',
  components: {
    baguaCreate,
    userCenter
  },
  data () {
    return {
      // 底部Tab
      tabList: [
        {
          icon: 'iconfont icon-calendar',
          text: '起局排盘',
          key: 'bagua',
        },
        {
          icon: 'iconfont icon-bussiness-man1',
          text: '我的',
          key: 'user'
        },
      ],
      
      // 当前选中的页面
      selectedIndex: 0,
      // 打开的页面
      loadedPages: [0]
    }
  },
  computed: {
    currentPage () {
      return this.tabList[this.selectedIndex].text
    }
  },
  methods: {
    // 点击万年历的回调
    calendarClickHandler () {
      this.$u.router.push('calendar').then(() => {})
      .catch((e) => {
        console.log(e)
      })
    },
    
    // 切换Tab选项卡的回调
    tabChangeHandler (item, index) {
      this.selectedIndex = index
      if (this.loadedPages.indexOf(index) < 0) {
        this.loadedPages.push(index)
      }
    },
    
    // 页面是否打开
    isPageLoaded (index) {
      return this.loadedPages.indexOf(index) >= 0
    },
    
    // 页面是否显示
    isPageShow (index) {
      return this.selectedIndex === index
    }
  },
  onLoad (options) {
    // this.options = this.$u.router.analyseData(options)
  }
}