import User from '@/class/user/user.js'

export default {
  name: 'login',
  data () {
    return {
      user: new User(),
      
      // 是否使用密码登录
      // #ifdef MP
      isPwdLogin: false
      // #endif
      
      // #ifdef H5
      // 微信H5可以自动登录，浏览器要输密码
      isPwdLogin: true
      // #endif
      
      // #ifdef APP-PLUS
      // 微信H5可以自动登录，浏览器要输密码
      isPwdLogin: true
      // #endif
    }
  },
  computed: {
    isLogin () {
      return this.user.uid !== null
    },
  },
  methods: {
    noPwdLoginHandler () {
      if (this.isLogin) { return }
      
      // #ifdef MP
      // 小程序登录
      this.$u.msg.loading('登录中')
      this.user.mpLogin().then(() => {
        this.$u.msg.hideLoading()
        console.log('登录成功', this.user)
        this.$u.msg.toast('登录成功')
        uni.navigateBack({ delta: 1 })
        
        this.user.checkMobile()
      })
      .catch((e) => {
        this.$u.msg.hideLoading()
        console.log('小程序登录失败', e)
        this.$u.msg.errToast(e)
      })
      // #endif
    }
  }
}